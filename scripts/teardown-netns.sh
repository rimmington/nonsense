#!/usr/bin/env bash

set -euxo pipefail

ip netns delete aneq
ip netns delete pell
ip netns delete mane
ip netns delete elsweyr
