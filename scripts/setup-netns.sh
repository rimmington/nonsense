#!/usr/bin/env bash

set -euxo pipefail

sysctl -w net.ipv6.conf.all.disable_ipv6=0
ip netns add elsweyr
ip netns add aneq
ip netns add pell
ip netns add mane
ip link add sand0 netns elsweyr type bridge
ip netns exec elsweyr sysctl -w net.ipv6.conf.sand0.disable_ipv6=1
ip -n elsweyr link set sand0 up
ip link add mane0 netns elsweyr type veth peer mane1 netns mane
ip -n elsweyr link set mane0 master sand0
ip -n elsweyr link set mane0 up
ip -n mane addr add 10.120.1.1/24 dev mane1
ip -n mane link set mane1 up
ip -n mane link set lo up
ip link add aneq0 netns elsweyr type veth peer aneq1 netns aneq
ip -n elsweyr link set aneq0 master sand0
ip -n elsweyr link set aneq0 up
ip -n aneq addr add 10.120.1.2/24 dev aneq1
ip -n aneq link set aneq1 up
ip -n aneq link set lo up
ip link add pell0 netns elsweyr type veth peer pell1 netns pell
ip -n elsweyr link set pell0 master sand0
ip -n elsweyr link set pell0 up
ip -n pell addr add 10.120.1.3/24 dev pell1
ip -n pell link set pell1 up
ip -n pell link set lo up
