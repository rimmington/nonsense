let
  lock = builtins.fromJSON (builtins.readFile ./flake.lock);
  default = (import
    (
      fetchTarball {
        url = lock.nodes.flake-compat.locked.url or "https://github.com/edolstra/flake-compat/archive/${lock.nodes.flake-compat.locked.rev}.tar.gz";
        sha256 = lock.nodes.flake-compat.locked.narHash;
      }
    )
    { src = ./.; }
  ).defaultNix;
  isDerivation = x: x.type or null == "derivation";
  mapAttrs' =
    # A function, given an attribute's name and value, returns a new `nameValuePair`.
    f:
    # Attribute set to map over.
    set:
    builtins.listToAttrs (map (attr: f attr set.${attr}) (builtins.attrNames set));
  nameValuePair =
    # Attribute name
    name:
    # Attribute value
    value:
    { inherit name value; };
  markForDescent = as:
    if isDerivation as
      then as
      else
        mapAttrs' (n: v: nameValuePair (builtins.replaceStrings [":"] ["_"] n) (markForDescent v)) as
        // { recurseForDerivations = true; };
in {
  hydraJobs = markForDescent default.hydraJobs;
  internal = {
    nixpkgs =
      let info = lock.nodes.nixpkgs-unstable.locked;
      in fetchTarball {
        url = "https://api.github.com/repos/${info.owner}/${info.repo}/tarball/${info.rev}";
        sha256 = info.narHash;
      };
    hnu =
      let info = lock.nodes.haskell-nix-utils.locked;
      in fetchTarball {
        url = "https://gitlab.com/api/v4/projects/${info.owner}%2F${info.repo}/repository/archive.tar.gz?sha=${info.rev}";
        sha256 = info.narHash;
      };
  };
}
