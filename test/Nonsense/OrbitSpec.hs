module Nonsense.OrbitSpec (spec) where

import Hedgehog.Gen qualified as Gen
import Hedgehog.Range qualified as Range
import Network.Socket (tupleToHostAddress, tupleToHostAddress6)
import Nonsense.Address (keyAddr)
import Nonsense.Orbit
  (Registered (..), RemoteAddr (..), Seen (..), bsRegistered, bsSeen, moonAddr, moonReg,
  registeredBS, seenBS)
import Nonsense.PacketSpec (genPubKeyBytes)
import Nonsense.ProcSpec (genIp6Lo)
import RIO
import Test.Hspec
import Test.Hspec.Hedgehog

spec :: Spec
spec = do
    describe "registeredBS" $
        it "round-trips" $ hedgehog do
            a <- forAll $ Registered <$> genPubKeyBytes <*> genRemoteAddr
            tripping a registeredBS bsRegistered
    describe "seenBS" $
        it "round-trips" $ hedgehog do
            a <- forAll $ Seen <$> genIp6Lo <*> genRemoteAddr
            tripping a seenBS bsSeen
    describe "moonReg" $
        it "produces moonAddr" $ hedgehog do
            Registered pk _ <- evalNF moonReg
            keyAddr pk === Just moonAddr

genRemoteAddr :: Gen RemoteAddr
genRemoteAddr = Gen.choice
    [ RemoteV4 <$> Gen.integral Range.linearBounded <*> (tupleToHostAddress <$> g4)
    , RemoteV6 <$> Gen.integral Range.linearBounded <*> (tupleToHostAddress6 <$> g6)
    ]
  where
    g4 = (,,,)
        <$> Gen.integral Range.linearBounded
        <*> Gen.integral Range.linearBounded
        <*> Gen.integral Range.linearBounded
        <*> Gen.integral Range.linearBounded
    g6 = (,,,,,,,)
        <$> Gen.integral Range.linearBounded
        <*> Gen.integral Range.linearBounded
        <*> Gen.integral Range.linearBounded
        <*> Gen.integral Range.linearBounded
        <*> Gen.integral Range.linearBounded
        <*> Gen.integral Range.linearBounded
        <*> Gen.integral Range.linearBounded
        <*> Gen.integral Range.linearBounded
