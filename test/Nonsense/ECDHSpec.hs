module Nonsense.ECDHSpec (spec, genGen, genPrivKey) where

import Hedgehog.Gen qualified as Gen
import Hedgehog.Range qualified as Range
import Nonsense.Bytes qualified as B
import Nonsense.ECDH
import Nonsense.K12Spec (eqMem)
import Nonsense.Random (FrozenGen, Seed (..), seedGen, thawGen, uniformW32)
import RIO
import Test.Hspec
import Test.Hspec.Hedgehog hiding (Seed)

genPrivKeyBytes :: MonadGen m => m PrivKeyBytes
genPrivKeyBytes = PrivKeyBytes . B.unsafeAtLeast . B.pack <$>
    Gen.list (Range.singleton keyLength) Gen.enumBounded

genPrivKey :: MonadGen m => m PrivKey
genPrivKey = loadPrivKey <$> genPrivKeyBytes

genGen :: MonadGen m => m FrozenGen
genGen = seedGen <$> (Seed <$> Gen.word64 Range.linearBounded <*> Gen.word64 Range.linearBounded)

spec :: Spec
spec = do
    describe "loadPrivKey" $ do
        it "loads a fixture" $ void $
            evaluate @IO $ loadPrivKey . PrivKeyBytes .
                B.unsafeAtLeast . B.pack $ replicate keyLength 0

        it "loads any 32 bytes" $ hedgehog $ do
            k <- forAll genPrivKeyBytes
            void . evaluate $ loadPrivKey k

    describe "privPubKey" $
        it "produces a pubkey for any privkey" $ hedgehog $ do
            bytes <- forAll genPrivKeyBytes
            let k = loadPrivKey bytes
                PubKeyBytes pk = privPubKey k
            B.byteLength pk === 32

    describe "keyAgreement" do
        it "produces a common shared secret" $ hedgehog $ do
            bytes0 <- forAll genPrivKeyBytes
            bytes1 <- forAll genPrivKeyBytes
            let k0 = loadPrivKey bytes0
                k1 = loadPrivKey bytes1
                s0 = keyAgreement k0 $ privPubKey k1
                s1 = keyAgreement k1 $ privPubKey k0
            annotateShow s0
            annotateShow s1
            case (s0, s1) of
                (Nothing, Nothing) -> pure ()
                (Just b0, Just b1) -> diff b0 eqMem b1
                (_, _)             -> failure

        it "doesn't choke on invalid public key" $ hedgehog do
            rkBytes <- forAll genPrivKeyBytes
            rubbish <- forAll $ PubKeyBytes . B.unsafeAtLeast <$>
                Gen.bytes (Range.singleton keyLength)
            let rk = loadPrivKey rkBytes
            void . evaluate $ keyAgreement rk rubbish

    describe "privKeyBytesGen" $
        it "generates a private key" $ hedgehog do
            gen <- forAll genGen
            void . evaluate $ runST do
                r <- thawGen gen
                loadPrivKey <$> privKeyBytesGen (uniformW32 r)
