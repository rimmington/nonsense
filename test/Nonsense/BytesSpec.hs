module Nonsense.BytesSpec (spec) where

import Hedgehog.Gen qualified as Gen
import Hedgehog.Range qualified as Range
import Nonsense.Bytes qualified as B
import RIO
import System.IO.Unsafe (unsafeDupablePerformIO)
import Test.Hspec
import Test.Hspec.Hedgehog

spec :: Spec
spec = describe "constEqMem" $
    it "holds when (==) holds" $ hedgehog do
        l <- forAll $ Gen.int (Range.linear 0 2048)
        a <- forAll $ Gen.bytes (Range.singleton l)
        b <- forAll $ Gen.choice
            [ Gen.bytes (Range.singleton l)
            , pure a
            ]
        (a == b) === unsafeDupablePerformIO (a `B.constEqMem` b)
