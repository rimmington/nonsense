{-# language StrictData #-}

module Nonsense.ProcSpec (mkFlow, mkFlow', genIp6Lo, spec) where

import Data.Bits (xor)
import Data.Primitive.PVar (newPVar, readPVar, writePVar)
import Data.Vector.Storable qualified as V
import Data.Vector.Storable.Mutable qualified as MV
import Hedgehog.Gen qualified as Gen
import Hedgehog.Range qualified as Range
import Network.Socket (SockAddr (..))
import Nonsense.AEAD (AeadKey, keyCipher)
import Nonsense.AEADSpec (genAeadKey)
import Nonsense.Bytes (Buffer)
import Nonsense.Cipher (initCipher)
import Nonsense.Constants qualified as Const
import Nonsense.Ctl (timeoutFlowExpires)
import Nonsense.Ip6Addr (Ip6Addr (..), Ip6Lo (..), addrIp6Lo)
import Nonsense.K12Spec (eqMem, genSharedSecret)
import Nonsense.MonoTime (MonoTime32 (..), leq)
import Nonsense.Packet
  (FlowId (FlowId), FromJunkBuffer, JunkBuffer, V6Buffer, copyIntoJunkBuffer, fromJunkBuffer,
  getBuf, newJunkBuffer, v6DestAddr)
import Nonsense.PacketSpec
  (genContent, genHelo, genKeepalive, genTher, genV6BufferWithAddrs, genValidAddr)
import Nonsense.Proc
  (Flow (..), Role (..), describeWindow, eqFlow, maxSendCount, newWindow, notReplaced, procIncoming,
  procOutgoing, splitKey, windowHigh, windowLength, windowMember, windowSet)
import RIO
import StmContainers.Map qualified as M
import Test.Hspec
import Test.Hspec.Hedgehog

data OutgoingActions
  = OutgoingActions
    { dispatched :: Integer
    , sent       :: Integer
    , saidGood   :: Integer
    }

checkProcOutgoing ::
    Ip6Lo
 -> M.Map Ip6Lo Flow
 -> MonoTime32
 -> JunkBuffer
 -> Buffer
 -> PropertyT IO OutgoingActions
checkProcOutgoing ourAddr flowMap now junk buf = do
    dispatchRef <- newIORef (0 :: Integer)
    sendRef <- newIORef (0 :: Integer)
    evalIO $ procOutgoing
        ourAddr
        (atomically . (`M.lookup` flowMap))
        (modifyIORef dispatchRef (+1))
        (\_ _ -> modifyIORef sendRef (+1))
        now
        junk
        buf
    dispatched <- readIORef dispatchRef
    sent <- readIORef sendRef
    pure OutgoingActions{saidGood = 0, ..}

checkProcIncoming ::
    M.Map FlowId Flow
 -> MonoTime32
 -> JunkBuffer
 -> Buffer
 -> IO OutgoingActions
checkProcIncoming flowMap now junk buf = do
    dispatchRef <- newIORef (0 :: Integer)
    sendRef <- newIORef (0 :: Integer)
    goodRef <- newIORef (0 :: Integer)
    procIncoming
        (Ip6Lo 0)
        (atomically . (`M.lookup` flowMap))
        (modifyIORef dispatchRef (+1))
        (\_ -> modifyIORef goodRef (+1))
        (\_ -> modifyIORef sendRef (+1))
        now
        junk
        buf
    dispatched <- readIORef dispatchRef
    sent <- readIORef sendRef
    saidGood <- readIORef goodRef
    pure OutgoingActions{..}

mkFlow :: Gen AeadKey -> PropertyT IO Flow
mkFlow mkK = do
    sentAnyOff    <- forAll $ Gen.word32 (Range.linear 0 timeoutFlowExpires)
    sentYetAckOff <- forAll $ Gen.word32 (Range.linear 0 sentAnyOff)
    recvAnyOff    <- forAll $ Gen.word32 (Range.linear 0 timeoutFlowExpires)
    recvYetAckOff <- forAll $ Gen.word32 (Range.linear 0 recvAnyOff)
    mkFlow' sentAnyOff sentYetAckOff recvAnyOff recvYetAckOff genIp6Lo mkK

genIp6Lo :: Gen Ip6Lo
genIp6Lo = Ip6Lo <$> Gen.word64 Range.linearBounded

mkFlow' ::
    Word32 -- ^ sentAny offset
 -> Word32 -- ^ sentYetAck offset
 -> Word32 -- ^ recvAny offset
 -> Word32 -- ^ recvYetAck offset
 -> Gen Ip6Lo   -- ^ their addr
 -> Gen AeadKey -- ^ recv key
 -> PropertyT IO Flow
mkFlow' sentAnyOff sentYetAckOff recvAnyOff recvYetAckOff lo rk = do
    remoteStr <- forAll $ Gen.string (Range.linear 0 10) Gen.unicode
    remoteAddr <- newTVarIO $ SockAddrUnix remoteStr
    theirId <- forAll $ FlowId <$> Gen.word32 Range.linearBounded
    theirAddr <- forAll lo
    sendK <- forAll genAeadKey
    recvK <- forAll rk
    created <- forAll $ MonoTime32 <$> Gen.word32 Range.linearBounded
    role <- forAll $ Gen.element [Initiator, Responder]
    sendCipher <- evalIO initCipher
    evalIO $ keyCipher sendCipher sendK
    recvCipher <- evalIO initCipher
    evalIO $ keyCipher recvCipher recvK
    sentAny    <- newPVar . MonoTime32 $ ms32 created + sentAnyOff
    sentYetAck <- newPVar . MonoTime32 $ ms32 created + sentYetAckOff
    recvAny    <- newPVar . MonoTime32 $ ms32 created + recvAnyOff
    recvYetAck <- newPVar . MonoTime32 $ ms32 created + recvYetAckOff
    replacedBy <- newPVar notReplaced
    sentCount  <- newPVar 2
    recvWindow <- evalIO newWindow
    pure Flow{..}

genValidV6 :: PropertyT IO (V.Vector Word8, Ip6Lo, Ip6Addr, FromJunkBuffer V6Buffer)
genValidV6 = do
    (a, b, c, d) <- genV6BufferWithAddrs genValidAddr (const genValidAddr)
    pure (a, addrIp6Lo b, c, d)

epoch :: MonoTime32
epoch = MonoTime32 0

procOutgoingSpec :: Spec
procOutgoingSpec = describe "procOutgoing" do
    it "dispatches unknown destinations" do
        (_, ourAddr, _, fromJunkBuffer -> (junk, v6)) <- genValidV6
        flowMap <- evalIO M.newIO
        OutgoingActions{dispatched, sent} <- checkProcOutgoing ourAddr flowMap epoch junk (getBuf v6)
        dispatched === 1
        sent === 0

    it "ignores packets with out-of-range destinations" do
        (_, ourAddr, _, fromJunkBuffer -> (junk, v6)) <- genV6BufferWithAddrs genValidAddr
            $ \_ -> Ip6Addr
                <$> Gen.filter (/= Const.subnet) (Gen.word64 Range.linearBounded)
                <*> Gen.word64 Range.linearBounded
        flowMap <- evalIO M.newIO
        let ourLo = addrIp6Lo ourAddr
        OutgoingActions{dispatched, sent} <- checkProcOutgoing ourLo flowMap epoch junk (getBuf v6)
        dispatched === 0
        sent === 0

    it "dispatches for known destinations when flow is not goodForSending" do
        (_, ourAddr, _, fromJunkBuffer -> (junk, v6)) <- genValidV6
        flow <- (\Flow{..} -> Flow{role = Initiator, ..}) <$> mkFlow genAeadKey
        Ip6Addr _ (Ip6Lo -> dstLo) <- evalIO $ v6DestAddr v6
        flowMap <- evalIO $ atomically do
            m <- M.new
            M.insert flow dstLo m
            pure m
        writePVar (recvAny flow) (created flow)
        writePVar (recvYetAck flow) (created flow)
        OutgoingActions{dispatched, sent} <- checkProcOutgoing ourAddr flowMap epoch junk (getBuf v6)
        dispatched === 1
        sent === 0

    it "sends to known destinations when flow is goodForSending" do
        (_, ourAddr, _, fromJunkBuffer -> (junk, v6)) <- genValidV6
        flow <- mkFlow genAeadKey
        Ip6Addr _ (Ip6Lo -> dstLo) <- evalIO $ v6DestAddr v6
        flowMap <- evalIO $ atomically do
            m <- M.new
            M.insert flow dstLo m
            pure m
        case role flow of
            Responder -> pure ()
            Initiator -> do
                rtime <- readPVar $ recvAny flow
                when (rtime == created flow) $
                    writePVar (recvAny flow) $ MonoTime32 $ ms32 rtime + 1
        OutgoingActions{dispatched, sent} <- checkProcOutgoing ourAddr flowMap epoch junk (getBuf v6)
        dispatched === 0
        sent === 1

    it "dispatches when outbound counter is too high" do
        preSent <- forAll $ Gen.integral (Range.linear maxSendCount maxBound)
        (_, ourAddr, _, fromJunkBuffer -> (junk, v6)) <- genValidV6
        flow <- mkFlow genAeadKey
        Ip6Addr _ (Ip6Lo -> dstLo) <- evalIO $ v6DestAddr v6
        flowMap <- evalIO $ atomically do
            m <- M.new
            M.insert flow dstLo m
            pure m
        writePVar (sentCount flow) (fromIntegral preSent)
        OutgoingActions{dispatched, sent} <- checkProcOutgoing ourAddr flowMap epoch junk (getBuf v6)
        dispatched === 1
        sent === 0

    it "ignores packets with incorrect lengths" pending
    it "ignores random bytes" pending

    it "updates time after sending" do
        sentAnyOff    <- forAll $ Gen.word32 (Range.linear 0 (timeoutFlowExpires - 1))
        sentYetAckOff <- forAll $ Gen.word32 (Range.linear 0 sentAnyOff)
        -- Start at 1 to take updateRecvAny/goodForSending into account
        recvAnyOff    <- forAll $ Gen.word32 (Range.linear 1 (timeoutFlowExpires - 1))
        recvYetAckOff <- forAll $ Gen.word32 (Range.linear 0 recvAnyOff)
        additional <- forAll . Gen.word32 $
            Range.linear (max sentAnyOff recvAnyOff) (timeoutFlowExpires - 1)

        (_, ourAddr, _, fromJunkBuffer -> (_, v6)) <- genValidV6

        flow <- mkFlow' sentAnyOff sentYetAckOff recvAnyOff recvYetAckOff genIp6Lo genAeadKey
        let now = MonoTime32 $ ms32 (created flow) + additional

        Ip6Addr _ (Ip6Lo -> dstLo) <- evalIO $ v6DestAddr v6
        flowMap <- evalIO $ atomically do
            m <- M.new
            M.insert flow dstLo m
            pure m
        -- Copy v6 since it'll be overwritten
        (fromJunkBuffer -> (junk', v6')) <- evalIO $ (`copyIntoJunkBuffer` v6) =<< newJunkBuffer
        OutgoingActions{dispatched, sent} <- checkProcOutgoing ourAddr flowMap now junk' (getBuf v6')
        dispatched === 0
        sent === 1

        sentAny <- readPVar $ sentAny flow
        sentAny === now
        sentYetAck <- readPVar $ sentYetAck flow
        case sentYetAckOff <= recvAnyOff of
            True  -> sentYetAck === now
            False -> sentYetAck === MonoTime32 (ms32 (created flow) + sentYetAckOff)

procIncomingSpec :: Spec
procIncomingSpec = describe "procIncoming" do
    it "dispatches HELO" do
        (_, _, junk, msg) <- genHelo
        flowMap <- evalIO M.newIO
        OutgoingActions{dispatched, sent} <- evalIO $
            checkProcIncoming flowMap epoch junk (getBuf msg)
        dispatched === 1
        sent === 0

    it "dispatches THER" do
        (_, _, _, _, _, junk, msg) <- genTher
        flowMap <- evalIO M.newIO
        OutgoingActions{dispatched, sent} <- evalIO $
            checkProcIncoming flowMap epoch junk (getBuf msg)
        dispatched === 1
        sent === 0

    it "ignores unknown flows for KEEP" do
        (_, _, _, junk, msg) <- genKeepalive
        flowMap <- evalIO M.newIO
        OutgoingActions{dispatched, sent} <- evalIO $
            checkProcIncoming flowMap epoch junk (getBuf msg)
        dispatched === 0
        sent === 0

    it "updates time and goodness for KEEP on known flows" do
        additional <- forAll $ Gen.word32 (Range.linear 0 (timeoutFlowExpires - 1))
        (ourId, k, _, junk, msg) <- genKeepalive
        flowMap <- evalIO M.newIO
        flow <- mkFlow (Gen.constant k)
        atomically $ M.insert flow ourId flowMap
        let now = MonoTime32 $ ms32 (created flow) + additional
        prevRecvAny <- evalIO . readPVar $ recvAny flow
        OutgoingActions{dispatched, sent, saidGood} <- evalIO $
            checkProcIncoming flowMap now junk (getBuf msg)
        dispatched === 0
        sent === 0
        recvAny <- evalIO . readPVar $ recvAny flow
        case role flow of
            Initiator -> do
                when (prevRecvAny == created flow) $ saidGood === 1
                diff (ms32 now) (leq (created flow)) (ms32 recvAny)
            Responder -> recvAny === now

    it "ignores duplicate counters for KEEP" do
        additional <- forAll $ Gen.word32 (Range.linear 0 (timeoutFlowExpires - 2))
        (ourId, k, _, junk, msg) <- genKeepalive
        flowMap <- evalIO M.newIO
        flow <- mkFlow (Gen.constant k)
        atomically $ M.insert flow ourId flowMap

        -- Add 1 to take updateRecvAny/goodForSending into account
        let now = MonoTime32 $ ms32 (created flow) + 1
        a1 <- evalIO $ checkProcIncoming flowMap now junk (getBuf msg)
        dispatched a1 === 0
        sent a1 === 0
        recvAny1 <- evalIO . readPVar $ recvAny flow
        recvAny1 === now

        let now' = MonoTime32 $ ms32 now + additional
        a2 <- evalIO $ checkProcIncoming flowMap now' junk (getBuf msg)
        dispatched a2 === 0
        sent a2 === 0
        recvAny2 <- evalIO . readPVar $ recvAny flow
        recvAny2 === now

    it "ignores unknown flows for CONT" do
        (_, _, _, junk, msg) <- genContent
        flowMap <- evalIO M.newIO
        OutgoingActions{dispatched, sent} <- evalIO $
            checkProcIncoming flowMap epoch junk (getBuf msg)
        dispatched === 0
        sent === 0

    it "sends for known flows with valid HMAC" do
        (ourId, k, _, junk, msg) <- genContent
        flowMap <- evalIO M.newIO
        flow <- mkFlow (Gen.constant k)
        atomically $ M.insert flow ourId flowMap
        OutgoingActions{dispatched, sent} <- evalIO $
            checkProcIncoming flowMap epoch junk (getBuf msg)
        dispatched === 0
        sent === 1

    it "updates time and goodness for valid CONT" do
        sentAnyOff    <- forAll $ Gen.word32 (Range.linear 0 (timeoutFlowExpires - 1))
        sentYetAckOff <- forAll $ Gen.word32 (Range.linear 0 sentAnyOff)
        recvAnyOff    <- forAll $ Gen.word32 (Range.linear 0 (timeoutFlowExpires - 1))
        recvYetAckOff <- forAll $ Gen.word32 (Range.linear 0 recvAnyOff)
        additional <- forAll . Gen.word32 $
            Range.linear (max sentAnyOff recvAnyOff) (timeoutFlowExpires - 1)

        (ourId, k, _, _, msg) <- genContent
        flowMap <- evalIO M.newIO
        flow <- mkFlow' sentAnyOff sentYetAckOff recvAnyOff recvYetAckOff genIp6Lo (Gen.constant k)
        (junk', msg') <- evalIO $ fromJunkBuffer <$> ((`copyIntoJunkBuffer` msg) =<< newJunkBuffer)
        atomically $ M.insert flow ourId flowMap
        let now = MonoTime32 $ ms32 (created flow) + additional
        OutgoingActions{dispatched, sent, saidGood} <- evalIO $
            checkProcIncoming flowMap now junk' (getBuf msg')
        dispatched === 0
        sent === 1

        case (role flow, recvAnyOff) of
            (Initiator, 0) -> do
                saidGood === 1
            _ -> do
                recvAny <- readPVar $ recvAny flow
                recvAny === now

        recvYetAck <- readPVar $ recvYetAck flow
        case recvYetAckOff <= sentAnyOff of
            True  -> recvYetAck === now
            False -> recvYetAck === MonoTime32 (ms32 (created flow) + recvYetAckOff)

    it "ignores duplicate counters for CONT" do
        additional <- forAll $ Gen.word32 (Range.linear 0 (timeoutFlowExpires - 2))
        (ourId, k, _, _, msg) <- genContent
        flowMap <- evalIO M.newIO
        flow <- mkFlow' 0 0 0 0 genIp6Lo (Gen.constant k)
        atomically $ M.insert flow ourId flowMap

        -- Add 1 to take updateRecvAny/goodForSending into account
        let now = MonoTime32 $ ms32 (created flow) + 1
        (junk1, msg1) <- evalIO $ fromJunkBuffer <$> ((`copyIntoJunkBuffer` msg) =<< newJunkBuffer)
        a1 <- evalIO $ checkProcIncoming flowMap now junk1 (getBuf msg1)
        dispatched a1 === 0
        sent a1 === 1
        recvAny1 <- readPVar $ recvAny flow
        recvAny1 === now

        let now' = MonoTime32 $ ms32 now + additional
        (junk2, msg2) <- evalIO $ fromJunkBuffer <$> ((`copyIntoJunkBuffer` msg) =<< newJunkBuffer)
        a2 <- evalIO $ checkProcIncoming flowMap now' junk2 (getBuf msg2)
        dispatched a2 === 0
        sent a2 === 0
        recvAny2 <- readPVar $ recvAny flow
        recvAny2 === now

    it "drops for known flows with invalid HMAC" do
        (ourId, k, _, junk, msg) <- genContent
        -- Pick part of the message to scribble
        let msgLen = MV.length $ getBuf msg
        -- Must be counter or below
        idx <- forAll $ Gen.int (Range.linear 8 (msgLen - 1))

        -- Do the scribble
        annotateShow =<< V.freeze (getBuf msg)
        MV.modify (getBuf msg) (xor 0x01) idx
        annotateShow =<< V.freeze (getBuf msg)

        -- Check
        flowMap <- evalIO M.newIO
        flow <- mkFlow (Gen.constant k)
        atomically $ M.insert flow ourId flowMap
        OutgoingActions{dispatched, sent} <- evalIO $
            checkProcIncoming flowMap epoch junk (getBuf msg)
        dispatched === 0
        sent === 0

    xit "ignores random bytes" $ hedgehog do
        randomBytes <- forAll $ Gen.list
            (Range.linear 0 (fromIntegral Const.mtu + 1))
            (Gen.word8 Range.linearBounded)
        let buf = V.fromList randomBytes
        mbuf <- V.unsafeThaw buf
        bufInJunk <- evalIO $ (`copyIntoJunkBuffer` mbuf) =<< newJunkBuffer
        flowMap <- evalIO M.newIO
        OutgoingActions{dispatched, sent} <- evalIO $
            uncurry (checkProcIncoming flowMap epoch) (fromJunkBuffer bufInJunk)
        dispatched === 0
        sent === 0

eqFlowSpec :: Spec
eqFlowSpec = describe "eqFlow" do
    it "is reflexive" do
        flow <- mkFlow genAeadKey
        res <- evalIO $ eqFlow flow flow
        res === True

windowSpec :: Spec
windowSpec = describe "Window" do
    it "can be empty" $ hedgehog do
        i <- forAll $ Gen.word32 Range.linearBounded
        w <- evalIO newWindow
        m <- evalIO $ windowMember i w
        m === False

    it "remembers what is set" $ hedgehog do
        i <- forAll $ Gen.word32 Range.linearBounded
        w <- evalIO newWindow
        s0 <- evalIO $ windowSet i w
        s0 === True
        annotate =<< evalIO (describeWindow w)
        m0 <- evalIO $ windowMember i w
        m0 === True

    it "can go forwards" $ hedgehog do
        i0 <- forAll $ Gen.word32 Range.linearBounded
        i1 <- forAll $ Gen.word32 (Range.linear i0 maxBound)
        w <- evalIO newWindow
        s0 <- evalIO $ windowSet i0 w
        s0 === True
        s1 <- evalIO $ windowSet i1 w
        s1 === True
        m0 <- evalIO $ windowMember i0 w
        m0 === True
        m1 <- evalIO $ windowMember i1 w
        m1 === True

    it "cannot go backwards past its length" $ hedgehog do
        i1 <- forAll $ Gen.word32 (Range.linear 0 (maxBound - windowLength))
        i0 <- forAll $ Gen.word32 (Range.linear (i1 + windowLength) maxBound)
        w <- evalIO newWindow
        s0 <- evalIO $ windowSet i0 w
        s0 === True
        s1 <- evalIO $ windowSet i1 w
        s1 === False

    it "can estimate its high point" $ hedgehog do
        i <- forAll $ Gen.word32 (Range.linear 0 maxBound)
        w <- evalIO newWindow
        -- Ensure that the higher value is used
        evalIO . when (i > 0) . void $ windowSet (i - 1) w
        _ <- evalIO $ windowSet i w
        r <- evalIO $ windowHigh w
        diff r (>=) i
        case i > windowLength of
            True  -> r === i
            False -> diff r (<) windowLength

splitKeySpec :: Spec
splitKeySpec = describe "splitKey" do
    it "is deterministic" $ hedgehog do
        ck <- forAll genSharedSecret
        (ir0, ri0) <- evalIO $ splitKey ck Initiator
        (ri1, ir1) <- evalIO $ splitKey ck Responder
        diff ir0 eqMem ir1
        diff ri0 eqMem ri1

spec :: Spec
spec = modifyMaxSuccess (const 1000) . parallel $
    procOutgoingSpec *> procIncomingSpec *> eqFlowSpec *> windowSpec *> splitKeySpec
