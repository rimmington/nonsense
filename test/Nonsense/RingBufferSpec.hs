{-# language StrictData #-}

module Nonsense.RingBufferSpec (spec) where

import Data.Vector.Storable (freeze, toList)
import Data.Vector.Storable.Mutable (set)
import Hedgehog.Gen qualified as Gen
import Hedgehog.Range qualified as Range
import Nonsense.RingBuffer
  (CommitResult (..), newAlignedPinnedRingBuffer, null, readVector, toRead, unsafeCommitRead,
  unsafeCommitWrite, unsafeReadPtr, writeVector)
import RIO hiding (null, set, toList)
import Test.Hspec
import Test.Hspec.Hedgehog

data BufferSpec
  = BufferSpec
    { blockS :: Int
    , capS   :: Int
    }
  deriving (Show)

genBuffer :: (MonadGen m) => m BufferSpec
genBuffer = do
    blockS <- Gen.int $ Range.linear 1 1024
    capS <- Gen.int $ Range.linear 1 1024
    pure BufferSpec{..}

spec :: Spec
spec = modifyMaxSuccess (const 1000) $ do
    describe "unsafeCommitWrite" $ do
        it "works on an empty buffer" $ hedgehog $ do
            BufferSpec{blockS, capS} <- forAll genBuffer
            buf <- evalIO $ newAlignedPinnedRingBuffer blockS capS
            res <- evalIO $ unsafeCommitWrite buf
            res === Committed

        it "does not work on a full buffer" $ hedgehog $ do
            BufferSpec{blockS, capS} <- forAll genBuffer
            buf <- evalIO $ newAlignedPinnedRingBuffer blockS capS
            evalIO . replicateM_ capS $ unsafeCommitWrite buf
            res <- evalIO $ unsafeCommitWrite buf
            res === Full

    describe "unsafeReadPtr" $ do
        it "does not work on an empty buffer" $ hedgehog $ do
            BufferSpec{blockS, capS} <- forAll genBuffer
            buf <- evalIO $ newAlignedPinnedRingBuffer blockS capS
            res <- evalIO $ unsafeReadPtr buf
            res === Nothing

        it "works on buffers with content" $ hedgehog $ do
            BufferSpec{blockS, capS} <- forAll genBuffer
            buf <- evalIO $ newAlignedPinnedRingBuffer blockS capS
            upto <- forAll $ Gen.int (Range.linear 1 capS)
            evalIO . replicateM_ upto $ unsafeCommitWrite buf
            res <- evalIO $ unsafeReadPtr buf
            void $ evalMaybe res

    describe "null" $ do
        it "returns True on an empty buffer" $ hedgehog $ do
            BufferSpec{blockS, capS} <- forAll genBuffer
            buf <- evalIO $ newAlignedPinnedRingBuffer blockS capS
            res <- evalIO $ null buf
            res === True

        it "returns False on a non-empty buffer" $ hedgehog $ do
            BufferSpec{blockS, capS} <- forAll genBuffer
            buf <- evalIO $ newAlignedPinnedRingBuffer blockS capS
            upto <- forAll $ Gen.int (Range.linear 1 capS)
            evalIO . replicateM_ upto $ unsafeCommitWrite buf
            res <- evalIO $ null buf
            res === False

    describe "toRead" do
        it "is accurate" $ hedgehog do
            BufferSpec{blockS, capS} <- forAll genBuffer
            buf <- evalIO $ newAlignedPinnedRingBuffer blockS capS
            writeTo <- forAll $ Gen.int (Range.linear 0 capS)
            readTo <- forAll $ Gen.int (Range.linear 0 writeTo)
            evalIO . replicateM_ writeTo $ unsafeCommitWrite buf
            evalIO . replicateM_ readTo $ unsafeCommitRead buf
            res <- evalIO $ toRead buf
            res === writeTo - readTo

    describe "readVector" do
        it "returns Nothing for an empty buffer" $ hedgehog do
            BufferSpec{blockS, capS} <- forAll genBuffer
            buf <- evalIO $ newAlignedPinnedRingBuffer blockS capS
            res <- evalIO $ readVector buf
            maybe (pure ()) (const failure) res

    describe "RingBuffer" $ do
        it "reads back data that was written" $ hedgehog $ do
            BufferSpec{blockS, capS} <- forAll genBuffer
            buf <- evalIO $ newAlignedPinnedRingBuffer blockS capS
            bytes <- forAll . Gen.list (Range.linear 1 capS) $ Gen.word8 Range.linearBounded
            forM_ bytes $ \byte -> do
                vec <- evalIO $ writeVector buf
                set vec byte
                res <- evalIO $ unsafeCommitWrite buf
                res === Committed
            forM_ bytes $ \expected -> do
                mvec <- evalIO $ readVector buf
                vec <- maybe failure pure mvec
                fvec <- freeze vec
                let read = toList fvec
                length read === blockS
                read === replicate blockS expected
                evalIO $ unsafeCommitRead buf
