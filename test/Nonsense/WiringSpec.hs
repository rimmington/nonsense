module Nonsense.WiringSpec (spec) where

import Data.List (intercalate, sort)
import Data.Vector qualified as B
import Data.Vector.Storable qualified as V
import Data.Vector.Storable.Mutable qualified as VM
import Hedgehog.Gen qualified as Gen
import Hedgehog.Range qualified as Range
import ListT qualified
import Network.Socket (SockAddr (SockAddrInet), tupleToHostAddress)
import Nonsense.Address (AddrKey (..))
import Nonsense.Bytes (Buffer)
import Nonsense.Ctl (RemoteInfo (..), timeoutFlowDiscard)
import Nonsense.CtlSpec (known)
import Nonsense.Ip6Addr (Ip6Lo (..), ip6LoAddr)
import Nonsense.MonoTime (MonoTime32 (MonoTime32), ms32)
import Nonsense.Packet (V6Buffer, copyIntoJunkBuffer, fromJunkBuffer, getBuf)
import Nonsense.PacketSpec (buildV6, genHelo, genV6BufferWithAddrs)
import Nonsense.Proc (describeFlow)
import Nonsense.Wiring (Wiring, ctl, flowIdMap, nextIn, nextOut, procIn, procOut, seek, sneak, wire)
import RIO
import RIO.List (sortOn)
import RIO.Map qualified as Map
import StmContainers.Map qualified as M
import Test.Hspec
import Test.Hspec.Hedgehog

addr1, addr2 :: SockAddr
addr1 = SockAddrInet 1234 $ tupleToHostAddress (127, 0, 0, 1)
addr2 = SockAddrInet 1234 $ tupleToHostAddress (127, 0, 0, 2)

ak1, ak2 :: AddrKey
((_, ak1), (_, ak2)) = case Map.toAscList known of
    (v1:v2:_) -> (v1, v2)
    _         -> error "not enough known"
{-# NOINLINE ak1 #-}
{-# NOINLINE ak2 #-}

constRemote :: SockAddr -> AddrKey -> Ip6Lo -> IO (Maybe RemoteInfo)
constRemote riSockAddr a _ = pure $ Just RemoteInfo
    { riStKey = akPub a
    , ..}

wireSpec :: Spec
wireSpec = describe "wire" do
    it "responds to HELO" do
        (_, _, _, msg) <- genHelo
        sentInV <- newIORef []
        sentOutV <- newIORef []
        let
            sendIn :: V6Buffer -> IO ()
            sendIn v6 = modifyIORef' sentInV . (:) =<< V.freeze (getBuf v6)
            sendOut :: SockAddr -> Buffer -> IO ()
            sendOut dest buf = modifyIORef' sentOutV . (:) . (dest,) =<< V.freeze (getBuf buf)
        wiring <- evalIO $ wire ak1 sendOut (constRemote addr2 ak2) id
        junk <- evalIO $ nextIn wiring
        (fromJunkBuffer -> (_, msgInJunk)) <- evalIO $ copyIntoJunkBuffer junk msg
        evalIO $ procIn wiring sendIn (pure ()) (MonoTime32 0) addr1 (getBuf msgInJunk)
        _ <- evalIO $ ctl wiring (pure (MonoTime32 1))
        sent <- readIORef sentOutV
        length sent === 1

    it "works in a simple scenario" do
        let
            sendIn :: IORef [V.Vector Word8] -> V6Buffer -> IO ()
            sendIn v v6 = modifyIORef' v . (:) =<< V.freeze (getBuf v6)
            sendOut :: SockAddr -> SockAddr -> IORef [(SockAddr, V.Vector Word8)]
                -> SockAddr -> Buffer -> IO ()
            sendOut from tAddr v dest buf
              | dest == tAddr = modifyIORef' v . (:) . (from,) =<< V.freeze (getBuf buf)
              | otherwise     = error $ "tried to send to " <> show dest
        (msgTo2, _, _, fromJunkBuffer -> (_, bufTo2)) <- genV6BufferWithAddrs
            (Gen.constant (ip6LoAddr (akLo ak1)))
            (const $ Gen.constant (ip6LoAddr (akLo ak2)))

        to1 <- newIORef []
        kernel1 <- newIORef []
        to2 <- newIORef []
        kernel2 <- newIORef []
        node1 <- evalIO $ wire ak1 (sendOut addr1 addr2 to2) (constRemote addr2 ak2) id
        node2 <- evalIO $ wire ak2 (sendOut addr2 addr1 to1) (constRemote addr1 ak1) id

        let
            pumpIn :: IORef [(SockAddr, V.Vector Word8)] -> Wiring -> (V6Buffer -> IO ()) -> MonoTime32 -> PropertyT IO ()
            pumpIn v w s now = do
                (readIORef v >>=) . mapM_ $ \(from, vec) -> do
                    annotateShow (now, from, vec)
                    evalIO do
                        junk <- nextIn w
                        mvec <- V.unsafeThaw vec
                        (fromJunkBuffer -> (_, inJunk)) <- copyIntoJunkBuffer junk mvec
                        procIn w s (pure ()) now from inJunk
                writeIORef v []
            cycle now = do
                pumpIn to1 node1 (sendIn kernel1) now
                pumpIn to2 node2 (sendIn kernel2) now
                void . evalIO . ctl node1 . pure . MonoTime32 $ ms32 now + 10
                void . evalIO . ctl node2 . pure . MonoTime32 $ ms32 now + 10

        junk <- evalIO $ nextOut node1
        (fromJunkBuffer -> (_, inJunk)) <- evalIO $ copyIntoJunkBuffer junk (getBuf bufTo2)
        evalIO $ procOut node1 (pure ()) (MonoTime32 0) inJunk
        for_ ([10, 100..800] <> [2000, 120000]) $ cycle . MonoTime32
        let final = MonoTime32 $ timeoutFlowDiscard + 410
        cycle final

        got1 <- readIORef kernel1
        got2 <- readIORef kernel2
        got1 === []
        got2 === [msgTo2]

        flows1 <- evalIO . atomically . ListT.toReverseList . M.listT $ flowIdMap node1
        annotateShow flows1
        length flows1 === 0
        flows2 <- evalIO . atomically . ListT.toReverseList . M.listT $ flowIdMap node2
        annotateShow flows2
        length flows2 === 0

    modifyMaxSuccess (const 1000) $ it "consistently transmits a number of packets" $ hedgehog do
        let simTime = 180
            packetCount = 3
            sendIn :: IORef (B.Vector (V.Vector Word8)) -> V6Buffer -> IO ()
            sendIn v v6 = modifyIORef' v . flip B.snoc =<< V.freeze (getBuf v6)
            sendOut :: SockAddr -> SockAddr -> IORef (B.Vector (SockAddr, V.Vector Word8))
                -> SockAddr -> Buffer -> IO ()
            sendOut from tAddr v dest buf
              | dest == tAddr = modifyIORef' v . flip B.snoc . (from,) =<< V.freeze (getBuf buf)
              | otherwise     = error $ "tried to send to " <> show dest
        packetTimes <- forAll $ fmap (sortOn snd) . Gen.shuffle . zip [1..packetCount] =<<
            Gen.list (Range.singleton packetCount) (Gen.int (Range.linear 1 (simTime - 10)))
        packetsWithTimes <- evalIO . for packetTimes $ \(i, time) ->
            (time,) <$> buildV6 (ip6LoAddr (akLo ak1)) (ip6LoAddr (akLo ak2)) (replicate time (fromIntegral i))

        to1 <- newIORef mempty
        kernel1 <- newIORef mempty
        to2 <- newIORef mempty
        kernel2 <- newIORef mempty
        node1 <- evalIO $
            wire ak1 (sendOut addr1 addr2 to2) (constRemote addr2 ak2) id
        node2 <- evalIO $
            wire ak2 (sendOut addr2 addr1 to1) (constRemote addr1 ak1) id

        let
            pumpIn :: IORef (B.Vector (SockAddr, V.Vector Word8)) -> Wiring -> (V6Buffer -> IO ()) -> MonoTime32 -> PropertyT IO ()
            pumpIn v w s now = do
                (readIORef v >>=) . mapM_ $ \(from, vec) -> do
                    evalIO do
                        junk <- nextIn w
                        mvec <- V.unsafeThaw vec
                        (fromJunkBuffer -> (_, inJunk)) <- copyIntoJunkBuffer junk mvec
                        procIn w s (pure ()) now from inJunk
                    n1 <- evalIO $ atomically (ListT.toList . M.listT $ flowIdMap node1) >>= traverse \(_, flow) ->
                        describeFlow flow
                    n2 <- evalIO $ atomically (ListT.toList . M.listT $ flowIdMap node2) >>= traverse \(_, flow) ->
                        describeFlow flow
                    annotate $ intercalate "\n"
                        [ show (now, from, vec)
                        , intercalate "\n" n1
                        , intercalate "\n" n2
                        ]
                writeIORef v mempty

        for_ [1..simTime] $ \step -> do
            let now = MonoTime32 . fromIntegral $ step * 1e3
            for_ packetsWithTimes $ \(t, (_, msg)) -> do
                when (t == step) $ evalIO do
                    junk <- nextOut node1
                    (fromJunkBuffer -> (_, inJunk)) <- copyIntoJunkBuffer junk msg
                    procOut node1 (pure ()) now (getBuf inJunk)
            pumpIn to1 node1 (sendIn kernel1) now
            pumpIn to2 node2 (sendIn kernel2) now
            void . evalIO . ctl node1 . pure . MonoTime32 $ ms32 now + 10
            void . evalIO . ctl node2 . pure . MonoTime32 $ ms32 now + 10

        got1 <- readIORef kernel1
        got2 <- readIORef kernel2
        toList got1 === []
        sort (toList got2) === sort (fst . snd <$> packetsWithTimes)

sneakSpec :: Spec
sneakSpec = describe "sneak and seek" do
    it "round-trips" $ hedgehog do
        addr <- forAll $ do
            b0 <- Gen.word8 Range.linearBounded
            b1 <- Gen.word8 Range.linearBounded
            b2 <- Gen.word8 Range.linearBounded
            b3 <- Gen.word8 Range.linearBounded
            pure $ tupleToHostAddress (b0, b1, b2, b3)
        port <- forAll $ fromIntegral <$> Gen.word16 Range.linearBounded
        let sockAddr = SockAddrInet port addr
        len <- forAll $ Gen.int (Range.linear 0 65535)
        buf <- VM.unsafeNew 100
        evalIO $ sneak sockAddr len buf
        (sockAddr', len') <- evalIO $ seek buf
        (sockAddr', len') === (sockAddr, len)

spec :: Spec
spec = parallel $ sneakSpec *> wireSpec
