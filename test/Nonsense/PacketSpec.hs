module Nonsense.PacketSpec
  ( genIp6Addr, genValidAddr, genFlowId, genPubKeyBytes
  , genHelo, genTher, genGnrl, genContent, genKeepalive
  , genV6BufferWithAddrs, buildV6
  , cl, cl'
  , spec
  ) where

import Data.Bits (shiftR, xor)
import Data.Coerce (Coercible, coerce)
import Data.Vector.Storable qualified as V
import Data.Vector.Storable.Mutable qualified as VM
import Hedgehog.Gen qualified as Gen
import Hedgehog.Range qualified as Range
import Nonsense.AEAD (AeadKey (..), keyCipher)
import Nonsense.AEADSpec (genAeadKey)
import Nonsense.Bytes qualified as B
import Nonsense.Cipher (Counter, initCipher)
import Nonsense.Constants qualified as Const
import Nonsense.ECDH (PubKeyBytes (..), keyLength)
import Nonsense.Ip6Addr (Ip6Addr (..))
import Nonsense.K12 (Hash (..), hmacLength)
import Nonsense.Packet
import RIO
import Test.Hspec
import Test.Hspec.Hedgehog

genFlowId :: MonadGen m => m FlowId
genFlowId = FlowId <$> Gen.word32 Range.linearBounded

genIp6Addr :: MonadGen m => m Ip6Addr
genIp6Addr = Ip6Addr <$> Gen.word64 Range.linearBounded <*> Gen.word64 Range.linearBounded

genValidAddr :: Gen Ip6Addr
genValidAddr = Ip6Addr <$> Gen.constant Const.subnet <*> Gen.word64 Range.linearBounded

genPubKeyBytes :: MonadGen m => m (PubKeyBytes ByteString)
genPubKeyBytes = PubKeyBytes . B.unsafeAtLeast <$> Gen.bytes (Range.singleton keyLength)

genV6BufferWithAddrs ::
    Gen Ip6Addr
 -> (Ip6Addr -> Gen Ip6Addr)
 -> PropertyT IO (V.Vector Word8, Ip6Addr, Ip6Addr, FromJunkBuffer V6Buffer)
genV6BufferWithAddrs genSrc genDest = do
    src <- forAll genSrc
    dst <- forAll $ genDest src
    dataBytes <- forAll $ Gen.list
        (Range.linear 0 1240)
        (Gen.word8 Range.linearBounded)
    (bytes, msg) <- evalIO $ buildV6 src dst dataBytes
    junk <- liftIO newJunkBuffer
    evalIO $ (bytes,src,dst,) <$> copyIntoJunkBuffer junk msg

buildV6 :: Ip6Addr -> Ip6Addr -> [Word8] -> IO (V.Vector Word8, V6Buffer)
buildV6 src dst dataBytes =
    maybe (fail "buildV6 bug") (pure . (bytes,)) =<< mkV6Buffer =<< V.thaw bytes
  where
    bytes = V.fromList $
        take 4 hdrBytes ++ lenBytes ++ drop 4 hdrBytes ++ srcBytes ++ dstBytes ++ dataBytes
    srcBytes = V.toList $ V.unsafeCast @_ @Word8 $ V.fromList [srcHi, srcLo]
    dstBytes = V.toList $ V.unsafeCast @_ @Word8 $ V.fromList [dstHi, dstLo]
    Ip6Addr srcHi srcLo = src
    Ip6Addr dstHi dstLo = dst
    lenBytes = [fromIntegral (dataLen `shiftR` 8), fromIntegral dataLen]
    dataLen = length dataBytes
    -- 0x6 + traffic class (0x0) + flow label + next hdr + hop limit
    hdrBytes = case dataBytes of
        []        -> [0x60, 0x06, 8, 1, 23,  0]
        [a]       -> [0x60, 0x09, a, 4, 147, 0]
        [a, b]    -> [0x60, 0x09, a, b, 225, 0]
        (a:b:c:_) -> [0x60, 0x0C, a, b, c,   0]

genV6Buffer :: PropertyT IO (V.Vector Word8, FromJunkBuffer V6Buffer)
genV6Buffer = (\(a,_,_,b) -> (a, b)) <$>
    genV6BufferWithAddrs genIp6Addr (const genIp6Addr)

genHash :: (B.Bytes ba, B.Uninitialised ba) => PropertyT IO (Hash ba)
genHash = Hash . B.unsafeAtLeast . B.pack <$>
    forAll (Gen.list (Range.singleton hmacLength) (Gen.word8 Range.linearBounded))

genHelo :: PropertyT IO (FlowId, PubKeyBytes ByteString, JunkBuffer, HeloMsg)
genHelo = do
    ourId <- forAll genFlowId
    ourEphPubKey <- forAll genPubKeyBytes
    junk <- liftIO newJunkBuffer
    msg <- liftIO $ buildHelo junk ourId ourEphPubKey
    pure (ourId, ourEphPubKey, junk, msg)

genTher ::
    PropertyT IO (FlowId, PubKeyBytes ByteString, FlowId, AeadKey, Hash ByteString, JunkBuffer, TherMsg)
genTher = do
    ourId <- forAll genFlowId
    theirId <- forAll genFlowId
    ourEphPubKey <- forAll genPubKeyBytes
    junk <- liftIO newJunkBuffer
    k <- forAll genAeadKey
    h <- genHash @ByteString
    msg <- liftIO $ buildTher junk ourId ourEphPubKey theirId k h
    pure (ourId, ourEphPubKey, theirId, k, h, junk, msg)

genGnrl ::
    PropertyT IO (FlowId, PubKeyBytes ByteString, AeadKey, Hash ByteString, JunkBuffer, GnrlMsg)
genGnrl = do
    theirId <- forAll genFlowId
    ourStPubKey <- forAll genPubKeyBytes
    k <- forAll genAeadKey
    h <- genHash
    junk <- liftIO newJunkBuffer
    cipher <- evalIO $ initCipher >>= \c -> c <$ keyCipher c k
    lt <- evalIO $ buildLt junk ourStPubKey theirId k cipher h
    msg <- liftIO $ buildGnrl k h lt
    pure (theirId, ourStPubKey, k, h, junk, msg)

genContent :: PropertyT IO (FlowId, AeadKey, Counter, JunkBuffer, ContentMsg)
genContent = do
    (_, fromJunkBuffer -> (junk, v6)) <- genV6Buffer
    theirId <- forAll genFlowId
    k <- forAll genAeadKey
    count <- forAll $ Gen.integral Range.linearBounded
    cipher <- evalIO initCipher
    evalIO $ keyCipher cipher k
    msg <- liftIO $ overwriteV6ToContent junk v6 theirId k cipher count
    pure (theirId, k, count, junk, msg)

genKeepalive :: PropertyT IO (FlowId, AeadKey, Counter, JunkBuffer, KeepaliveMsg)
genKeepalive = do
    theirId <- forAll genFlowId
    k <- forAll genAeadKey
    count <- forAll $ Gen.integral Range.linearBounded
    junk <- liftIO newJunkBuffer
    msg <- liftIO $ buildKeepalive junk theirId k count
    pure (theirId, k, count, junk, msg)

spec :: Spec
spec = modifyMaxSuccess (const 1000) $ do
    describe "buildHelo" $ do
        it "passes mkGotMsg" $ hedgehog $ do
            (_, _, _, msg) <- genHelo
            got <- liftIO . mkGotMsg $ getBuf msg
            case got of
                Just (Helo _) -> pure ()
                Just _        -> fail "parsed as non-Helo"
                Nothing       -> fail "did not parse"
        it "preserves values" $ hedgehog $ do
            (ourId, ourEphPubKey, _, msg) <- genHelo
            (ourId ===) =<< liftIO (heloTheirId msg)
            (ourEphPubKey ===) =<< liftIO (clpk (heloTheirEphPubkey msg))
        it "is padded with zeros" $ hedgehog do
            (_, _, _, msg) <- genHelo
            pad <- V.freeze $ VM.slice 40 20 (getBuf msg)
            pad === V.replicate 20 0

    describe "buildTher" $ do
        it "passes mkGotMsg" $ hedgehog $ do
            (_, _, _, _, _, _, msg) <- genTher
            got <- liftIO . mkGotMsg $ getBuf msg
            case got of
                Just (Ther _) -> pure ()
                Just _        -> fail "parsed as non-Ther"
                Nothing       -> fail "did not parse"
        it "preserves values" $ hedgehog $ do
            (ourId, ourEphPubKey, theirId, _, _, _, msg) <- genTher
            (ourId ===) =<< liftIO (therTheirId msg)
            (ourEphPubKey ===) =<< liftIO (clpk (therTheirEphPubkey msg))
            (theirId ===) =<< liftIO (therOurId msg)

    describe "therCheckHmac" do
        it "passes" do
            (_, _, _, k, h, _, msg) <- genTher
            res <- evalIO $ therCheckHmac k h msg (pure False) (pure True)
            res === True
        it "doesn't pass if scribbled" do
            (_, _, _, k, h, _, msg) <- genTher
            -- Pick part of the message to scribble
            let msgLen = VM.length $ getBuf msg
            -- Must be within HMAC
            idx <- forAll $ Gen.int (Range.linear 44 (msgLen - 1))

            -- Do the scribble
            annotateShow =<< V.freeze (getBuf msg)
            VM.modify (getBuf msg) (xor 0x01) idx
            annotateShow =<< V.freeze (getBuf msg)

            -- Check
            res <- evalIO $ therCheckHmac k h msg (pure False) (pure True)
            res === False

    describe "buildGnrl" do
        it "passes mkGotMsg" $ hedgehog do
            (_, _, _, _, _, msg) <- genGnrl
            got <- liftIO . mkGotMsg $ getBuf msg
            case got of
                Just (Gnrl _) -> pure ()
                Just _        -> fail "parsed as non-Gnrl"
                Nothing       -> fail "did not parse"
        it "preserves values" $ hedgehog do
            (ourId, ourStPubKey, k, h, _, msg) <- genGnrl
            (ourId ===) =<< liftIO (gnrlOurId msg)
            cipher <- evalIO $ initCipher >>= \c -> c <$ keyCipher c k
            pkm <- evalIO $ gnrlConsumeTsuk k cipher h msg
            pk <- maybe failure pure pkm
            (ourStPubKey ===) =<< liftIO (clpk pk)

    describe "gnrlConsumeTsuk" do
        it "doesn't pass if scribbled" do
            (_, _, k, h, _, msg) <- genGnrl
            -- Pick part of the message to scribble
            -- Must be within key or HMAC
            idx <- forAll $ Gen.int (Range.linear 8 55)

            -- Do the scribble
            annotateShow =<< V.freeze (getBuf msg)
            VM.modify (getBuf msg) (xor 0x01) idx
            annotateShow =<< V.freeze (getBuf msg)

            -- Check
            cipher <- evalIO $ initCipher >>= \c -> c <$ keyCipher c k
            res <- evalIO $ gnrlConsumeTsuk k cipher h msg
            maybe (pure ()) (const failure) res

    describe "gnrlCheck2ndHmac" do
        it "passes" do
            (_, _, k, h, _, msg) <- genGnrl
            res <- evalIO $ gnrlCheck2ndHmac k h msg (pure False) (pure True)
            res === True
        it "doesn't pass if scribbled" do
            (_, _, k, h, _, msg) <- genGnrl
            -- Pick part of the message to scribble
            let msgLen = VM.length $ getBuf msg
            -- Must be within HMAC
            idx <- forAll $ Gen.int (Range.linear 56 (msgLen - 1))

            -- Do the scribble
            annotateShow =<< V.freeze (getBuf msg)
            VM.modify (getBuf msg) (xor 0x01) idx
            annotateShow =<< V.freeze (getBuf msg)

            -- Check
            res <- evalIO $ gnrlCheck2ndHmac k h msg (pure False) (pure True)
            res === False

    describe "buildKeepalive" $ do
        it "passes mkGotMsg" $ hedgehog $ do
            (_, _, _, _, msg) <- genKeepalive
            got <- liftIO . mkGotMsg $ getBuf msg
            case got of
                Just (Keepalive _) -> pure ()
                Just _             -> fail "parsed as non-Keepalive"
                Nothing            -> fail "did not parse"
        it "preserves values" $ hedgehog $ do
            (theirId, _, count, _, msg) <- genKeepalive
            (theirId ===) =<< liftIO (keepaliveOurId msg)
            (count ===) =<< liftIO (keepaliveCounter msg)

    describe "keepaliveCheckHmac" do
        it "passes" do
            (_, k, c, _, msg) <- genKeepalive
            res <- evalIO $ keepaliveCheckHmac c k msg (pure False) (pure True)
            res === True

        it "doesn't pass if scribbled" do
            (_, k, c, _, msg) <- genKeepalive
            -- Pick part of the message to scribble
            let msgLen = VM.length $ getBuf msg
            -- Must be within HMAC
            idx <- forAll $ Gen.int (Range.linear 12 (msgLen - 1))

            -- Do the scribble
            annotateShow =<< V.freeze (getBuf msg)
            VM.modify (getBuf msg) (xor 0x01) idx
            annotateShow =<< V.freeze (getBuf msg)

            -- Check
            res <- evalIO $ keepaliveCheckHmac c k msg (pure False) (pure True)
            res === False

    describe "overwriteV6ToContent" $ do
        it "passes mkGotMsg" $ hedgehog $ do
            (_, _, _, _, msg) <- genContent
            got <- evalIO . mkGotMsg $ getBuf msg
            case got of
                Just (Content _) -> pure ()
                Just _           -> fail "parsed as non-Content"
                Nothing          -> fail "did not parse"
        it "preserves values" $ hedgehog $ do
            (theirId, _, count, _, msg) <- genContent
            (theirId ===) =<< liftIO (contentOurId msg)
            (count ===) =<< liftIO (contentCounter msg)

    describe "overwriteContentToV6" do
        it "reverses overwriteV6ToContent" do
            (bytes, fromJunkBuffer -> (junk, v6)) <- genV6Buffer
            k <- forAll genAeadKey
            count <- forAll $ Gen.integral Range.linearBounded
            cipher <- evalIO initCipher
            evalIO $ keyCipher cipher k
            src <- liftIO $ v6SourceAddr v6
            dst <- liftIO $ v6DestAddr v6
            msg <- liftIO $ overwriteV6ToContent junk v6 (FlowId 0) k cipher count
            annotateShow =<< liftIO (V.freeze $ getBuf msg)
            (fromJunkBuffer -> (junk2, msg2)) <- evalIO $
                (`copyIntoJunkBuffer` msg) =<< newJunkBuffer
            res <- liftIO $ overwriteContentToV6 junk2 msg2 dst src k cipher count
            end <- maybe failure pure res
            end' <- liftIO . V.unsafeFreeze $ getBuf end
            bytes === end'
        it "rejects scribbed messages" do
            (_, fromJunkBuffer -> (junk, v6)) <- genV6Buffer
            k <- forAll genAeadKey
            count <- forAll $ Gen.integral Range.linearBounded
            cipher <- evalIO initCipher
            evalIO $ keyCipher cipher k
            src <- liftIO $ v6SourceAddr v6
            dst <- liftIO $ v6DestAddr v6
            msg <- liftIO $ overwriteV6ToContent junk v6 (FlowId 0) k cipher count

            -- Pick part of the message to scribble
            let msgLen = VM.length $ getBuf msg
            -- Must be v6 header or below
            idx <- forAll $ Gen.int (Range.linear 12 (msgLen - 1))

            -- Do the scribble
            annotateShow =<< V.freeze (getBuf msg)
            VM.modify (getBuf msg) (xor 0x01) idx
            annotateShow =<< V.freeze (getBuf msg)

            -- Check
            res <- liftIO $ overwriteContentToV6 junk msg dst src k cipher count
            case res of
                Nothing -> pure ()
                Just _  -> failure

cl' ::
    forall bb n a b ba.
    ( B.Bytes ba
    , B.Bytes bb
    , B.Uninitialised bb
    , Coercible a (B.AtLeast n ba)
    , Coercible b (B.AtLeast n bb))
 => (B.AtLeast n ba -> a) -> Proxy bb -> a -> IO b
cl' _ _ a = coerce $ B.cloneAtLeast @bb (coerce @_ @(B.AtLeast n ba) a)

cl ::
    forall n a ba.
    (B.Bytes ba, B.Uninitialised ba, Coercible a (B.AtLeast n ba))
 => (B.AtLeast n ba -> a) -> a -> IO a
cl f = cl' f (Proxy @ba)

clpk :: B.Bytes ba => PubKeyBytes ba -> IO (PubKeyBytes ByteString)
clpk = cl' PubKeyBytes (Proxy @ByteString)
