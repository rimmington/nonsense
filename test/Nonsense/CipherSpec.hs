module Nonsense.CipherSpec (spec) where

import Data.Vector.Storable qualified as V
import Hedgehog.Gen qualified as Gen
import Hedgehog.Range qualified as Range
import Nonsense.Bytes qualified as B
import Nonsense.Cipher (Counter (..), cipherInPlace, initCipher, setKey)
import Nonsense.ECDH (SharedSecret (..), keyLength)
import RIO
import Test.Hspec
import Test.Hspec.Hedgehog

spec :: Spec
spec = do
    describe "initCipher" do
        it "inits" do
            void initCipher

    describe "setKey" do
        it "can setKey" do
            c <- initCipher
            setKey c . SharedSecret . B.unsafeAtLeast $ B.pack [0..31]

    describe "cipherInPlace" do
        it "can encipher" do
            c <- initCipher
            let bs = B.unsafeAtLeast $ B.pack [0..31]
                n  = Counter 1
            setKey c $ SharedSecret bs
            buf <- B.clone @B.Buffer bs
            cipherInPlace c n buf
            bytes <- V.toList <$> V.freeze buf
            bytes `shouldBe` [182,16,201,102,19,223,175,149,101,234,232,116,57,170,28,78,201,128,23,1,93,116,128,140,132,255,98,211,0,228,195,141]

        it "round-trips" $ hedgehog do
            c <- evalIO initCipher
            key <- forAll $ Gen.list
                (Range.singleton keyLength)
                (Gen.word8 Range.linearBounded)
            nce <- forAll $ Gen.integral Range.linearBounded
            -- 0 bytes works fine, but then the /== check below will fail
            msg <- forAll $ Gen.list (Range.linear 1 2048) (Gen.word8 Range.linearBounded)

            evalIO $ setKey c . SharedSecret . B.unsafeAtLeast $ B.pack key
            buf <- evalIO $ V.thaw $ V.fromList msg

            evalIO $ cipherInPlace c nce buf
            cipheredMsg <- evalIO $ V.toList <$> V.freeze buf
            cipheredMsg /== msg

            evalIO $ cipherInPlace c nce buf
            twiceMsg <- evalIO $ V.toList <$> V.freeze buf
            twiceMsg === msg
