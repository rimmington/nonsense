{-# language OverloadedStrings, StrictData #-}

module Nonsense.CtlSpec (spec, known) where

import Data.List (permutations)
import Data.Primitive.PVar (newPVar, readPVar, writePVar)
import Data.Vector.Storable qualified as V
import Hedgehog.Gen qualified as Gen
import Hedgehog.Range qualified as Range
import ListT qualified
import Network.Socket (SockAddr (SockAddrUnix))
import Nonsense.Address (AddrKey (..), keyAddr)
import Nonsense.AEAD (AeadKey (AeadKey), keyCipher)
import Nonsense.AEADSpec (genAeadKey)
import Nonsense.Bytes (pack, unsafeAtLeast)
import Nonsense.Bytes qualified as B
import Nonsense.Cipher (initCipher)
import Nonsense.Constants qualified as Const
import Nonsense.Ctl
  (LastRecv (..), Preflow (..), PreflowId (..), RemoteInfo (RemoteInfo), ctlFlowTiming, ctlIncoming,
  ctlOutgoing, ctlPreflowTiming, mixHash, mixKey, nearMaxCount, timeoutFlowDiscard,
  timeoutFlowExpires, timeoutPreflowExpires, timeoutSendKeepalive, timeoutWantKeepalive)
import Nonsense.ECDH
  (KeyLength, PrivKey, PrivKeyBytes (..), PubKeyBytes (PubKeyBytes), SharedSecret (..),
  keyAgreement, keyLength, loadPrivKey, privKeyBytesGen, privPubKey)
import Nonsense.ECDHSpec (genGen, genPrivKey)
import Nonsense.Ip6Addr (Ip6Addr (..), Ip6Lo (..), addrIp6Lo, ip6LoAddr)
import Nonsense.K12 (Hash (..), hmacLength)
import Nonsense.K12Spec (eqMem, genSharedSecret)
import Nonsense.MonoTime (MonoTime32 (..))
import Nonsense.Packet
  (FlowId (..), GnrlMsg, GotMsg (..), JunkBuffer, TherMsg, asJunkBuffer, buildGnrl, buildLt,
  buildTher, copyIntoJunkBuffer, fromJunkBuffer, getBuf, junkBufferSize, ltTsukTagged, mkGotMsg,
  newJunkBuffer)
import Nonsense.PacketSpec
  (cl, genContent, genFlowId, genHelo, genIp6Addr, genKeepalive, genTher, genV6BufferWithAddrs,
  genValidAddr)
import Nonsense.Proc (Flow (..), Role (..), describeFlow, maxSendCount, windowSet)
import Nonsense.ProcSpec (genIp6Lo, mkFlow, mkFlow')
import Nonsense.Random (FrozenGen, GenST, freezeGen, thawGen, uniformW32)
import Nonsense.RingBuffer qualified as RB
import RIO
import RIO.Map qualified as Map
import StmContainers.Map qualified as M
import System.IO.Unsafe (unsafePerformIO)
import Test.Hspec
import Test.Hspec.Hedgehog hiding (Seed)

data IncomingActions
  = IncomingActions
    { sent       :: [(SockAddr, B.Buffer)]
    , registered :: [(FlowId, Flow)]
    , preflow    :: [(PreflowId, Preflow)]
    }

data OutgoingActions
  = OutgoingActions
    { sentOut       :: [(SockAddr, B.Buffer)]
    , registeredOut :: [(PreflowId, Preflow)]
    }

data TimingActions
  = TimingActions
    { expired       :: Bool
    , discarded     :: Bool
    , sentTiming    :: [(SockAddr, B.Buffer)]
    , preflowTiming :: [(PreflowId, Preflow)]
    }

newtype Rand
  = Rand (forall a. (forall s. GenST s -> ST s a) -> IO a)

frozenRand :: FrozenGen -> IO Rand
frozenRand gen = do
    genV <- newTVarIO gen
    let rand :: forall a. (forall s. GenST s -> ST s a) -> IO a
        rand f = atomically do
            g <- readTVar genV
            let (res, g') = runST do
                    gst <- thawGen g
                    (,) <$> f gst <*> freezeGen gst
            writeTVar genV g'
            pure res
    pure $ Rand rand

properRand :: PropertyT IO Rand
properRand = evalIO . frozenRand =<< forAll genGen

zeroSs :: B.AtLeast KeyLength ByteString
zeroSs = B.unsafeAtLeast . pack $ replicate keyLength 0

mkPreflow :: Ip6Lo -> PrivKey -> PropertyT IO Preflow
mkPreflow tAddr oerk = do
    began <- forAll $ MonoTime32 <$> Gen.word32 Range.linearBounded
    buffered <- evalIO $ RB.newAlignedPinnedRingBuffer junkBufferSize 10
    tId <- evalIO $ newPVar (FlowId 0)
    lastR <- evalIO $ newPVar LNone
    intKey <- evalIO $ AeadKey . snd <$> B.allocAtLeast (const $ pure ())
    ck <- evalIO $ SharedSecret <$> B.cloneAtLeast zeroSs
    tsukV <- evalIO . newTVarIO $ PubKeyBytes zeroSs
    h <- evalIO $ Hash <$> B.cloneAtLeast zeroSs
    pure Preflow{..}

noTsuk :: TVar (PubKeyBytes ByteString)
noTsuk = unsafePerformIO $ newTVarIO (error "noTsuk")
{-# NOINLINE noTsuk #-}

mirrorPf :: FlowId -> Ip6Lo -> PrivKey -> Preflow -> IO Preflow
mirrorPf ourId ourLo terk pf = do
    intKey' <- cl AeadKey $ intKey pf
    ck' <- cl SharedSecret $ ck pf
    h' <- cl Hash $ h pf
    pure Preflow
        { oerk     = terk
        , tAddr    = ourLo
        , tsukV    = noTsuk
        , tId      = unsafePerformIO (newPVar ourId)
        , buffered = unsafePerformIO (RB.newAlignedPinnedRingBuffer 0 0)
        , began    = began pf
        , lastR    = unsafePerformIO (newPVar LNone)
        , intKey   = intKey'
        , ck       = ck'
        , h        = h'
        }

mkValidGnrl ::
    B.Bytes ba
 => PrivKey
 -> Preflow
 -> FlowId
 -> PubKeyBytes ba
 -> IO GnrlMsg
mkValidGnrl osrk pf theirId teuk = do
    junk <- newJunkBuffer
    cipher <- initCipher >>= \c -> c <$ keyCipher c (intKey pf)
    se <- maybe (fail "no ka") pure $ keyAgreement osrk teuk
    lt <- buildLt junk (privPubKey osrk) theirId (intKey pf) cipher (h pf)
    mixHash pf (ltTsukTagged lt)
    mixKey pf se
    buildGnrl (intKey pf) (h pf) lt

mkValidTher ::
    B.Bytes ba
 => PrivKey
 -> Preflow
 -> PubKeyBytes ba
 -> FlowId
 -> IO TherMsg
mkValidTher osrk pf teuk theirId = do
    let oeuk = privPubKey $ oerk pf
        ourId = FlowId 1
    ee <- maybe (fail "no ka") pure $ keyAgreement (oerk pf) teuk
    es <- maybe (fail "no ka") pure $ keyAgreement osrk teuk
    mixHash pf oeuk
    mixKey pf ee
    mixKey pf es
    junk <- newJunkBuffer
    buildTher junk ourId oeuk theirId (intKey pf) (h pf)

-- | Fill RingBuffer as a collection of JunkBuffers.
fillRb :: RB.RingBuffer -> [B.Buffer] -> IO ()
fillRb rb ms = RB.null rb >>= \case
    True -> for_ ms $ \src -> do
        rbBuf <- RB.writeVector rb
        let (fromJunkBuffer -> (dst, _)) =
                asJunkBuffer (B.unsafeUncheckedAtLeast rbBuf)
        _ <- copyIntoJunkBuffer dst src
        RB.unsafeCommitWrite rb >>= \case
            RB.Committed -> pure ()
            RB.Full      -> error "ring buffer full"
    False -> error "ring buffer used"

known :: Map Ip6Lo AddrKey
known = Map.fromList $ load <$> rks
  where
    rks =
        [ [131,89,254,64,237,241,178,164,221,27,72,51,168,184,103,3,138,6,217,244,248,149,178,77,137,196,0,99,234,50,173,63]
        , [23,118,92,4,126,254,206,21,158,176,226,203,79,229,163,236,27,103,67,104,148,160,218,218,63,163,210,38,143,254,3,129]
        , [222,99,138,32,116,85,33,168,186,26,198,198,38,216,218,97,49,121,151,139,40,190,167,49,13,123,184,176,54,95,130,181]
        , [50,194,8,18,174,153,224,133,1,71,222,17,247,92,73,119,200,170,1,241,8,48,43,214,155,16,152,88,217,131,168,226]
        , [127,111,175,96,52,139,38,31,245,195,253,98,101,118,243,49,240,128,65,153,191,249,125,232,237,37,7,87,237,57,53,197]
        , [205,127,169,29,31,102,174,174,216,16,103,87,149,60,160,147,66,177,4,82,172,223,121,44,45,55,56,183,93,156,154,132]
        , [14,60,92,215,251,27,206,142,231,56,95,44,84,210,54,139,156,69,81,182,163,59,57,21,133,42,87,9,253,202,69,127]
        , [203,148,64,247,43,150,69,174,181,236,111,250,87,223,163,102,237,217,154,38,6,153,152,36,211,37,48,202,26,116,21,120]
        , [103,69,38,47,45,186,109,161,100,93,251,203,92,227,51,157,90,39,51,100,135,194,127,3,224,32,156,186,25,238,16,21]
        , [195,155,23,96,61,228,240,202,16,122,129,25,8,29,195,254,160,175,85,120,240,231,144,44,212,225,144,238,219,5,214,27]
        ]
    load bs = (lo, AddrKey rk uk lo)
      where
        rk = loadPrivKey . PrivKeyBytes . unsafeAtLeast $ pack bs
        uk = privPubKey rk
        lo = fromMaybe (error $ "known " <> show bs) (keyAddr uk)

genKnownAddrKey :: Gen AddrKey
genKnownAddrKey = Gen.element (Map.elems known)

genKnownAddr :: Gen Ip6Lo
genKnownAddr = Gen.element (Map.keys known)

genCheckCtlFlowTiming :: PropertyT IO (MonoTime32 -> [(PreflowId, Preflow)] -> Flow -> IO TimingActions)
genCheckCtlFlowTiming = do
    go <$> properRand
  where
    go (Rand rand) now alreadyP flow = do
        let fid = FlowId 0
        sentV <- newIORef []
        (loPre, idPre) <- atomically do
            p <- M.new
            i <- M.new
            for_ alreadyP \(k, v) -> M.insert v k i *> M.insert v (tAddr v) p
            pure (p, i)
        (loFlow, idFlow) <- atomically do
            p <- M.new
            i <- M.new
            M.insert flow fid i
            M.insert flow (theirAddr flow) p
            pure (p, i)
        junk <- newJunkBuffer
        ctlFlowTiming
            junk
            rand
            loPre
            idPre
            loFlow
            idFlow
            (const $ pure Nothing)
            (\lo -> flip (maybe (pure Nothing)) (Map.lookup lo known) \(AddrKey _ pub _) -> do
                sa <- readTVarIO (remoteAddr flow)
                pure . Just $ RemoteInfo sa pub
            )
            (\addr buf -> modifyIORef' sentV . (:) . (addr,) .
                snd . fromJunkBuffer =<< (`copyIntoJunkBuffer` buf) =<< newJunkBuffer)
            id
            now
            fid
            flow
        expired <- isNothing <$> atomically (M.lookup (theirAddr flow) loFlow)
        discarded <- isNothing <$> atomically (M.lookup fid idFlow)
        sentTiming <- readIORef sentV
        preflowTiming <- atomically . ListT.toList $ M.listT idPre
        pure TimingActions{..}

key :: PubKeyBytes ByteString
key = case Map.toList known of
    ((_, AddrKey _ pub _):_) -> pub
    _                        -> error "no known"

checkCtlOutgoing ::
    PropertyT IO Rand
 -> Gen Ip6Addr
 -> [(FlowId, Flow)]
 -> [(PreflowId, Preflow)]
 -> PropertyT IO OutgoingActions
checkCtlOutgoing randGen theirGen alreadyR alreadyP = do
    Rand rand <- randGen
    (_, _, _, fromJunkBuffer -> (junk, v6)) <- genV6BufferWithAddrs genValidAddr (const theirGen)
    junkHelo <- evalIO newJunkBuffer
    sentV <- newTVarIO []
    (loPre, idPre) <- atomically do
        p <- M.new
        i <- M.new
        for_ alreadyP \(k, v) -> M.insert v k i *> M.insert v (tAddr v) p
        pure (p, i)
    (loFlow, idFlow) <- atomically do
        p <- M.new
        i <- M.new
        for_ alreadyR \(k, v) -> M.insert v k i *> M.insert v (theirAddr v) p
        pure (p, i)
    cipher <- evalIO initCipher
    evalIO $ ctlOutgoing
        junkHelo
        cipher
        rand
        loPre
        idPre
        loFlow
        idFlow
        (const $ pure Nothing)
        (const . pure . pure $ RemoteInfo (SockAddrUnix "") key)
        (\addr send -> atomically $ modifyTVar sentV ((addr, send):))
        id
        (MonoTime32 0)
        junk
        (getBuf v6)
    sentOut <- readTVarIO sentV
    registeredOut <- atomically . ListT.toList $ M.listT idPre
    pure OutgoingActions{sentOut, registeredOut}

aStaticKp :: AddrKey
aStaticKp = AddrKey rk uk lo
  where
    rk = loadPrivKey . PrivKeyBytes . unsafePerformIO $ B.cloneAtLeast zeroSs
    uk = privPubKey rk
    lo = fromMaybe (error "loading aStaticKp") (keyAddr uk)

checkCtlIncoming ::
    Rand
 -> [(FlowId, Flow)]
 -> [(PreflowId, Preflow)]
 -> MonoTime32
 -> JunkBuffer
 -> B.Buffer
 -> IO (Either SomeException IncomingActions)
checkCtlIncoming (Rand rand) alreadyR alreadyP now junk buf = do
    sentV <- newTVarIO []
    (loPre, idPre) <- atomically do
        p <- M.new
        i <- M.new
        for_ alreadyP \(k, v) -> M.insert v k i *> M.insert v (tAddr v) p
        pure (p, i)
    (loFlow, idFlow) <- atomically do
        p <- M.new
        i <- M.new
        for_ alreadyR \(k, v) -> M.insert v k i *> M.insert v (theirAddr v) p
        pure (p, i)
    cipher <- initCipher
    scratch <- snd <$> B.allocAtLeast (const $ pure ())
    res <- tryAny $ ctlIncoming
        aStaticKp
        cipher
        rand
        loPre
        idPre
        loFlow
        idFlow
        (\addr send -> atomically $ modifyTVar sentV ((addr, send):))
        now
        (SockAddrUnix "")
        junk
        scratch
        buf
    sent <- readTVarIO sentV
    registered <- atomically . ListT.toList $ M.listT idFlow
    preflow <- atomically . ListT.toList $ M.listT idPre
    pure $ IncomingActions{sent, registered, preflow} <$ res

evalMkGotMsg :: (MonadTest m, MonadIO m, HasCallStack) => B.Buffer -> m GotMsg
evalMkGotMsg = maybe failure pure <=< evalIO . mkGotMsg

ctlFlowTimingSpec :: Spec
ctlFlowTimingSpec = describe "ctlFlowTiming" do
    it "removes expired flows from outbound map" do
        additional <- forAll $ Gen.word32 (Range.linear timeoutFlowExpires maxBound)
        goCheck <- genCheckCtlFlowTiming
        flow <- mkFlow genAeadKey
        let now = MonoTime32 $ ms32 (created flow) + additional
        TimingActions{..} <- evalIO $ goCheck now [] flow
        expired === True
        length sentTiming === 0
    it "removes expired flows from inbound map after additional time" do
        additional <- forAll $ Gen.word32 (Range.linear timeoutFlowDiscard maxBound)
        goCheck <- genCheckCtlFlowTiming
        flow <- mkFlow genAeadKey
        let now = MonoTime32 $ ms32 (created flow) + additional
        TimingActions{..} <- evalIO $ goCheck now [] flow
        expired === True
        discarded === True
        length sentTiming === 0
    it "does not remove unexpired flows" do
        additional <- forAll $ Gen.word32 (Range.linear 0 (timeoutFlowExpires - 1))
        goCheck <- genCheckCtlFlowTiming
        flow <- mkFlow genAeadKey
        let now = MonoTime32 $ ms32 (created flow) + additional
        TimingActions{..} <- evalIO $ goCheck now [] flow
        expired === False
        discarded === False
    it "sends keepalives" do
        sentAnyOff    <- forAll $ Gen.word32 (Range.linear 0 (timeoutFlowExpires - timeoutSendKeepalive - 1))
        sentYetAckOff <- forAll $ Gen.word32 (Range.linear 0 sentAnyOff)
        recvAnyOff    <- forAll $ Gen.word32 (Range.linear (sentAnyOff + 1) (timeoutFlowExpires - timeoutSendKeepalive - 1))
        recvYetAckOff <- forAll $ Gen.word32 (Range.linear (sentAnyOff + 1) recvAnyOff)
        goCheck <- genCheckCtlFlowTiming
        additional <- forAll . Gen.word32 $
            Range.linear (recvYetAckOff + timeoutSendKeepalive) (timeoutFlowExpires - 1)
        flow <- mkFlow' sentAnyOff sentYetAckOff recvAnyOff recvYetAckOff genKnownAddr genAeadKey
        let now = MonoTime32 $ ms32 (created flow) + additional
        TimingActions{..} <- evalIO $ goCheck now [] flow
        expired === False
        msgs <- traverse (traverse evalMkGotMsg) sentTiming
        let kas = filter ((\case{Keepalive _ -> True; _ -> False}) . snd) msgs
        length kas === 1
        case kas of
            [(addr, _)] -> do
                expectAddr <- readTVarIO $ remoteAddr flow
                addr === expectAddr
            _ -> failure
        sentAny <- evalIO . readPVar $ sentAny flow
        sentAny === now
    it "doesn't send keepalives after receiving a keepalive" do
        sentAnyOff    <- forAll $ Gen.word32 (Range.linear 0 (timeoutFlowExpires - timeoutSendKeepalive - 1))
        sentYetAckOff <- forAll $ Gen.word32 (Range.linear 0 sentAnyOff)
        recvAnyOff    <- forAll $ Gen.word32 (Range.linear sentAnyOff (timeoutFlowExpires - timeoutSendKeepalive - 1))
        recvYetAckOff <- forAll $ Gen.word32 (Range.linear 0 sentAnyOff)
        goCheck <- genCheckCtlFlowTiming
        additional <- forAll $ Gen.word32 (Range.linear recvAnyOff (timeoutFlowExpires - 1))

        flow <- mkFlow' sentAnyOff sentYetAckOff recvAnyOff recvYetAckOff genKnownAddr genAeadKey
        let now = MonoTime32 $ ms32 (created flow) + additional
        TimingActions{..} <- evalIO $ goCheck now [] flow
        expired === False
        msgs <- traverse (traverse evalMkGotMsg) sentTiming
        let kas = filter ((\case{Keepalive _ -> True; _ -> False}) . snd) msgs
        length kas === 0
    it "doesn't re-send keepalives" do
        sentAnyOff    <- forAll $ Gen.word32 (Range.linear timeoutSendKeepalive (timeoutFlowExpires - timeoutSendKeepalive - 1))
        sentYetAckOff <- forAll $ Gen.word32 (Range.linear 0 (sentAnyOff - 1))
        recvAnyOff    <- forAll $ Gen.word32 (Range.linear sentYetAckOff (sentAnyOff - 1))
        recvYetAckOff <- forAll $ Gen.word32 (Range.linear sentYetAckOff recvAnyOff)
        additional <- forAll $ Gen.word32 (Range.linear sentAnyOff (timeoutFlowExpires - 1))
        goCheck <- genCheckCtlFlowTiming

        flow <- mkFlow' sentAnyOff sentYetAckOff recvAnyOff recvYetAckOff genKnownAddr genAeadKey
        let now = MonoTime32 $ ms32 (created flow) + additional
        TimingActions{..} <- evalIO $ goCheck now [] flow
        expired === False
        msgs <- traverse (traverse evalMkGotMsg) sentTiming
        let kas = filter ((\case{Keepalive _ -> True; _ -> False}) . snd) msgs
        length kas === 0
    it "refreshes non-responding flows as either role" do
        sentAnyOff    <- forAll $ Gen.word32 (Range.linear 1 (timeoutFlowExpires - timeoutWantKeepalive - 1))
        sentYetAckOff <- forAll $ Gen.word32 (Range.linear 1 sentAnyOff)
        recvAnyOff    <- forAll $ Gen.word32 (Range.linear 0 (sentYetAckOff - 1))
        recvYetAckOff <- forAll $ Gen.word32 (Range.linear 0 recvAnyOff)
        additional <- forAll . Gen.word32 $
            Range.linear (max sentAnyOff (sentYetAckOff + timeoutWantKeepalive)) (timeoutFlowExpires - 1)
        goCheck <- genCheckCtlFlowTiming

        flow <- mkFlow' sentAnyOff sentYetAckOff recvAnyOff recvYetAckOff genKnownAddr genAeadKey
        let now = MonoTime32 $ ms32 (created flow) + additional
        TimingActions{..} <- evalIO $ goCheck now [] flow
        expired === False
        length sentTiming === 1
        case sentTiming of
            [(addr, buf)] -> do
                expectAddr <- readTVarIO $ remoteAddr flow
                addr === expectAddr
                msg <- evalIO $ mkGotMsg buf
                case msg of
                    Just (Helo _) -> pure ()
                    _             -> failure
            _ -> failure
    it "refreshes nearly-expired active flows if initiator" do
        let nearExpiry = timeoutFlowExpires - timeoutWantKeepalive
        additional <- forAll $ Gen.word32 (Range.linear nearExpiry (timeoutFlowExpires - 1))
        let nearNearExpiry = additional - timeoutWantKeepalive + 1
        sentAnyOff    <- forAll $ Gen.word32 (Range.linear nearNearExpiry (timeoutFlowExpires - 1))
        sentYetAckOff <- forAll $ Gen.word32 (Range.linear 0 sentAnyOff)
        recvAnyOff    <- forAll $ Gen.word32 (Range.linear nearNearExpiry (timeoutFlowExpires - 1))
        recvYetAckOff <- forAll $ Gen.word32 (Range.linear 0 recvAnyOff)
        goCheck <- genCheckCtlFlowTiming

        flow <- (\f -> f{role = Initiator}) <$>
            mkFlow' sentAnyOff sentYetAckOff recvAnyOff recvYetAckOff genKnownAddr genAeadKey
        let now = MonoTime32 $ ms32 (created flow) + additional
        TimingActions{..} <- evalIO $ goCheck now [] flow
        expired === False
        msgs <- traverse (traverse evalMkGotMsg) sentTiming
        let helos = filter ((\case{Helo _ -> True; _ -> False}) . snd) msgs
        length helos === 1
        case helos of
            [(addr, _)] -> do
                expectAddr <- readTVarIO $ remoteAddr flow
                addr === expectAddr
            _ -> failure
    it "doesn't refresh nearly-expired active flows if responder" do
        let nearExpiry = timeoutFlowExpires - timeoutWantKeepalive
        additional <- forAll $ Gen.word32 (Range.linear nearExpiry (timeoutFlowExpires - 1))
        let nearNearExpiry = additional - timeoutWantKeepalive + 1
        sentAnyOff    <- forAll $ Gen.word32 (Range.linear nearNearExpiry (timeoutFlowExpires - 1))
        sentYetAckOff <- forAll $ Gen.word32 (Range.linear 0 sentAnyOff)
        recvAnyOff    <- forAll $ Gen.word32 (Range.linear nearNearExpiry (timeoutFlowExpires - 1))
        recvYetAckOff <- forAll $ Gen.word32 (Range.linear 0 recvAnyOff)
        goCheck <- genCheckCtlFlowTiming

        flow <- (\f -> f{role = Responder}) <$>
            mkFlow' sentAnyOff sentYetAckOff recvAnyOff recvYetAckOff genKnownAddr genAeadKey
        let now = MonoTime32 $ ms32 (created flow) + additional
        TimingActions{..} <- evalIO $ goCheck now [] flow
        expired === False
        msgs <- traverse (traverse evalMkGotMsg) sentTiming
        let helos = filter ((\case{Helo _ -> True; _ -> False}) . snd) msgs
        length helos === 0
    it "doesn't refresh nearly-expired inactive flows" do
        let nearExpiry = timeoutFlowExpires - timeoutWantKeepalive
        additional <- forAll $ Gen.word32 (Range.linear nearExpiry (timeoutFlowExpires - 1))
        let nearNearExpiry = additional - timeoutWantKeepalive
        sentAnyOff    <- forAll $ Gen.word32 (Range.linear 0 nearNearExpiry)
        sentYetAckOff <- forAll $ Gen.word32 (Range.linear 0 sentAnyOff)
        -- Must have received ack or else unresponsive flow HELO will trigger
        recvAnyOff    <- forAll $ Gen.word32 (Range.linear sentYetAckOff nearNearExpiry)
        recvYetAckOff <- forAll $ Gen.word32 (Range.linear 0 recvAnyOff)

        goCheck <- genCheckCtlFlowTiming
        flow <- mkFlow' sentAnyOff sentYetAckOff recvAnyOff recvYetAckOff genKnownAddr genAeadKey
        let now = MonoTime32 $ ms32 (created flow) + additional
        TimingActions{..} <- evalIO $ goCheck now [] flow

        expired === False
        msgs <- traverse (traverse evalMkGotMsg) sentTiming
        let helos = filter ((\case{Helo _ -> True; _ -> False}) . snd) msgs
        length helos === 0
    it "doesn't attempt refresh too often" do
        sentAnyOff    <- forAll $ Gen.word32 (Range.linear 1 (timeoutFlowExpires - timeoutWantKeepalive - 1))
        sentYetAckOff <- forAll $ Gen.word32 (Range.linear 1 sentAnyOff)
        recvAnyOff    <- forAll $ Gen.word32 (Range.linear 0 (sentYetAckOff - 1))
        recvYetAckOff <- forAll $ Gen.word32 (Range.linear 0 recvAnyOff)
        additional <- forAll . Gen.word32 $
            Range.linear (max sentAnyOff (sentYetAckOff + timeoutWantKeepalive)) (timeoutFlowExpires - 2)
        additional' <- forAll . Gen.word32 $
            Range.linear additional (timeoutFlowExpires - 1)
        goCheck <- genCheckCtlFlowTiming

        flow <- mkFlow' sentAnyOff sentYetAckOff recvAnyOff recvYetAckOff genKnownAddr genAeadKey
        let now = MonoTime32 $ ms32 (created flow) + additional
        acts <- evalIO $ goCheck now [] flow
        expired acts === False
        msgs <- traverse (traverse evalMkGotMsg) (sentTiming acts)
        let helos = filter ((\case{Helo _ -> True; _ -> False}) . snd) msgs
        length helos === 1
        case helos of
            [(addr, _)] -> do
                expectAddr <- readTVarIO $ remoteAddr flow
                addr === expectAddr
            _ -> failure
        let now' = MonoTime32 $ ms32 (created flow) + additional'
        acts' <- evalIO $ goCheck now' (preflowTiming acts) flow
        expired acts' === False
        msgs' <- traverse (traverse evalMkGotMsg) (sentTiming acts')
        let helos' = filter ((\case{Helo _ -> True; _ -> False}) . snd) msgs'
        length helos' === 0
    it "doesn't refresh a replaced flow" do
        goCheck <- genCheckCtlFlowTiming

        let t = timeoutFlowExpires - timeoutWantKeepalive
        flow1_ <- mkFlow' t t t t genKnownAddr genAeadKey
        let now = MonoTime32 $ ms32 (created flow1) + t
            flow1 = flow1_{ role = Initiator }
            fid1 = FlowId 0
            AddrKey tsrk _ _ = fromMaybe (error "tsrk") $ Map.lookup (theirAddr flow1) known
        acts <- evalIO $ goCheck now [] flow1
        msgs <- traverse (traverse evalMkGotMsg) (sentTiming acts)
        length msgs === 1
        (pfid, pf) <- case preflowTiming acts of
            [r] -> pure r
            _   -> failure
        annotateShow pfid
        case msgs of
            [(_, Helo _)] -> pure ()
            _             -> failure

        -- Skip to the end of the handshake for an Initiator
        ther <- evalIO do
            tpf <- mirrorPf (getPreflowId pfid) (Ip6Lo 0) tsrk pf
            mkValidTher tsrk tpf (privPubKey (oerk pf)) (getPreflowId pfid)
        (fromJunkBuffer -> (junk, therBuf)) <- evalIO $
            (`copyIntoJunkBuffer` getBuf ther) =<< newJunkBuffer

        let now' = MonoTime32 $ ms32 now + 1
        rand <- properRand
        res <- evalIO $ checkCtlIncoming rand [(fid1, flow1)] [(pfid, pf)] now' junk therBuf
        acts' <- evalEither res
        -- Preflow sticks around waiting for goodness
        length (preflow acts') === 1

        for_ (registered acts') \(flid, fl) -> do
            d1 <- evalIO $ describeFlow fl
            annotate $ show flid <> " = " <> d1

        length (registered acts') === 2
        for_ (registered acts') \(_, fl) -> role fl === Initiator

        let now'' = MonoTime32 $ ms32 now' + 1
        -- Flow has gotten good, preflow is gone
        acts'' <- evalIO $ goCheck now'' [] flow1
        length (sentTiming acts'') === 0
    it "refreshes flows approaching outbound counter limit if initiator" do
        goCheck <- genCheckCtlFlowTiming
        sent <- forAll $ Gen.word32 (Range.linear (fromIntegral nearMaxCount) maxBound)
        flow <- (\f -> f{ role = Initiator }) <$> mkFlow' 0 0 0 0 genKnownAddr genAeadKey
        let now = created flow
        writePVar (sentCount flow) (fromIntegral sent)
        TimingActions{..} <- evalIO $ goCheck now [] flow
        expired === False
        discarded === False
        length preflowTiming === 1
        length sentTiming === 1
    it "refreshes flows approaching inbound counter limit if initiator" do
        goCheck <- genCheckCtlFlowTiming
        recv <- forAll $ Gen.word32 (Range.linear (fromIntegral nearMaxCount) maxBound)
        flow <- (\f -> f{ role = Initiator }) <$> mkFlow' 0 0 0 0 genKnownAddr genAeadKey
        let now = created flow
        _ <- evalIO $ windowSet recv (recvWindow flow)
        TimingActions{..} <- evalIO $ goCheck now [] flow
        expired === False
        discarded === False
        length preflowTiming === 1
        length sentTiming === 1

ctlPreflowTimingSpec :: Spec
ctlPreflowTimingSpec = describe "ctlPreflowTiming" do
    it "removes expired preflows" do
        let pfid = PreflowId $ FlowId 0
        privKey <- forAll genPrivKey
        tAddr <- forAll genIp6Lo
        pf <- mkPreflow tAddr privKey
        additional <- forAll $ Gen.word32 (Range.linear timeoutPreflowExpires maxBound)
        let now = MonoTime32 $ ms32 (began pf) + additional
        idFlow <- evalIO M.newIO
        loFlow <- evalIO M.newIO
        (loPre, idPre) <- atomically do
            p <- M.new
            i <- M.new
            M.insert pf pfid i
            M.insert pf tAddr p
            pure (p, i)
        evalIO $ ctlPreflowTiming loPre idPre loFlow idFlow (const $ pure ()) now pfid pf
        idLookup <- atomically $ M.lookup pfid idPre
        loLookup <- atomically $ M.lookup tAddr loPre
        isNothing idLookup === True
        isNothing loLookup === True

ctlOutgoingSpec :: Spec
ctlOutgoingSpec = describe "ctlOutgoing" do
    it "uses the provided rand" do
        gen <- forAll genGen
        let flowId = runST do
                r <- thawGen gen
                PreflowId . FlowId <$> uniformW32 r
            rand = evalIO $ frozenRand gen
        actions <- checkCtlOutgoing rand genIp6Addr [] []
        case registeredOut actions of
            [(xid, _)] -> xid === flowId
            _          -> failure
    it "doesn't re-use running flow ids when creating a HELO" do
        gen <- forAll genGen
        let flowId = runST do
                r <- thawGen gen
                FlowId <$> uniformW32 r
            rand = evalIO $ frozenRand gen
        flow <- mkFlow genAeadKey
        let theirGen = Gen.filter ((/= theirAddr flow) . addrIp6Lo) genIp6Addr
        actions <- checkCtlOutgoing rand theirGen [(flowId, flow)] []
        length (registeredOut actions) === 1
        length (sentOut actions) === 1
        traverse_ (uncurry (/==))
            [ (xid, yid)
            | l <- permutations (registeredOut actions)
            , ((xid, _), (yid, _)) <- zip l (drop 1 l)
            ]
    it "doesn't re-use preflow flow ids when creating a HELO" do
        gen <- forAll genGen
        let (flowId, privKey) = runST do
                r <- thawGen gen
                flid <- PreflowId . FlowId <$> uniformW32 r
                pk <- loadPrivKey <$> privKeyBytesGen (uniformW32 r)
                pure (flid, pk)
            rand = evalIO $ frozenRand gen
        tAddr@(Ip6Addr _ (Ip6Lo -> tLo)) <- forAll genIp6Addr
        pf <- mkPreflow tLo privKey
        let theirGen = Gen.filter (/= tAddr) genIp6Addr
        actions <- checkCtlOutgoing rand theirGen [] [(flowId, pf)]
        length (registeredOut actions) === 2
        length (sentOut actions) === 1
        traverse_ (uncurry (/==))
            [ (xid, yid)
            | l <- permutations (registeredOut actions)
            , ((xid, _), (yid, _)) <- zip l (drop 1 l)
            ]
    it "does nothing for an existing preflow" do
        gen <- forAll genGen
        let (flowId, privKey) = runST do
                r <- thawGen gen
                flid <- PreflowId . FlowId <$> uniformW32 r
                pk <- loadPrivKey <$> privKeyBytesGen (uniformW32 r)
                pure (flid, pk)
            rand = evalIO $ frozenRand gen
        tAddr@(Ip6Addr _ (Ip6Lo -> tLo)) <- forAll genIp6Addr
        pf <- mkPreflow tLo privKey
        let theirGen = Gen.constant tAddr
        actions <- checkCtlOutgoing rand theirGen [] [(flowId, pf)]
        length (registeredOut actions) === 1
        length (sentOut actions) === 0
    it "creates a new preflow when outbound counter is too high" do
        preSent <- forAll $ Gen.integral (Range.linear maxSendCount maxBound)
        flow <- mkFlow genAeadKey
        writePVar (sentCount flow) (fromIntegral preSent)
        -- Ensure flow is good
        writePVar (recvAny flow) (MonoTime32 $ ms32 (created flow) + 1)
        let fid = FlowId 0
            theirGen = Gen.constant (ip6LoAddr $ theirAddr flow)
        OutgoingActions{..} <- checkCtlOutgoing properRand theirGen [(fid, flow)] []
        length registeredOut === 1
        sentMsgs <- traverse (traverse evalMkGotMsg) sentOut
        case sentMsgs of
            [(_, Helo _)] -> pure ()
            _             -> failure

    it "doesn't keep more than one old flow per destination" pending

ctlIncomingSpec :: Spec
ctlIncomingSpec = describe "ctlIncoming" do
    xit "throws on random bytes" do
        now <- forAll $ MonoTime32 <$> Gen.word32 Range.linearBounded
        bytes <- forAll $ Gen.list
            (Range.linear 0 (fromIntegral Const.mtu))
            (Gen.word8 Range.linearBounded)
        rand <- properRand
        let buf = V.fromList bytes
        mbuf <- V.unsafeThaw buf
        (junk, bufInJunk) <- evalIO $ fmap fromJunkBuffer $
            (`copyIntoJunkBuffer` mbuf) =<< newJunkBuffer
        res <- evalIO $ checkCtlIncoming rand [] [] now junk bufInJunk
        isLeft res === True
    it "throws on CONT" do
        now <- forAll $ MonoTime32 <$> Gen.word32 Range.linearBounded
        rand <- properRand
        (_, _, _, junk, msg) <- genContent
        res <- evalIO $ checkCtlIncoming rand [] [] now junk $ getBuf msg
        isLeft res === True
    it "throws on KEEP" do
        now <- forAll $ MonoTime32 <$> Gen.word32 Range.linearBounded
        rand <- properRand
        (_, _, _, junk, msg) <- genKeepalive
        res <- evalIO $ checkCtlIncoming rand [] [] now junk $ getBuf msg
        isLeft res === True
    it "responds to a HELO with THER" do
        now <- forAll $ MonoTime32 <$> Gen.word32 Range.linearBounded
        rand <- properRand
        (_, _, junk, msg) <- genHelo
        res <- evalIO $ checkCtlIncoming rand [] [] now junk $ getBuf msg
        actions <- evalEither res
        length (registered actions) === 0
        length (preflow actions) === 1
        length (sent actions) === 1
    it "uses the provided rand" do
        now <- forAll $ MonoTime32 <$> Gen.word32 Range.linearBounded
        gen <- forAll genGen
        let flowId = runST do
                r <- thawGen gen
                FlowId <$> uniformW32 r
        rand <- evalIO $ frozenRand gen
        (_, _, junk, msg) <- genHelo
        res <- evalIO $ checkCtlIncoming rand [] [] now junk $ getBuf msg
        actions <- evalEither res
        case preflow actions of
            [(xid, _)] -> xid === PreflowId flowId
            _          -> failure
    it "doesn't re-use running flow ids when creating a THER" do
        now <- forAll $ MonoTime32 <$> Gen.word32 Range.linearBounded
        gen <- forAll genGen
        let flowId = runST do
                r <- thawGen gen
                FlowId <$> uniformW32 r
        rand <- evalIO $ frozenRand gen
        flow <- mkFlow genAeadKey
        (_, _, junk, msg) <- genHelo
        res <- evalIO $ checkCtlIncoming rand [(flowId, flow)] [] now junk $ getBuf msg
        actions <- evalEither res
        length (registered actions) === 1
        length (preflow actions) === 1
        length (sent actions) === 1
        case (registered actions, preflow actions) of
            ([(xid, _)], [(yid, _)]) -> PreflowId xid /== yid
            _                        -> failure
    it "doesn't re-use preflow flow ids when creating a THER" do
        now <- forAll $ MonoTime32 <$> Gen.word32 Range.linearBounded
        gen <- forAll genGen
        let (flowId, privKey) = runST do
                r <- thawGen gen
                flid <- PreflowId . FlowId <$> uniformW32 r
                pk <- loadPrivKey <$> privKeyBytesGen (uniformW32 r)
                pure (flid, pk)
        rand <- evalIO $ frozenRand gen
        (_, _, junk, msg) <- genHelo
        tAddr <- forAll genIp6Lo
        pf <- mkPreflow tAddr privKey
        res <- evalIO $ checkCtlIncoming rand [] [(flowId, pf)] now junk $ getBuf msg
        actions <- evalEither res
        length (registered actions) === 0
        length (sent actions) === 1
        length (preflow actions) === 2
        traverse_ (uncurry (/==))
            [ (xid, yid)
            | l <- permutations (preflow actions)
            , ((xid, _), (yid, _)) <- zip l (drop 1 l)
            ]
    it "ignores an unexpected THER" do
        now <- forAll $ MonoTime32 <$> Gen.word32 Range.linearBounded
        rand <- properRand
        (_, _, _, _, _, junk, msg) <- genTher
        res <- evalIO $ checkCtlIncoming rand [] [] now junk $ getBuf msg
        actions <- evalEither res
        length (sent actions) === 0
        length (registered actions) === 0
        length (preflow actions) === 0
    modifyMaxDiscardRatio (const 100) $ it "does nothing for a THER with an invalid HMAC" do
        now <- forAll $ MonoTime32 <$> Gen.word32 Range.linearBounded
        rand <- properRand
        privKey <- forAll genPrivKey
        pf <- mkPreflow (Ip6Lo 0) privKey
        (_, _, ourId, k', h', junk, msg) <- genTher
        when (h' `eqMem` h pf) discard
        when (k' `eqMem` intKey pf) discard
        res <- evalIO $ checkCtlIncoming rand [] [(PreflowId ourId, pf)] now junk $ getBuf msg
        actions <- evalEither res
        length (registered actions) === 0
        length (preflow actions) === 1
        length (sent actions) === 0
    it "responds to a THER with GNRL" do
        ourId <- forAll genFlowId
        (now, rand, pf, msg, junk) <- correctTher ourId
        res <- evalIO $ checkCtlIncoming rand [] [(PreflowId ourId, pf)] now junk $ getBuf msg
        actions <- evalEither res
        length (registered actions) === 1
        length (preflow actions) === 1
        length (sent actions) === 1
    modifyMaxDiscardRatio (const 100) $ it "responds to a THER with GNRL, even after receiving a bad THER" do
        (_, _, ourId, k', h', _, badMsg) <- genTher
        (now, rand, pf, goodMsg, junk) <- correctTher ourId
        when (h' `eqMem` h pf) discard
        when (k' `eqMem` intKey pf) discard
        resBad <- evalIO $ checkCtlIncoming rand [] [(PreflowId ourId, pf)] now junk $ getBuf badMsg
        _ <- evalEither resBad
        resGood <- evalIO $ checkCtlIncoming rand [] [(PreflowId ourId, pf)] now junk $ getBuf goodMsg
        actions <- evalEither resGood
        length (registered actions) === 1
        length (preflow actions) === 1
        length (sent actions) === 1
    it "upon GNRL, sends at least one message and removes preflow" do
        now <- forAll $ MonoTime32 <$> Gen.word32 Range.linearBounded
        rand <- properRand
        oerk <- forAll genPrivKey
        msgCount <- forAll $ Gen.int (Range.linear 0 10)
        messages <- for (take msgCount [(0 :: Int)..]) . const $
            (\(_,_,_,fromJunkBuffer -> (_, a)) -> getBuf a) <$>
                genV6BufferWithAddrs genValidAddr (\_ -> Gen.constant (ip6LoAddr (Ip6Lo 0)))
        ourId <- forAll genFlowId
        AddrKey tsrk tsuk _ <- forAll genKnownAddrKey
        intKey0 <- forAll genAeadKey
        ck0 <- forAll genSharedSecret
        h0 <- Hash . B.unsafeAtLeast . B.pack <$>
            forAll (Gen.list (Range.singleton hmacLength) (Gen.word8 Range.linearBounded))
        pf <- do
            intKey <- evalIO $ cl AeadKey intKey0
            ck <- evalIO $ cl SharedSecret ck0
            h <- evalIO $ cl Hash h0
            tsukV <- newTVarIO tsuk
            buffered <- evalIO $ RB.newAlignedPinnedRingBuffer junkBufferSize 10
            pure Preflow
                    { oerk
                    , tAddr    = Ip6Lo 0
                    , tsukV
                    , tId      = unsafePerformIO (newPVar ourId)
                    , buffered
                    , began    = now
                    , lastR    = unsafePerformIO (newPVar LHelo)
                    , intKey, ck, h
                    }
        msg0 <- do
            tpf <- evalIO $ mirrorPf ourId (Ip6Lo 0) tsrk pf
            evalIO $ mkValidGnrl tsrk tpf ourId (privPubKey oerk)
        evalIO $ fillRb (buffered pf) messages
        (fromJunkBuffer -> (junk, msg)) <- evalIO $
            (`copyIntoJunkBuffer` getBuf msg0) =<< newJunkBuffer
        res <- evalIO $ checkCtlIncoming rand [] [(PreflowId ourId, pf)] now junk $ getBuf msg
        actions <- evalEither res
        length (registered actions) === 1
        length (preflow actions) === 0
        length (sent actions) === max 1 (length messages)
    it "adopts peer's preflow if their ID is higher" pending
    it "adopts own preflow if our ID is higher" pending
    it "doesn't keep more than one old flow per destination" pending
  where
    correctTher ourId = do
        now <- forAll $ MonoTime32 <$> Gen.word32 Range.linearBounded
        rand <- properRand
        oerk <- forAll genPrivKey
        tsrk <- forAll genPrivKey
        terk <- forAll genPrivKey
        theirId <- forAll genFlowId
        intKey0 <- forAll genAeadKey
        ck0 <- forAll genSharedSecret
        h0 <- Hash . B.unsafeAtLeast . B.pack <$>
            forAll (Gen.list (Range.singleton hmacLength) (Gen.word8 Range.linearBounded))
        junk <- evalIO newJunkBuffer
        pf <- do
            intKey <- evalIO $ cl AeadKey intKey0
            ck <- evalIO $ cl SharedSecret ck0
            h <- evalIO $ cl Hash h0
            tsukV <- newTVarIO $ privPubKey tsrk
            pure Preflow
                    { oerk
                    , tAddr    = Ip6Lo 0
                    , tsukV
                    , tId      = unsafePerformIO (newPVar ourId)
                    , buffered = unsafePerformIO (RB.newAlignedPinnedRingBuffer 0 0)
                    , began    = now
                    , lastR    = unsafePerformIO (newPVar LNone)
                    , intKey, ck, h
                    }
        msg <- do
            tpf <- evalIO $ mirrorPf ourId (Ip6Lo 0) terk pf
            let teuk = privPubKey terk
                oeuk = privPubKey oerk
            ee <- maybe failure pure $ keyAgreement terk oeuk
            es <- maybe failure pure $ keyAgreement tsrk oeuk
            evalIO do
                mixHash tpf teuk
                mixKey tpf ee
                mixKey tpf es
            evalIO $ buildTher junk theirId teuk ourId (intKey tpf) (h tpf)
        pure (now, rand, pf, msg, junk)

spec :: Spec
spec = modifyMaxSuccess (const 1000) . parallel $
    ctlIncomingSpec *> ctlOutgoingSpec *> ctlFlowTimingSpec *> ctlPreflowTimingSpec
