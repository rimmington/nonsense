module Nonsense.AddressSpec (spec) where

import Control.Concurrent (getNumCapabilities)
import Hedgehog.Gen qualified as Gen
import Hedgehog.Range qualified as Range
import Nonsense.Address (AddrKey (..), keyAddr, newKey)
import Nonsense.ECDH (loadPrivKey, privPubKey)
import Nonsense.Random (seedFromBS)
import RIO
import Test.Hspec
import Test.Hspec.Hedgehog

spec :: Spec
spec = do
    describe "newKey" $
        modifyMaxSuccess (const 2) . it "produces a key which keyAddr works on" $ hedgehog do
            caps <- evalIO getNumCapabilities
            seedBS <- forAll $ Gen.bytes (Range.singleton 16)
            seed <- evalMaybe $ seedFromBS seedBS
            (_, rk, AddrKey{akLo}) <- evalIO $ newKey seed caps
            keyAddr (privPubKey $ loadPrivKey rk) === Just akLo
