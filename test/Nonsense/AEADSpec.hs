module Nonsense.AEADSpec (genAeadKey, spec) where

import Data.Bits (xor)
import Data.Vector.Storable qualified as V
import Data.Vector.Storable.Mutable qualified as VM
import Hedgehog.Gen qualified as Gen
import Hedgehog.Range qualified as Range
import Nonsense.AEAD
import Nonsense.Bytes qualified as B
import Nonsense.Cipher (Counter (..), initCipher)
import Nonsense.K12Spec (eqMem, genSharedSecret)
import RIO
import Test.Hspec
import Test.Hspec.Hedgehog

genAeadKey :: MonadGen m => m AeadKey
genAeadKey = AeadKey . B.unsafeAtLeast . B.pack <$>
    Gen.list (Range.singleton 64) Gen.enumBounded

genBS :: Int -> Gen ByteString
genBS r = Gen.bytes (Range.linear r 1024)

spec :: Spec
spec = modifyMaxSuccess (const 1000) $ do
    describe "expandInto" do
        it "is deterministic" $ hedgehog do
            ss <- forAll genSharedSecret
            r1 <- evalIO $ AeadKey . snd <$> B.allocAtLeast (const $ pure ())
            r2 <- evalIO $ AeadKey . snd <$> B.allocAtLeast (const $ pure ())
            evalIO $ expandInto ss r1
            evalIO $ expandInto ss r2
            diff r1 eqMem r2

    describe "encryptInPlace" do
        it "is deterministic" $ hedgehog do
            k <- forAll genAeadKey
            c <- forAll $ Counter <$> Gen.word32 Range.linearBounded
            ad <- forAll $ genBS 0
            bytes <- forAll $ B.unsafeAtLeast @32 <$> genBS 32
            buf1 <- evalIO $ B.cloneAtLeast @B.Buffer bytes
            buf2 <- evalIO $ B.cloneAtLeast @B.Buffer bytes
            cipher <- evalIO initCipher
            evalIO $ keyCipher cipher k
            r1 <- evalIO $ encryptInPlace cipher k c ad buf1
            r2 <- evalIO $ encryptInPlace cipher k c ad buf2
            b1 <- evalIO $ B.cloneAtLeast @ByteString r1
            b2 <- evalIO $ B.cloneAtLeast @ByteString r2
            diff b1 eqMem b2

    describe "decryptInPlace" do
        it "inverts encryptInPlace" $ hedgehog do
            k <- forAll genAeadKey
            c <- forAll $ Counter <$> Gen.word32 Range.linearBounded
            ad <- forAll $ genBS 0
            bytes <- forAll $ B.unsafeAtLeast @32 <$> genBS 32
            buf <- evalIO $ B.cloneAtLeast @B.Buffer bytes
            cipher <- evalIO initCipher
            evalIO $ keyCipher cipher k
            crypt <- evalIO $ encryptInPlace cipher k c ad buf
            res <- evalIO $ decryptInPlace cipher k c ad crypt
            ult <- maybe failure pure res
            ultBS <- evalIO $ B.cloneAtLeast @ByteString ult
            diff (B.dropBack @32 bytes) eqMem ultBS
        it "doesn't work after scribble" $ hedgehog do
            k <- forAll genAeadKey
            c <- forAll $ Counter <$> Gen.word32 Range.linearBounded
            ad <- forAll $ genBS 0
            bytes <- forAll $ B.unsafeAtLeast @32 <$> genBS 32
            buf <- evalIO $ B.cloneAtLeast @B.Buffer bytes
            cipher <- evalIO initCipher
            evalIO $ keyCipher cipher k
            crypt <- evalIO $ encryptInPlace cipher k c ad buf

            -- Pick part of the message to scribble
            let msgLen = VM.length $ B.orMore crypt
            idx <- forAll $ Gen.int (Range.linear 0 (msgLen - 1))

            -- Do the scribble
            annotateShow =<< V.freeze (B.orMore crypt)
            VM.modify (B.orMore crypt) (xor 0x01) idx
            annotateShow =<< V.freeze (B.orMore crypt)

            -- Check
            res <- evalIO $ decryptInPlace cipher k c ad crypt
            maybe (pure ()) (const failure) res
        it "doesn't work if AD changed" $ hedgehog do
            k <- forAll genAeadKey
            c <- forAll $ Counter <$> Gen.word32 Range.linearBounded
            ad <- forAll $ genBS 0
            ad' <- forAll . Gen.filter (/= ad) $ genBS 0
            bytes <- forAll $ B.unsafeAtLeast @32 <$> genBS 32
            buf <- evalIO $ B.cloneAtLeast @B.Buffer bytes
            cipher <- evalIO initCipher
            evalIO $ keyCipher cipher k
            crypt <- evalIO $ encryptInPlace cipher k c ad buf
            res <- evalIO $ decryptInPlace cipher k c ad' crypt
            maybe (pure ()) (const failure) res
        it "doesn't work if counter changed" $ hedgehog do
            k <- forAll genAeadKey
            c <- forAll $ Counter <$> Gen.word32 Range.linearBounded
            c' <- forAll . Gen.filter (/= c) $ Counter <$> Gen.word32 Range.linearBounded
            ad <- forAll $ genBS 0
            bytes <- forAll $ B.unsafeAtLeast @32 <$> genBS 32
            buf <- evalIO $ B.cloneAtLeast @B.Buffer bytes
            cipher <- evalIO initCipher
            evalIO $ keyCipher cipher k
            crypt <- evalIO $ encryptInPlace cipher k c ad buf
            res <- evalIO $ decryptInPlace cipher k c' ad crypt
            maybe (pure ()) (const failure) res
        it "doesn't work if key changed" $ hedgehog do
            k <- forAll genAeadKey
            k' <- forAll $ Gen.filter (not . eqMem k) genAeadKey
            c <- forAll $ Counter <$> Gen.word32 Range.linearBounded
            ad <- forAll $ genBS 0
            bytes <- forAll $ B.unsafeAtLeast @32 <$> genBS 32
            buf <- evalIO $ B.cloneAtLeast @B.Buffer bytes
            cipher <- evalIO initCipher
            evalIO $ keyCipher cipher k
            crypt <- evalIO $ encryptInPlace cipher k c ad buf
            evalIO $ keyCipher cipher k'
            res <- evalIO $ decryptInPlace cipher k' c ad crypt
            maybe (pure ()) (const failure) res

    describe "encryptEmpty" do
        it "is deterministic" $ hedgehog do
            k <- forAll genAeadKey
            c <- forAll $ Counter <$> Gen.word32 Range.linearBounded
            ad <- forAll $ genBS 0
            out1 <- evalIO $ snd <$> B.allocAtLeast @_ @B.Buffer (const $ pure ())
            out2 <- evalIO $ snd <$> B.allocAtLeast @_ @B.Buffer (const $ pure ())
            cipher <- evalIO initCipher
            evalIO $ keyCipher cipher k
            r1 <- evalIO $ encryptEmpty k c ad out1
            r2 <- evalIO $ encryptEmpty k c ad out2
            b1 <- evalIO $ B.cloneAtLeast @ByteString r1
            b2 <- evalIO $ B.cloneAtLeast @ByteString r2
            diff b1 eqMem b2

    describe "decyptEmpty" do
        it "passes if all is the same" $ hedgehog do
            k <- forAll genAeadKey
            c <- forAll $ Counter <$> Gen.word32 Range.linearBounded
            ad <- forAll $ genBS 0
            out <- evalIO $ snd <$> B.allocAtLeast @_ @B.Buffer (const $ pure ())
            tag <- evalIO $ encryptEmpty k c ad out
            res <- evalIO $ decryptEmpty k c ad tag
            res === True
        it "doesn't work if AD changed" $ hedgehog do
            k <- forAll genAeadKey
            c <- forAll $ Counter <$> Gen.word32 Range.linearBounded
            ad <- forAll $ genBS 0
            ad' <- forAll . Gen.filter (/= ad) $ genBS 0
            out <- evalIO $ snd <$> B.allocAtLeast @_ @B.Buffer (const $ pure ())
            tag <- evalIO $ encryptEmpty k c ad out
            res <- evalIO $ decryptEmpty k c ad' tag
            res === False
        it "doesn't work if counter changed" $ hedgehog do
            k <- forAll genAeadKey
            c <- forAll $ Counter <$> Gen.word32 Range.linearBounded
            c' <- forAll . Gen.filter (/= c) $ Counter <$> Gen.word32 Range.linearBounded
            ad <- forAll $ genBS 0
            out <- evalIO $ snd <$> B.allocAtLeast @_ @B.Buffer (const $ pure ())
            tag <- evalIO $ encryptEmpty k c ad out
            res <- evalIO $ decryptEmpty k c' ad tag
            res === False
        it "doesn't work if key changed" $ hedgehog do
            k <- forAll genAeadKey
            k' <- forAll $ Gen.filter (not . eqMem k) genAeadKey
            c <- forAll $ Counter <$> Gen.word32 Range.linearBounded
            ad <- forAll $ genBS 0
            out <- evalIO $ snd <$> B.allocAtLeast @_ @B.Buffer (const $ pure ())
            tag <- evalIO $ encryptEmpty k c ad out
            res <- evalIO $ decryptEmpty k' c ad tag
            res === False
