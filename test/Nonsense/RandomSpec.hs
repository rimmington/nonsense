module Nonsense.RandomSpec (spec) where

import Hedgehog.Gen qualified as Gen
import Hedgehog.Range qualified as Range
import Nonsense.Random
import RIO
import Test.Hspec
import Test.Hspec.Hedgehog hiding (Seed)

genSeedText :: MonadGen m => m Text
genSeedText = Gen.text (Range.singleton 32) Gen.hexit

genWord64 :: MonadGen m => m Word64
genWord64 = Gen.word64 Range.linearBounded

spec :: Spec
spec = do
    describe "initFrozen" $
        it "is deterministic" $ do
            let fr = seedGen (Seed 255 64874263)
            let (res0, _) = withFrozen fr $ \gen ->
                    (,) <$> uniformW32 gen <*> uniformW32 gen
                (res1, _) = withFrozen fr $ \gen ->
                    (,) <$> uniformW32 gen <*> uniformW32 gen
            res0 `shouldBe` (1778002774, 2025194860)
            res1 `shouldBe` res0

    describe "decodeSeed" $ do
        it "parses seeds" $ hedgehog $ do
            txt <- forAll genSeedText
            maybe failure (const $ pure ()) $ decodeSeed txt

        it "round-trips with encodeSeed" $ hedgehog $ do
            seed <- forAll (Seed <$> genWord64 <*> genWord64)
            tripping seed encodeSeed decodeSeed
