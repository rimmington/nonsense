{-# options_ghc -fno-cse #-}

-- The above flag is not required for correctness but used to avoid GHC
-- CSEing use of 'hash' in tests.

module Nonsense.K12Spec (genSharedSecret, eqMem, spec) where

import GHC.Exts (noinline)
import Hedgehog.Gen qualified as Gen
import Hedgehog.Range qualified as Range
import Nonsense.Bytes qualified as B
import Nonsense.ECDH (SharedSecret (..), keyLength)
import Nonsense.K12
import RIO
import System.IO.Unsafe (unsafeDupablePerformIO)
import Test.Hspec
import Test.Hspec.Hedgehog

genSharedSecret :: MonadGen m => m SharedSecret
genSharedSecret = SharedSecret . B.unsafeAtLeast . B.pack <$>
    Gen.list (Range.singleton keyLength) Gen.enumBounded

genMsg :: MonadGen m => m ByteString
genMsg = Gen.bytes (Range.linear 0 3096)

eqMem :: (B.Bytes a, B.Bytes b) => a -> b -> Bool
eqMem a b = unsafeDupablePerformIO $ B.constEqMem a b

hmacIntoSpec :: Spec
hmacIntoSpec = describe "hmacInto" $ do
    it "is deterministic" $ hedgehog $ do
        k <- forAll genSharedSecret
        msg <- forAll genMsg
        (_, r0) <- evalIO $ B.allocAtLeast @_ @B.Buffer pure
        (_, r1) <- evalIO $ B.allocAtLeast @_ @B.Buffer pure
        evalIO $ hmacInto k msg r0
        evalIO $ hmacInto k msg r1
        eq <- evalIO $ r0 `B.constEqMem` r1
        eq === True

hkdfSpec :: Spec
hkdfSpec = describe "hkdf2Into" do
    it "is deterministic" $ hedgehog do
        salt <- forAll genSharedSecret
        ikm <- forAll genMsg
        out11 <- new32
        out12 <- new32
        out21 <- new32
        out22 <- new32
        evalIO $ hkdf2Into salt ikm out11 out12
        evalIO $ hkdf2Into salt ikm out21 out22

        out11B <- evalIO $ B.clone @ByteString out11
        out21B <- evalIO $ B.clone @ByteString out21
        diff out11B eqMem out21B

        out12B <- evalIO $ B.clone @ByteString out12
        out22B <- evalIO $ B.clone @ByteString out22
        diff out12B eqMem out22B
  where
    new32 = evalIO $ snd <$> B.allocAtLeast @_ @B.Buffer (const $ pure ())

hash2IntoSpec :: Spec
hash2IntoSpec = describe "hash2Into" do
    it "is deterministic" $ hedgehog do
        ba <- forAll genMsg
        bb <- forAll genMsg
        (_, Hash @B.Buffer -> h1) <- evalIO $ B.allocAtLeast pure
        (_, Hash @B.Buffer -> h2) <- evalIO $ B.allocAtLeast pure
        evalIO $ hash2Into ba bb h1
        evalIO $ hash2Into ba bb h2
        eq <- evalIO $ h1 `B.constEqMem` h2
        eq === True

    it "behaves sensibly with an empty input" $ hedgehog do
        ba <- forAll genMsg
        (_, Hash @B.Buffer -> h1) <- evalIO $ B.allocAtLeast pure
        (_, Hash @B.Buffer -> h2) <- evalIO $ B.allocAtLeast pure
        evalIO $ hash2Into ba (mempty :: ByteString) h1
        evalIO $ hash2Into (mempty :: ByteString) ba h2
        eq <- evalIO $ h1 `B.constEqMem` h2
        eq === True

hashSpec :: Spec
hashSpec = describe "hash" do
    it "is deterministic" $ hedgehog do
        ba <- forAll genMsg
        let h1 = noinline (hash ba)
            h2 = noinline (hash ba)
        h1 === h2

    it "is equivalent to hash2 mempty" $ hedgehog do
        ba <- forAll genMsg
        diff (noinline (hash ba)) eqMem (noinline (hash2 (mempty :: ByteString) ba))

hash2Spec :: Spec
hash2Spec = describe "hash2" do
    it "is deterministic" $ hedgehog do
        ba <- forAll genMsg
        bb <- forAll genMsg
        let h1 = noinline (hash2 ba bb)
            h2 = noinline (hash2 ba bb)
        h1 === h2

    it "is equivalent to hash2Into" $ hedgehog do
        ba <- forAll genMsg
        bb <- forAll genMsg
        (_, Hash @B.Buffer -> h1) <- evalIO $ B.allocAtLeast pure
        evalIO $ hash2Into ba bb h1
        let h2 = noinline (hash2 ba bb)
        eq <- evalIO $ h1 `B.constEqMem` h2
        eq === True

spec :: Spec
spec = hmacIntoSpec *> hkdfSpec *> hash2IntoSpec *> hashSpec *> hash2Spec
