{
  inputs.flake-compat.url = "github:edolstra/flake-compat";
  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.haskell-nix-utils.url = "gitlab:rimmington/haskell-nix-utils";
  inputs.haskell-nix.url = "github:input-output-hk/haskell.nix";
  inputs.k12 = {
    url = "git+https://github.com/XKCP/K12?submodules=1";
    flake = false;
  };
  inputs.nixpkgs.follows = "haskell-nix/nixpkgs-unstable";
  outputs = { self, nixpkgs, flake-utils, haskell-nix, haskell-nix-utils, k12, ... }:
    flake-utils.lib.eachSystem [ "x86_64-linux" ] (system:
    let
      overlays = [
        haskell-nix.overlay
        haskell-nix-utils.overlays.default
        libOverlay
      ];
      pkgs = import nixpkgs { inherit system overlays; inherit (haskell-nix) config; };
      utils = pkgs.haskell-nix-utils;
      project = utils.stackageProject' rec {
        resolver = "lts-22.4";
        name = "nonsense";
        src = pkgs.lib.cleanSourceWith {
          name = "${name}-src";
          src = self;
          filter = name: type:
            utils.gitHaskellSourceFilter self name type &&
            !(type == "directory" && baseNameOf name == "deploy") &&
            !(pkgs.lib.hasSuffix ".nix" name);
        };
        cabalFile = ./nonsense.cabal;
        fromHackage = utils.hackage-sets.hls_2_5_0_0 // {
          eventlog2html = "0.10.0";
          iso-deriving = "0.0.8";
        };
        modules = [
          # {
          #   enableLibraryProfiling = true;
          # }
          {
            # https://github.com/input-output-hk/haskell.nix/issues/231
            packages.nonsense.components.tests.nonsense-tests.build-tools = [
              project.hsPkgs.hspec-discover
            ];
          }
          {
            # https://github.com/haskell/haskell-language-server/issues/3185#issuecomment-1250264515
            packages.hlint.flags.ghc-lib = true;
            # https://github.com/haskell/haskell-language-server/blob/5d5f7e42d4edf3f203f5831a25d8db28d2871965/cabal.project#L67
            packages.ghc-lib-parser-ex.flags.auto = false;
            packages.stylish-haskell.flags.ghc-lib = true;

            # Disable unused formatters that take a while to build
            packages.haskell-language-server.flags.fourmolu = false;
            packages.haskell-language-server.flags.ormolu = false;
          }
        ];
        shell = {
          withHoogle = false;
          exactDeps = true;
          withHaddock = true;
          nativeBuildInputs = [
            pkgs.cabal-install
            pkgs.multitime
            # project.hsPkgs.eventlog2html.components.exes.eventlog2html
            project.hsPkgs.stylish-haskell.components.exes.stylish-haskell
            project.hsPkgs.haskell-language-server.components.exes.haskell-language-server
            project.roots
          ];
        };
      };
      libOverlay = self: super: {
        # Minimal build of Botan
        botan-2 =
          (self.callPackage "${nixpkgs}/pkgs/development/libraries/botan/2.0.nix" {
            extraConfigureFlags = "--minimized-build --enable-modules=ffi,camellia,curve25519,ofb,stream";
            inherit (self.darwin.apple_sdk.frameworks) CoreServices Security;
          }).overrideAttrs (_: {
            doCheck = true;
          });
        k12 = self.stdenv.mkDerivation rec {
          pname = "k12";
          version = k12.rev;
          src = k12;
          nativeBuildInputs = [ self.libxslt ];
          makeFlags = [ "generic64/K12Tests" "generic64/libk12.so" "generic64/libk12.a" ];
          doCheck = true;
          checkPhase = "bin/generic64/K12Tests -K12";
          installPhase = ''
            mkdir -p $out/lib/pkgconfig $out/include/k12
            cp bin/generic64/libk12.a bin/generic64/libk12.so $out/lib
            cp bin/generic64/libk12.a.headers/* $out/include/k12
            cat >$out/lib/pkgconfig/k12.pc <<EOF
            Name: k12
            Description: KangarooTwelve (or K12) is a fast and secure extendable-output function (XOF), the generalization of hash functions to arbitrary output lengths.
            Version: 0
            URL: https://github.com/XKCP/K12
            Libs: -L$out/lib -lk12
            Cflags: -I$out/include
            EOF
          '';
        };
        haskell-nix = super.haskell-nix // {
          extraPkgconfigMappings = super.haskell-nix.extraPkgconfigMappings // {
            # String pkgconfig-depends names are mapped to lists of Nixpkgs
            # package names
            "k12" = [ "k12" ];
          };
        };
      };
    in pkgs.lib.recursiveUpdate project.flake' {
      packages.default = project.flake'.packages."nonsense:exe:nonsense";
    });

  nixConfig = {
    allow-import-from-derivation = "true";
  };
}
