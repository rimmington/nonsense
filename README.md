# nonsense

A VPN with cryptographically-bound addresses (not [RFC 3972](https://datatracker.ietf.org/doc/html/rfc3972) compatible). Not entirely dissimilar to [Wireguard™](https://www.wireguard.com/) in concept. See also [ZeroTier](https://zerotier.com/).

**⚠️ If you actually use this, you're a bloody idiot.**

## Start components

```bash
bash scripts/setup-netns.sh
ip netns exec mane capsh --caps==ip --keep=1 --user=$USER --addamb=cap_net_admin,cap_setpcap -- -c $(cabal list-bin nonsense)' --keyfile ./super-secret-moon-private-key --bind 10.120.1.1'
ip netns exec mane $(cabal list-bin nonsense-moon)
cabal run nonsense-key -- gen --keyfile ./aneq-key
cabal run nonsense-key -- gen --keyfile ./pell-key
ip netns exec aneq capsh --caps==ip --keep=1 --user=$USER --addamb=cap_net_admin,cap_setpcap -- -c $(cabal list-bin nonsense)' --keyfile ./aneq-key --bind 10.120.1.2'
ip netns exec pell capsh --caps==ip --keep=1 --user=$USER --addamb=cap_net_admin,cap_setpcap -- -c $(cabal list-bin nonsense)' --keyfile ./pell-key --bind 10.120.1.3'
```

## Chat with ncat

```bash
# Start server
ip netns exec aneq ncat -klv --chat $(cabal run nonsense-key -- addr --keyfile ./aneq-key) 5000
# Connect from each host
ip netns exec aneq ncat $(cabal run nonsense-key -- addr --keyfile ./aneq-key) 5000
ip netns exec pell ncat $(cabal run nonsense-key -- addr --keyfile ./aneq-key) 5000
```
