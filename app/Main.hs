{-# language TemplateHaskell #-}

module Main (main) where

import Nonsense (Options (..), run)
import Nonsense.Address (readAddrKey)
import Options.Applicative.Simple
import Paths_nonsense qualified
import RIO
import System.Exit (die)

main :: IO ()
main = do
    ((keyFile, bindHost, underrunFd), ()) <- simpleOptions
        $(simpleVersion Paths_nonsense.version)
        "Header for command line arguments"
        "Program description, also for command line arguments"
        ((,,)
            <$> option str (long "keyfile" <> metavar "PATH")
            <*> option str (long "bind")
            <*> optional (option auto (long "underrunfd" <> hidden)))
        empty
    ourKey <- maybe (die "bad keyfile") pure =<< readAddrKey keyFile
    run Options{ourKey, ..}
