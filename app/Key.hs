{-# language TemplateHaskell #-}

module Main (main) where

import Control.Concurrent (getNumCapabilities)
import Nonsense.Address (AddrKey (..), newKey, readAddrKey)
import Nonsense.Bytes (byteLength, unsafeWithBytePtr)
import Nonsense.ECDH (PrivKeyBytes (..))
import Nonsense.Ip6Addr (ip6LoAddr, prettyIp6Addr)
import Nonsense.Random (encodeSeed, systemRandomSeed)
import Options.Applicative.Simple
import Paths_nonsense qualified
import RIO
import RIO.File (withBinaryFileDurable)
import RIO.Text (unpack)
import System.Exit (die)
import System.IO (hPutBuf, putStrLn)

main :: IO ()
main = do
    ((), run) <- simpleOptions
        $(simpleVersion Paths_nonsense.version)
        ""
        "Key utilities for nonsense."
        (pure ())
        do
            addCommand
                "gen"
                "Generate a new private key (and thus address)"
                gen
                kfOpt
            addCommand
                "addr"
                "Print the address for a private key"
                addr
                kfOpt
    run
  where
    kfOpt = option str (long "keyfile" <> metavar "PATH")

gen :: FilePath -> IO ()
gen keyfile = do
    seed <- systemRandomSeed
    caps <- getNumCapabilities
    putStrLn $ "Caps: " <> show caps
    putStrLn $ "Seed: " <> unpack (encodeSeed seed)
    (i, PrivKeyBytes scrub, AddrKey _ _ ourLo) <- newKey seed caps
    putStrLn $ "Iter: " <> show i
    withBinaryFileDurable keyfile WriteMode \h -> unsafeWithBytePtr scrub \ptr ->
        hPutBuf h ptr (fromIntegral (byteLength scrub))
    putStrLn $ "IP:   " <> prettyIp6Addr (ip6LoAddr ourLo)

addr :: FilePath -> IO ()
addr keyfile = readAddrKey keyfile >>= \case
    Nothing            -> die "bad keyfile"
    Just AddrKey{akLo} -> putStrLn . prettyIp6Addr $ ip6LoAddr akLo
