module Main (main) where

import Nonsense.Moon (run)
import RIO

main :: IO ()
main = run
