{-# language OverloadedStrings #-}
{- HLINT ignore "Redundant fmap" -}

module Nonsense.Moon (run) where

import Focus qualified
import Network.HTTP.Types.Status (badRequest400, internalServerError500, noContent204, status404)
import Network.Socket
  (Family (AF_INET), SockAddr (..), SocketType (..), bind, close, defaultProtocol,
  hostAddress6ToTuple, socket, tupleToHostAddress)
import Network.Socket.ByteString (recv)
import Network.Wai (remoteHost)
import Network.Wai.Handler.Warp (defaultSettings, setHost, setPort)
import Nonsense.Address (keyAddr)
import Nonsense.Bytes (atLeast)
import Nonsense.ECDH (PubKeyBytes (PubKeyBytes), keyLength)
import Nonsense.Ip6Addr
  (Ip6Lo (..), addrIp6Lo, ip6InSubnet, ip6LoAddr, prettyIp6Addr, tupleToIp6Addr)
import Nonsense.MonoTime (MonoTime64, monoTime64Coarse, ms64)
import Nonsense.Orbit (Registered (..), RemoteAddr (..), Seen (..), bsSeen, moonAddr, registeredBS)
import RIO
import RIO.ByteString.Lazy qualified as LB
import StmContainers.Map qualified as M
import System.IO (putStrLn)
import Web.Scotty
  (ScottyM, body, captureParam, defaultOptions, get, post, raiseStatus, raw, request, scottyOpts,
  settings, status)

run :: IO ()
run = do
    let host = fromString . prettyIp6Addr $ ip6LoAddr moonAddr
        port = 5000
        settings = defaultSettings & setPort port & setHost host
    putStrLn $ "starting moon on " <> show host <> " port " <> show port
    store <- M.newIO
    seen <- M.newIO
    race_
        (scottyOpts defaultOptions{settings} (app store seen))
        (autoinform seen)

app :: M.Map Ip6Lo Registered -> M.Map Ip6Lo Informed -> ScottyM ()
app store seen = do
    get "/lookup/:lo" do
        lo <- Ip6Lo <$> captureParam "lo"
        atomically (M.lookup lo store) >>= \case
            Just r  -> do
                liftIO $ putStrLn $ "lookup success " <> show lo
                raw . LB.fromStrict $ registeredBS r
            Nothing -> do
                liftIO $ putStrLn $ "lookup fail    " <> show lo
                status status404
    post "/register" do
        lo <- fmap remoteHost request >>= \case
            SockAddrInet6 _ _ host6 _
              | ip6 <- tupleToIp6Addr (hostAddress6ToTuple host6)
              , ip6InSubnet ip6
              -> pure (addrIp6Lo ip6)
            _ -> raiseStatus internalServerError500 "Internal Server Error"
        k <- fmap (atLeast . LB.toStrict . LB.take (fromIntegral keyLength)) body >>= \case
            Just al
              | k <- PubKeyBytes al
              , keyAddr k == Just lo
              -> pure k
            _ -> raiseStatus badRequest400 "Bad Request"
        delay <- registerDelay 3e6
        now <- liftIO monoTime64Coarse
        mra <- atomically $
            (Nothing <$ (checkSTM =<< readTVar delay))
            <|>
            (M.lookup lo seen >>= \case
                Just (Informed at ra)
                  | absDiff (ms64 at) (ms64 now) < 3e3
                  -> pure (Just ra)
                _ -> retrySTM
            )
        ra <- maybe (raiseStatus internalServerError500 "Internal Server Error") pure mra
        let r = Registered k ra
        atomically (M.focus (Focus.insertOrMerge merge r) lo store)
        liftIO $ putStrLn $ "register success " <> show lo <> " at " <> show ra
        status noContent204
  where
    merge (Registered _ newR) (Registered oldK _) = Registered oldK newR
    absDiff a b
      | a > b     = a - b
      | otherwise = b - a

autoinform :: M.Map Ip6Lo Informed -> IO a
autoinform seen = bracket (socket AF_INET Datagram defaultProtocol) close \loopback -> do
    bind loopback (SockAddrInet 5000 (tupleToHostAddress (127, 0, 0, 1)))
    forever do
        bs <- recv loopback 4096
        now <- monoTime64Coarse
        for_ (bsSeen bs) \(Seen lo ra) -> case ra of
            RemoteV6 _ h
              | ip6InSubnet (tupleToIp6Addr (hostAddress6ToTuple h))
              -> pure ()
            _ -> atomically (M.insert (Informed now ra) lo seen)

data Informed
  = Informed MonoTime64 RemoteAddr
