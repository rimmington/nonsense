{-# language StrictData #-}

module Nonsense
  ( Options (..)
  , run
  ) where

import Control.Concurrent (setNumCapabilities)
import Foreign.C.Error (Errno (..))
import Network.Socket
  (AddrInfoFlag (AI_NUMERICHOST, AI_NUMERICSERV), Family (AF_INET), HostName, SocketType (Datagram),
  addrAddress, addrFamily, addrFlags, bind, close, defaultHints, defaultProtocol, getAddrInfo,
  socket)
import Nonsense.Address (AddrKey (..))
import Nonsense.Ctl (RemoteInfo (..))
import Nonsense.Ip6Addr (ip6LoAddr, prettyIp6Addr)
import Nonsense.LinuxCaps (checkNoCaps, dropAllCapsAndCheck)
import Nonsense.MonoTime (MonoTime64 (..), monoTime32Coarse, monoTime64Coarse)
import Nonsense.Orbit (Registered (..), autoinform, mkRemote, moonAddr, orbit, remoteSockAddr)
import Nonsense.Packet (read, sockRecv, sockSend, write)
import Nonsense.Tun qualified as Tun
import Nonsense.Wiring (ctl, nextIn, nextOut, procIn, procOut, wire)
import Prelude (putStrLn)
import RIO
import System.Environment (getExecutablePath)
import System.Posix (Fd, closeFd, executeFile)

data Options
  = Options
    { ourKey     :: AddrKey
    , bindHost   :: HostName
    , underrunFd :: Maybe Fd
    , keyFile    :: FilePath
    }

run :: Options -> IO ()
run o = case underrunFd o of
    Nothing -> overrun o
    Just fd -> underrun o fd

overrun :: Options -> IO ()
overrun Options{..} = do
    setNumCapabilities 1
    -- If exec succeeds, this bracket will never be closed
    bracket (Tun.open (ip6LoAddr ourLo)) (traverse_ closeFd) \case
        Left (Tun.ErrnoMsg (Errno no) msg) ->
            error $ "bad: " ++ show no ++ " " ++ show msg
        Right fd -> do
            dropAllCapsAndCheck
            this <- getExecutablePath
            let args = ["--keyfile", keyFile, "--bind", bindHost, "--underrunfd", show fd]
            executeFile this False args Nothing
  where
    AddrKey _ _ ourLo = ourKey

underrun :: Options -> Fd -> IO ()
underrun Options{ourKey, bindHost} fd = (`finally` closeFd fd) do
    checkNoCaps
    bindAddr <- lookupAddr bindHost
    notifyV <- newEmptyMVar
    putStrLn $ "IP: " <> prettyIp6Addr (ip6LoAddr ourLo)
    withSock AF_INET $ \loopback -> withSock (addrFamily bindAddr) $ \sock -> do
        bind sock (addrAddress bindAddr)
        remote <- mkRemote
        let ingest
                | ourLo == moonAddr = \from v6 -> do
                autoinform loopback from v6
                write fd v6
                | otherwise = const (write fd)
        wiring <- wire
            ourKey
            (sockSend sock)
            (fmap (fmap \(Registered p r) -> RemoteInfo (remoteSockAddr r) p) . remote)
            (link <=< async)
        runConc $
            conc (forever do
                junk <- nextIn wiring
                (buf, from) <- sockRecv sock junk
                now <- monoTime32Coarse
                procIn wiring (ingest from) (void $ tryPutMVar notifyV ()) now from buf
            )
            <|>
            conc (forever do
                junk <- nextOut wiring
                buf <- read fd junk
                now <- monoTime32Coarse
                procOut wiring (void $ tryPutMVar notifyV ()) now buf
            )
            <|>
            conc (forever do
                takeMVar notifyV
                ctl wiring monoTime32Coarse
            )
            <|>
            conc ((monoTime64Coarse >>=) . iterateM_ $ \last -> do
                now <- monoTime64Coarse
                let next = ms64 last + 2000
                threadDelay . fromIntegral . max 0 . (* 1e3) $ next - ms64 now
                void $ tryPutMVar notifyV ()
                pure $ MonoTime64 next
            )
            <|>
            conc (orbit staticUk ourLo)
  where
    AddrKey _ staticUk ourLo = ourKey

    withSock fam = bracket (socket fam Datagram defaultProtocol) close

    lookupAddr host = getAddrInfo (Just hints) (Just host) (Just "30000") >>= \case
        ai:_ -> pure ai
        _    -> error $ "lookup failed for " ++ host
      where
        hints = defaultHints{ addrFlags = [AI_NUMERICHOST, AI_NUMERICSERV] }

    iterateM_ f = go
        where go x = f x >>= go
