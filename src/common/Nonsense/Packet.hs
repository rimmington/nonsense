{-# language CPP, GADTs, StrictData, UndecidableInstances #-}

module Nonsense.Packet
  ( JunkBuffer, JunkBufferSize
  , newJunkBuffer, asJunkBuffer, junkBufferSize
  , read, write, sockRecv, sockSend
  , FromJunkBuffer
  , copyIntoJunkBuffer, fromJunkBuffer
  , FlowId (..)
  , GotMsg (..)
  , mkGotMsg
  , HeloMsg
  , buildHelo, heloTheirId, heloTheirEphPubkey
  , TherMsg
  , buildTher, therOurId, therTheirId, therTheirEphPubkey
  , therHmac, therCheckHmac
  , GnrlMsg, LtMsg
  , buildLt, buildGnrl, ltTsukTagged, gnrlOurId, gnrlTsukTagged, gnrlConsumeTsuk
  , gnrl2ndHmac, gnrlCheck2ndHmac
  , ContentMsg
  , overwriteContentToV6, overwriteV6ToContent
  , contentOurId, contentCounter
  , KeepaliveMsg
  , buildKeepalive, keepaliveOurId, keepaliveCounter
  , keepaliveCheckHmac
  , V6Buffer
  , mkV6Buffer, v6SourceAddr, v6DestAddr, v6NextHeader, v6Payload, readAsV6Len
  -- * For testing
  , getBuf
  ) where

import Data.Bits (shiftR, (.&.))
import Data.Coerce (coerce)
import Data.Primitive (Prim)
import Data.Vector.Storable.Mutable qualified as VM
import GHC.Exts (proxy#)
import GHC.TypeNats (KnownNat, natVal', type (+), type (-), type (<=))
import Network.Socket (SockAddr, Socket, recvBufFrom, sendBufTo)
import Nonsense.AEAD
  (AeadKey, TagLength, decryptEmpty, decryptInPlace, encryptEmpty, encryptInPlace, tagLength)
import Nonsense.Bytes
  (AtLeast, Buffer, allocAtLeast, atLeast, byteLength, isAtLeast, mutWithBytePtr, orMore,
  unsafeSlice, unsafeUncheckedAtLeast, unsafeWithBytePtr)
import Nonsense.Bytes qualified as B
import Nonsense.Cipher (Cipher, Counter)
import Nonsense.ECDH (KeyLength, PubKeyBytes (..))
import Nonsense.Ip6Addr (Ip6Addr (..))
import Nonsense.K12 (Hash, HmacLength, hmacLength)
import RIO hiding (over)
import System.Posix.IO (fdReadBuf, fdWriteBuf)
import System.Posix.Types (Fd (..))

-- #define SAFER

newtype JunkBuffer
  = JunkBuffer (AtLeast JunkBufferSize Buffer)

type JunkBufferSize = 2000

junkBufferSize :: Int
junkBufferSize = 2000

newJunkBuffer :: IO JunkBuffer
newJunkBuffer = JunkBuffer . snd <$> allocAtLeast (const $ pure ())

asJunkBuffer :: AtLeast JunkBufferSize Buffer -> FromJunkBuffer (AtLeast 1500 Buffer)
asJunkBuffer buf = FromJunkBuffer (JunkBuffer buf) (junkBufferSize - contentJunkBufferOffset)

jbWritingArea :: JunkBuffer -> Buffer
jbWritingArea (JunkBuffer buf) = VM.drop contentJunkBufferOffset $ orMore buf

data FromJunkBuffer a
  = FromJunkBuffer JunkBuffer Int

-- | Copy into a 'JunkBuffer' that will work with 'overwriteContentToV6' and
-- 'overwriteV6ToContent'.
copyIntoJunkBuffer :: (Buf a) => JunkBuffer -> a -> IO (FromJunkBuffer a)
copyIntoJunkBuffer junk (getBuf -> buf) = do
    let target = take_ (VM.length buf) (jbWritingArea junk)
    VM.copy target buf
    pure $ FromJunkBuffer junk (VM.length buf)

fromJunkBuffer :: (Buf a) => FromJunkBuffer a -> (JunkBuffer, a)
fromJunkBuffer (FromJunkBuffer junk len) =
    (junk, fromBuf (take_ len (jbWritingArea junk)))

take_ :: Int -> Buffer -> Buffer
#ifdef SAFER
take_ = VM.take
#else
take_ = VM.unsafeTake
#endif

read :: Fd -> JunkBuffer -> IO Buffer
read fd (jbWritingArea -> writingArea) = do
    got <- mutWithBytePtr writingArea $ \ptr ->
        fdReadBuf fd ptr (fromIntegral (VM.length writingArea))
    pure $ take_ (fromIntegral got) writingArea

write :: Fd -> V6Buffer -> IO ()
write fd (V6Buffer buf) = do
    _wrote <- unsafeWithBytePtr buf $ \ptr ->
        fdWriteBuf fd ptr (fromIntegral (byteLength buf))
    pure ()

sockRecv :: Socket -> JunkBuffer -> IO (Buffer, SockAddr)
sockRecv sock (jbWritingArea -> writingArea) = do
    (got, sockAddr) <- mutWithBytePtr writingArea $ \ptr ->
        recvBufFrom sock ptr (VM.length writingArea)
    let content = take_ got writingArea
    pure (content, sockAddr)

sockSend :: Socket -> SockAddr -> Buffer -> IO ()
sockSend sock dest buf = do
    _sent <- unsafeWithBytePtr buf $ \ptr ->
        sendBufTo sock ptr (VM.length buf) dest
    pure ()

-- | Internal class.
class Buf a where
    getBuf :: a -> Buffer
    fromBuf :: Buffer -> a

instance Buf Buffer where
    getBuf = id
    fromBuf = id

#ifdef SAFER
instance (KnownNat n, n <= 1500) => Buf (AtLeast n Buffer) where
#else
instance (n <= 1500) => Buf (AtLeast n Buffer) where
#endif
    getBuf = orMore
    fromBuf = unsafeUncheckedAtLeast

--     0               1               2               3
--    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
--  0 |  0x6  | Traffic Class |               Flow Label              |
--    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
--  4 |         Payload Length        |  Next Header  |   Hop Limit   |
--    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
--  8 |                                                               |
--    +                                                               +
--    |                                                               |
--    +                        Source Address                         +
--    |                                                               |
--    +                                                               +
--    |                                                               |
--    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
-- 24 |                                                               |
--    +                                                               +
--    |                                                               |
--    +                      Destination Address                      +
--    |                                                               |
--    +                                                               +
--    |                                                               |
--    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
newtype V6Buffer
  = V6Buffer (AtLeast 40 Buffer)

instance Buf V6Buffer where
    getBuf (V6Buffer buf) = orMore buf
    fromBuf buf = V6Buffer (unsafeUncheckedAtLeast buf)

type Ipv6HeaderLength = 40

mkV6Buffer :: Buffer -> IO (Maybe V6Buffer)
mkV6Buffer (atLeast @40 -> mbuf) = case mbuf of
    Just buf -> do
        byte <- B.read @0 buf
        len <- readAsV6Len $ isAtLeast buf
        let nibble = (byte .&. 0xF0) `shiftR` 4
        pure $ do
            guard $ nibble == 6
            guard $ fromIntegral len == byteLength buf
            pure $ V6Buffer buf
    Nothing -> pure Nothing

addr :: AtLeast 16 Buffer -> IO Ip6Addr
addr = B.read0

-- | Read the V6 packet payload length and determine the expected length of
-- the _entire_ packet including header.
readAsV6Len :: AtLeast 6 Buffer -> IO Word16
readAsV6Len buf = do
    -- Can't just cast buffer because of byte order
    nbLen <- B.read0 (B.slice @4 buf)
    pure $ byteSwap16 nbLen + 40

v6SourceAddr :: V6Buffer -> IO Ip6Addr
v6SourceAddr (V6Buffer buf) = addr $ B.slice @8 buf

v6DestAddr :: V6Buffer -> IO Ip6Addr
v6DestAddr (V6Buffer buf) = addr $ B.slice @24 buf

v6NextHeader :: V6Buffer -> IO Word8
v6NextHeader (V6Buffer buf) = B.read @6 buf

v6Payload :: V6Buffer -> Buffer
v6Payload (V6Buffer buf) = orMore $ B.drop @40 buf

newtype FlowId
  = FlowId Word32
  deriving stock (Show)
  deriving newtype (B.SizeOf, Eq, Hashable, Ord, Prim, Storable)

--
-- IPv6 addresses are network byte order
-- Flow IDs are little-endian
--

--     0               1               2               3
--    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
--  0 |  Version 0x1  |   Type 0x1    |         Reserved 0x0          |
--    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
--  4 |                         Source FlowId                         |
--    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
--  8 |                                                               |
--    +                                                               +
--    |                                                               |
--    +                                                               +
--    |                                                               |
--    +                                                               +
--    |                                                               |
--    +                        Ephemeral Pubkey                       +
--    |                                                               |
--    +                                                               +
--    |                                                               |
--    +                                                               +
--    |                                                               |
--    +                                                               +
--    |                                                               |
--    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
-- 40 |                                                               |
--    +                            Padding                            +
--    |                                                               |
--    +...............................................................+
-- 56 |                                                               |
--    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
newtype HeloMsg
  = HeloMsg (AtLeast 60 Buffer)

instance Buf HeloMsg where
    getBuf (HeloMsg buf) = orMore buf
    fromBuf buf = HeloMsg $ unsafeUncheckedAtLeast buf

--     0               1               2               3
--    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
--  0 |  Version 0x1  |   Type 0x2    |         Reserved 0x0          |
--    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
--  4 |                      Destination FlowId                       |
--    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
--  8 |                         Source FlowId                         |
--    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
-- 12 |                                                               |
--    +                                                               +
--    |                                                               |
--    +                                                               +
--    |                                                               |
--    +                                                               +
--    |                                                               |
--    +                        Ephemeral Pubkey                       +
--    |                                                               |
--    +                                                               +
--    |                                                               |
--    +                                                               +
--    |                                                               |
--    +                                                               +
--    |                                                               |
--    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
-- 44 |                                                               |
--    +                                                               +
--    |                                                               |
--    +                             HMAC                              +
--    |                                                               |
--    +                                                               +
--    |                                                               |
--    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
newtype TherMsg
  = TherMsg (AtLeast 60 Buffer)

instance Buf TherMsg where
    getBuf (TherMsg buf) = orMore buf
    fromBuf buf = TherMsg $ unsafeUncheckedAtLeast buf

--     0               1               2               3
--    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
--  0 |  Version 0x1  |   Type 0x3    |         Reserved 0x0          |
--    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
--  4 |                      Destination FlowId                       |
--    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
--  8 |                                                               |
--    +                                                               +
--    |                                                               |
--    +                                                               +
--    |                                                               |
--    +                                                               +
--    |                                                               |
--    +                         Static Pubkey                         +
--    |                                                               |
--    +                                                               +
--    |                                                               |
--    +                                                               +
--    |                                                               |
--    +                                                               +
--    |                                                               |
--    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
-- 40 |                                                               |
--    +                                                               +
--    |                                                               |
--    +                             HMAC                              +
--    |                                                               |
--    +                                                               +
--    |                                                               |
--    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
-- 56 |                                                               |
--    +                                                               +
--    |                                                               |
--    +                             HMAC                              +
--    |                                                               |
--    +                                                               +
--    |                                                               |
--    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
newtype GnrlMsg
  = GnrlMsg (AtLeast 72 Buffer)

instance Buf GnrlMsg where
    getBuf (GnrlMsg buf) = orMore buf
    fromBuf buf = GnrlMsg $ unsafeUncheckedAtLeast buf

--       0               1               2               3
--      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
--    0 |  Version 0x1  |   Type 0x4    |         Reserved 0x0          |
--      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
--    4 |                      Destination FlowId                       |
--      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
--    8 |                            Counter                            |
--      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
--   12 |  Next Header  |  0x0  |               Flow Label              |
--      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
--   16 |                                                               |
--      +                            Payload                            +
--      |                                                               |
--      +...............................................................+
--  -16 |                                                               |
--      +                                                               +
--      |                                                               |
--      +                             HMAC                              +
--      |                                                               |
--      +                                                               +
--      |                                                               |
--      +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
newtype ContentMsg
  = ContentMsg (AtLeast (ContentMsgHeaderLength + TagLength) Buffer)

-- | Number of bytes before the IPv6 packet payload.
type ContentMsgHeaderLength = 16

instance Buf ContentMsg where
    getBuf (ContentMsg buf) = orMore buf
    fromBuf buf = ContentMsg $ unsafeUncheckedAtLeast buf

--     0               1               2               3
--    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
--  0 |  Version 0x1  |   Type 0x5    |         Reserved 0x0          |
--    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
--  4 |                      Destination FlowId                       |
--    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
--  8 |                            Counter                            |
--    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
-- 12 |                                                               |
--    +                                                               +
--    |                                                               |
--    +                             HMAC                              +
--    |                                                               |
--    +                                                               +
--    |                                                               |
--    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
newtype KeepaliveMsg
  = KeepaliveMsg (AtLeast 28 Buffer)

instance Buf KeepaliveMsg where
    getBuf (KeepaliveMsg buf) = orMore buf
    fromBuf buf = KeepaliveMsg $ unsafeUncheckedAtLeast buf

data GotMsg
  = Helo HeloMsg
  | Ther TherMsg
  | Gnrl GnrlMsg
  | Content ContentMsg
  | Keepalive KeepaliveMsg

mkGotMsg :: Buffer -> IO (Maybe GotMsg)
mkGotMsg (atLeast @8 -> mbuf) = case mbuf of
    Just buf -> do
        version <- B.read @0 buf
        typ <- B.read @1 buf
        case (version, typ) of
            (1, 1) -> pure $ Helo . HeloMsg <$> exact buf
            (1, 2) -> pure $ Ther . TherMsg <$> exact buf
            (1, 3) -> pure $ Gnrl . GnrlMsg <$> exact buf
            (1, 4) -> pure $ Content . ContentMsg <$> atLeast (orMore buf)
            (1, 5) -> pure $ Keepalive . KeepaliveMsg <$> exact buf
            _      -> pure Nothing
    Nothing -> pure Nothing
  where
    exact :: forall n. (KnownNat n) => AtLeast 8 Buffer -> Maybe (AtLeast n Buffer)
    exact = guarding ((== l) . byteLength) <=< atLeast . orMore
      where
        l = fromIntegral $ natVal' @n proxy#
        guarding p n = n <$ guard (p n)
    {-# INLINE exact #-}

flowId :: AtLeast 4 Buffer -> IO FlowId
flowId = B.read0

heloTheirId :: HeloMsg -> IO FlowId
heloTheirId (HeloMsg buf) = flowId $ B.slice @4 buf

-- | View of underlying buffer.
heloTheirEphPubkey :: HeloMsg -> PubKeyBytes Buffer
heloTheirEphPubkey (HeloMsg buf) = PubKeyBytes $ B.slice @8 buf

therTheirId :: TherMsg -> IO FlowId
therTheirId (TherMsg buf) = flowId $ B.slice @8 buf

therOurId :: TherMsg -> IO FlowId
therOurId (TherMsg buf) = flowId $ B.slice @4 buf

-- | View of underlying buffer.
therTheirEphPubkey :: TherMsg -> PubKeyBytes Buffer
therTheirEphPubkey (TherMsg buf) = PubKeyBytes $ B.slice @12 buf

-- | View of underlying buffer.
therHmac :: TherMsg -> AtLeast TagLength Buffer
therHmac (TherMsg buf) = B.slice @44 buf

therCheckHmac :: (B.Bytes ba) => AeadKey -> Hash ba -> TherMsg -> IO a -> IO a -> IO a
therCheckHmac k h msg fc tc = bool fc tc =<< decryptEmpty k 0 h (therHmac msg)

gnrlOurId :: GnrlMsg -> IO FlowId
gnrlOurId (GnrlMsg buf) = flowId $ B.slice @4 buf

-- | Mutates message to decrypt key. Returns view of underlying buffer.
gnrlConsumeTsuk ::
    (B.Bytes ba)
 => AeadKey
 -> Cipher -- ^ A Cipher already keyed via keyCipher
 -> Hash ba
 -> GnrlMsg
 -> IO (Maybe (PubKeyBytes Buffer))
gnrlConsumeTsuk k cipher h (GnrlMsg buf) =
    coerce $ decryptInPlace cipher k 1 h (B.slice @8 @(KeyLength + TagLength) buf)

-- | View of underlying buffer.
gnrlTsukTagged :: GnrlMsg -> AtLeast (KeyLength + TagLength) Buffer
gnrlTsukTagged (GnrlMsg buf) = B.slice @8 buf

gnrl2ndHmac :: GnrlMsg -> AtLeast 16 Buffer
gnrl2ndHmac (GnrlMsg buf) = B.slice @56 buf

gnrlCheck2ndHmac :: (B.Bytes ba) => AeadKey -> Hash ba -> GnrlMsg -> IO a -> IO a -> IO a
gnrlCheck2ndHmac k h msg fc tc = bool fc tc =<< decryptEmpty k 2 h (gnrl2ndHmac msg)

contentOurId :: ContentMsg -> IO FlowId
contentOurId (ContentMsg buf) = flowId $ B.slice @4 buf

contentCounter :: ContentMsg -> IO Counter
contentCounter (ContentMsg buf) = B.read0 (B.slice @8 buf)

contentPayloadLen :: ContentMsg -> Int
contentPayloadLen (ContentMsg buf) =
    byteLength buf - contentMsgHeaderLength - tagLength

keepaliveOurId :: KeepaliveMsg -> IO FlowId
keepaliveOurId (KeepaliveMsg buf) = flowId $ B.slice @4 buf

keepaliveCounter :: KeepaliveMsg -> IO Counter
keepaliveCounter (KeepaliveMsg buf) = B.read0 (B.slice @8 buf)

keepaliveCheckHmac :: Counter -> AeadKey -> KeepaliveMsg -> IO a -> IO a -> IO a
keepaliveCheckHmac count k (KeepaliveMsg buf) fc tc = bool fc tc =<<
    decryptEmpty k count (mempty :: ByteString) (B.slice @12 buf)

writeHeader :: AtLeast 4 Buffer -> Word8 -> IO ()
writeHeader buf typ = do
    B.write @0 buf 1
    B.write @1 buf typ
    B.write @2 buf 0
    B.write @3 buf 0

writeFlowId :: AtLeast 4 Buffer -> FlowId -> IO ()
writeFlowId buf =
    B.write @0 (B.reinterpretV (B.slice @0 @4 buf))

writeAddr :: AtLeast 16 Buffer -> Ip6Addr -> IO ()
writeAddr buf =
    B.write @0 (B.reinterpretV (B.slice @0 @16 buf))

writePubkey :: (B.Bytes ba) => AtLeast KeyLength Buffer -> PubKeyBytes ba -> IO ()
writePubkey buf (PubKeyBytes bs) = B.copy buf bs

writeCounter :: AtLeast 4 Buffer -> Counter -> IO ()
writeCounter buf =
    B.write @0 (B.reinterpretV (B.slice @0 @4 buf))

buildHelo :: (B.Bytes ba) => JunkBuffer -> FlowId -> PubKeyBytes ba -> IO HeloMsg
buildHelo (JunkBuffer buf) ourId ourEphPk = do
    writeHeader (isAtLeast buf) 1
    writeFlowId (B.slice @4 buf) ourId
    writePubkey (B.slice @8 buf) ourEphPk
    B.fill (B.slice @40 @20 buf) 0
    pure . HeloMsg $ B.slice @0 @60 buf

buildTher ::
    (B.Bytes ba, B.Bytes bb)
 => JunkBuffer
 -> FlowId
 -> PubKeyBytes ba
 -> FlowId
 -> AeadKey
 -> Hash bb
 -> IO TherMsg
buildTher (JunkBuffer buf) ourId ourEphPk theirId k h = do
    writeHeader (isAtLeast buf) 2
    writeFlowId (B.slice @4  buf) theirId
    writeFlowId (B.slice @8  buf) ourId
    writePubkey (B.slice @12 buf) ourEphPk
    _ <- encryptEmpty k 0 h (B.slice @44 buf)
    pure . TherMsg $ B.slice @0 @60 buf

newtype LtMsg
  = LtMsg JunkBuffer

-- | View of underlying buffer.
ltTsukTagged :: LtMsg -> B.AtLeast (KeyLength + TagLength) Buffer
ltTsukTagged (LtMsg (JunkBuffer buf)) = gnrlTsukTagged (GnrlMsg (isAtLeast buf))

buildLt ::
    (B.Bytes ba, B.Bytes bb)
 => JunkBuffer
 -> PubKeyBytes ba
 -> FlowId
 -> AeadKey
 -> Cipher -- ^ A Cipher already keyed via keyCipher
 -> Hash bb
 -> IO LtMsg
buildLt (JunkBuffer buf) ourStPk theirId k cipher h = do
    writeHeader (isAtLeast buf) 3
    writeFlowId (B.slice @4 buf) theirId
    writePubkey (B.slice @8 buf) ourStPk
    _ <- encryptInPlace cipher k 1 h (B.slice @8 @(KeyLength + HmacLength) buf)
    pure . LtMsg $ JunkBuffer buf

buildGnrl ::
    (B.Bytes ba)
 => AeadKey
 -> Hash ba
 -> LtMsg
 -> IO GnrlMsg
buildGnrl k h (LtMsg (JunkBuffer buf)) = do
    _ <- encryptEmpty k 2 h (B.slice @56 buf)
    pure . GnrlMsg $ B.slice @0 @72 buf

buildKeepalive :: JunkBuffer -> FlowId -> AeadKey -> Counter -> IO KeepaliveMsg
buildKeepalive (JunkBuffer buf) theirId k count = do
    writeHeader (isAtLeast buf) 5
    writeFlowId (B.slice @4 buf) theirId
    writeCounter (B.slice @8 buf) count
    _ <- encryptEmpty k count (mempty :: ByteString) (B.slice @12 buf)
    pure . KeepaliveMsg $ B.slice @0 @28 buf

contentMsgHeaderLength :: Int
contentMsgHeaderLength = fromIntegral $ natVal' (proxy# @ContentMsgHeaderLength)

ipv6HeaderLength :: Int
ipv6HeaderLength = fromIntegral $ natVal' (proxy# @Ipv6HeaderLength)

contentJunkBufferOffset :: Int
contentJunkBufferOffset = ipv6HeaderLength - contentMsgHeaderLength

-- | 'ContentMsg' must be at offset 'contentJunkBufferOffset' in the
-- 'JunkBuffer'. 'sockRecv' guarantees this.
overwriteContentToV6 ::
    JunkBuffer
 -> ContentMsg
 -> Ip6Addr
 -> Ip6Addr
 -> AeadKey
 -> Cipher  -- ^ A Cipher already keyed via keyCipher
 -> Counter
 -> IO (Maybe V6Buffer)
overwriteContentToV6 (JunkBuffer buf) msg@(ContentMsg cont) ourAddr theirAddr k cipher count =
    decryptInPlace cipher k count (mempty @ByteString) (B.drop @12 cont) >>= traverse \_ -> do
        -- 0x6 and 4 bits of traffic class
        B.write @0 buf 0x60
        -- Flow label
        B.copy
            (B.slice @1  @3 buf)
            (B.slice @13 @3 cont)
        -- Payload length
        let payloadLen = fromIntegral $
                byteLength cont - contentMsgHeaderLength - tagLength
        B.write @0 (B.reinterpretV (B.slice @4 @2 buf)) (byteSwap16 payloadLen)
        -- Next header
        B.copy
            (B.slice @6  @1 buf)
            (B.slice @12 @1 cont)
        -- Hop limit
        B.write @7 buf 0
        -- Source & dest addresses
        writeAddr (B.slice @8  buf) theirAddr
        writeAddr (B.slice @24 buf) ourAddr
        let pkt = take_ (ipv6HeaderLength + contentPayloadLen msg) (orMore buf)
        pure . V6Buffer $ unsafeUncheckedAtLeast @Ipv6HeaderLength pkt

-- | 'V6Buffer' must be at offset 'contentJunkBufferOffset' in the
-- 'JunkBuffer'. 'read' guarantees this.
overwriteV6ToContent ::
    JunkBuffer
 -> V6Buffer
 -> FlowId
 -> AeadKey
 -> Cipher  -- ^ A Cipher already keyed via keyCipher
 -> Counter
 -> IO ContentMsg
overwriteV6ToContent (JunkBuffer buf) (V6Buffer v6) theirId k cipher count = do
    -- Reserve the full length of the HMAC, and prune it before returning
    let msgBuf = unsafeUncheckedAtLeast @(ContentMsgHeaderLength + HmacLength) $
            unsafeSlice
                -- Start at (payload index - contentMsgHeaderLength)
                (contentJunkBufferOffset + contentJunkBufferOffset)
                (contentMsgHeaderLength + hmacLength + bodyLen)
                (orMore buf)
        bodyLen = byteLength v6 - ipv6HeaderLength
    -- Flow label
    B.copy
        (B.slice @12 @4 msgBuf)
        (B.slice @0  @4 v6)
    B.write @13 msgBuf . (.&. 0x0F) =<< B.read @13 msgBuf
    -- Next header
    B.copy
        (B.slice @12 @1 msgBuf)
        (B.slice @6  @1 v6)
    -- Insert ContentMsg header
    writeHeader (isAtLeast msgBuf) 4
    writeFlowId (B.slice @4 msgBuf) theirId
    writeCounter (B.slice @8 msgBuf) count

    _ <- encryptInPlace cipher k count (mempty @ByteString) (B.drop @12 msgBuf)

    -- Drop the last 16 bytes of the HMAC
    pure . ContentMsg $ B.dropBack @(HmacLength - TagLength) msgBuf
