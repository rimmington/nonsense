{-# language MagicHash, StrictData #-}

module Nonsense.RingBuffer
  ( RingBuffer
  , newAlignedPinnedRingBuffer
  , blockSize, null, toRead
  , readVector, withWritePtr, writeVector
  -- * Committing
  , CommitResult (..)
  , unsafeCommitWrite, unsafeCommitRead
  -- * Unsafe pointer access
  , unsafeWritePtr, unsafeReadPtr
  ) where

import Control.Monad.Primitive (touch)
import Data.Primitive
  (MutableByteArray (..), Ptr (..), alignment, mutableByteArrayContents, newAlignedPinnedByteArray,
  sizeofMutableByteArray)
import Data.Primitive.PVar (PVar, RW, atomicWriteIntPVar, newPVar, readPVar)
import Data.Vector.Storable.Mutable qualified as VM
import Foreign.Ptr (plusPtr)
import GHC.ForeignPtr (ForeignPtr (..), ForeignPtrContents (..))
import RIO hiding (null)

-- | Ring buffer with fixed element size.
data RingBuffer
  = RingBuffer
    { readPos  :: {-# UNPACK #-}PVar Int RW
    , buf      :: {-# UNPACK #-}MutableByteArray RW
    , elemSize :: {-# UNPACK #-}Int
    , writePos :: {-# UNPACK #-}PVar Int RW
    }

-- | Create a new empty Int-aligned ring buffer, given the element/block size and capacity.
newAlignedPinnedRingBuffer :: Int -> Int -> IO RingBuffer
newAlignedPinnedRingBuffer elemSize cap = do
    readPos <- newPVar 0
    buf <- newAlignedPinnedByteArray ((cap + 1) * elemSize) (alignment (0 :: Int))
    writePos <- newPVar 0
    pure RingBuffer{readPos, buf, elemSize, writePos}
{-# INLINABLE newAlignedPinnedRingBuffer #-}

-- | The size of blocks provided by 'nextWritePtr' and 'nextReadPtr'.
blockSize :: RingBuffer -> Int
blockSize = elemSize

-- | Is the buffer empty?
null :: RingBuffer -> IO Bool
null RingBuffer{readPos, writePos} = do
    rp <- readPVar readPos
    wp <- readPVar writePos
    pure $ rp == wp
{-# INLINABLE null #-}

-- | How many elements are available to read?
toRead :: RingBuffer -> IO Int
toRead RingBuffer{..} = do
    rp <- readPVar readPos
    wp <- readPVar writePos
    let wp'
          | wp >= rp  = wp
          | otherwise = wp + sizeofMutableByteArray buf
    pure $ (wp' - rp) `div` elemSize

-- | Fetch the next contigious block for writing.
--
-- Does not keep the buffer alive.
unsafeWritePtr :: RingBuffer -> IO (Ptr Word8)
unsafeWritePtr RingBuffer{writePos, buf} = do
    wp <- readPVar writePos
    pure $ mutableByteArrayContents buf `plusPtr` wp
{-# INLINE unsafeWritePtr #-}

withWritePtr :: RingBuffer -> (Ptr Word8 -> IO a) -> IO a
withWritePtr RingBuffer{writePos, buf} f = do
    wp <- readPVar writePos
    a <- f $ mutableByteArrayContents buf `plusPtr` wp
    touch buf
    pure a
{-# INLINE withWritePtr #-}

writeVector :: RingBuffer -> IO (VM.IOVector Word8)
writeVector RingBuffer{writePos, buf = buf@(MutableByteArray buf#), elemSize} = do
    wp <- readPVar writePos
    let !(Ptr addr#) = mutableByteArrayContents buf `plusPtr` wp
        fptr = ForeignPtr addr# (PlainPtr buf#)
    pure $ VM.MVector elemSize fptr
{-# INLINE writeVector #-}

-- | Were the bytes committed?
data CommitResult = Committed | Full deriving (Eq, Show)

-- | Make a block available to the reader after writing to a block provided by 'unsafeWritePtr'.
-- This can fail if the reader is too far behind.
--
-- Call this at most once after 'unsafeWritePtr'; does not check if you have done so.
unsafeCommitWrite :: RingBuffer -> IO CommitResult
unsafeCommitWrite RingBuffer{readPos, buf, writePos, elemSize} = do
    rp <- readPVar readPos
    wp <- readPVar writePos
    let allocated = sizeofMutableByteArray buf
        -- Wrap around buffer
        next = case wp + elemSize of
            prop | prop < allocated -> prop
                 | otherwise        -> 0
    -- Not allowed to catch up to reader
    case next == rp of
        True  -> pure Full
        False -> Committed <$ atomicWriteIntPVar writePos next
{-# INLINABLE unsafeCommitWrite #-}

-- | Fetch the next contigious block for reading.
--
-- Does not keep the buffer alive.
unsafeReadPtr :: RingBuffer -> IO (Maybe (Ptr Word8))
unsafeReadPtr RingBuffer{readPos, buf, writePos} = do
    rp <- readPVar readPos
    wp <- readPVar writePos
    case rp == wp of
        True  -> pure Nothing
        False -> pure . Just $ mutableByteArrayContents buf `plusPtr` rp
{-# INLINE unsafeReadPtr #-}

readVector :: RingBuffer -> IO (Maybe (VM.IOVector Word8))
readVector RingBuffer{readPos, buf = buf@(MutableByteArray buf#), writePos, elemSize} = do
    rp <- readPVar readPos
    wp <- readPVar writePos
    case rp == wp of
        True  -> pure Nothing
        False -> pure . Just $
            let !(Ptr addr#) = mutableByteArrayContents buf `plusPtr` rp
                fptr = ForeignPtr addr# (PlainPtr buf#)
            in VM.MVector elemSize fptr
{-# INLINE readVector #-}

-- | Mark a block provided by 'unsafeReadPtr' as read.
--
-- Call this at most once after 'unsafeReadPtr'; does not check if you have done so.
unsafeCommitRead :: RingBuffer -> IO ()
unsafeCommitRead RingBuffer{readPos, buf, elemSize} = do
    rp <- readPVar readPos
    let allocated = sizeofMutableByteArray buf
        -- Wrap around buffer
        next = case rp + elemSize of
            prop | prop < allocated -> prop
                 | otherwise        -> 0
    atomicWriteIntPVar readPos next
{-# INLINE unsafeCommitRead #-}
