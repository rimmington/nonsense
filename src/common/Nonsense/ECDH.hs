{-# language StrictData, TemplateHaskell #-}

module Nonsense.ECDH
  ( PrivKeyBytes (..), PrivKey
  , loadPrivKey, privKeyBytesGen
  , PubKeyBytes (..)
  , privPubKey
  , SharedSecret (..)
  , keyAgreement
  , KeyLength
  , keyLength
  ) where

import Control.Monad.Primitive (unsafePrimToPrim)
import Foreign (FinalizerPtr, ForeignPtr, alloca, newForeignPtr, peek, pokeByteOff, withForeignPtr)
import Language.C.Inline qualified as C
import Nonsense.Bytes (AtLeast, Bytes, Scrubbed, allocAtLeast, unsafeWithBytePtr)
import RIO
import System.IO.Unsafe (unsafePerformIO)

C.context (C.baseCtx <> C.bsCtx)
C.include "<botan/ffi.h>"

newtype PrivKey
  = PrivKey (ForeignPtr ())

instance Show PrivKey where
    show _ = "PrivKey"

newtype PrivKeyBytes
  = PrivKeyBytes (AtLeast KeyLength Scrubbed)
  deriving newtype (NFData, Show)

newtype PubKeyBytes ba
  = PubKeyBytes (AtLeast KeyLength ba)
  deriving newtype (Bytes, Eq, NFData, Show)

newtype SharedSecret
  = SharedSecret (AtLeast KeyLength Scrubbed)
  deriving newtype (Bytes, Show)

type KeyLength = 32

keyLength :: Int
keyLength = 32

privKeyBytesGen :: ST s Word32 -> ST s PrivKeyBytes
privKeyBytesGen g32 = fmap (PrivKeyBytes . snd) . unsafePrimToPrim $ allocAtLeast $ \ptr -> do
    pokeByteOff ptr 0  =<< unsafePrimToPrim g32
    pokeByteOff ptr 4  =<< unsafePrimToPrim g32
    pokeByteOff ptr 8  =<< unsafePrimToPrim g32
    pokeByteOff ptr 12 =<< unsafePrimToPrim g32
    pokeByteOff ptr 16 =<< unsafePrimToPrim g32
    pokeByteOff ptr 20 =<< unsafePrimToPrim g32
    pokeByteOff ptr 24 =<< unsafePrimToPrim g32
    pokeByteOff ptr 28 =<< unsafePrimToPrim g32

botanPrivkeyDestroy :: FinalizerPtr ()
botanPrivkeyDestroy =
    [C.funPtr| void fr(void *ptr) {
        botan_privkey_destroy(ptr);
    } |]

loadPrivKey :: PrivKeyBytes -> PrivKey
loadPrivKey (PrivKeyBytes bytes) = unsafePerformIO . mask_ $
    unsafeWithBytePtr bytes $ \pbytes -> alloca $ \ptrptr ->
        [C.exp| int {
            botan_privkey_load_x25519(
                (botan_privkey_t *)$(void **ptrptr),
                $(uint8_t *pbytes)
            )
        } |] >>= \case
            0 -> do
                ptr <- peek ptrptr
                fp <- newForeignPtr botanPrivkeyDestroy ptr
                pure $ PrivKey fp
            _ -> error "bug: loadPrivKey failure"

privPubKey :: PrivKey -> PubKeyBytes ByteString
privPubKey (PrivKey pfp) = unsafePerformIO . mask_ $
    withForeignPtr pfp $ \priv -> allocAtLeast (\pub ->
        [C.block| int {
            botan_pubkey_t pk;
            int res = botan_privkey_export_pubkey(&pk, $(void *priv));
            if (res != 0) return res;
            res = botan_pubkey_x25519_get_pubkey(pk, $(uint8_t *pub));
            botan_pubkey_destroy(pk);
            return res;
        } |]) >>= \case
            (0, pb) -> pure $ PubKeyBytes pb
            _       -> error "bug: privPubKey failed"

keyAgreement :: (Bytes ba) => PrivKey -> PubKeyBytes ba -> Maybe SharedSecret
keyAgreement (PrivKey privFp) (PubKeyBytes pub) = unsafePerformIO . mask_ $
    withForeignPtr privFp $ \privPtr ->
    unsafeWithBytePtr pub $ \pubPtr ->
        allocAtLeast (\ssPtr ->
            [C.block| int {
                botan_pk_op_ka_t ka;
                int res = botan_pk_op_key_agreement_create(
                    &ka,
                    (botan_privkey_t)$(void *privPtr),
                    "Raw",
                    0);
                if (res != 0) goto quit;
                size_t outLen = 32;
                res = botan_pk_op_key_agreement(
                    ka,
                    $(uint8_t *ssPtr),
                    &outLen,
                    $(uint8_t *pubPtr),
                    32,
                    NULL,
                    0
                );
                quit:
                botan_pk_op_key_agreement_destroy(ka);
                return res;
            } |]
        ) >>= \case
            (0, sb) -> pure . Just $ SharedSecret sb
            _       -> pure Nothing

instance NFData PrivKey where
    rnf p = p `seq` ()
