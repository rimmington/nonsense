{-# language StrictData, TemplateHaskell #-}

module Nonsense.Tun (ErrnoMsg (..), open) where

import Foreign.C.Error (Errno (..))
import Foreign.Marshal.Alloc (allocaBytes)
import qualified Language.C.Inline as C
import qualified Nonsense.Constants as Const
import Nonsense.Ip6Addr (Ip6Addr (..))
import RIO
import RIO.ByteString (packCString)
import System.Posix.Types (Fd (..))

-- Make sure we don't get the nasty GNU strerror_r
C.verbatim "#ifdef _GNU_SOURCE"
C.verbatim "#error \"Someone set _GNU_SOURCE\""
C.verbatim "#endif"

-- Constants
C.verbatim ("#define DEVICE " ++ show Const.deviceName)
C.verbatim ("#define MTU " ++ show Const.mtu)

C.include "<stdio.h>"        -- perror

C.include "<errno.h>"        -- errno
C.include "<fcntl.h>"        -- open()
C.include "<string.h>"       -- strncpy(), strerror_r()
C.include "<stdint.h>"       -- uint64_t
C.include "<sys/ioctl.h>"    -- ioctl()
C.include "<sys/socket.h>"   -- socket, AF_INET6, SOCK_DGRAM
                             -- required for <linux/if.h>
C.include "<unistd.h>"       -- close()

C.include "<linux/if.h>"     -- ifreq, IFNAMSIZ, SIOC*
C.include "<linux/if_tun.h>" -- TUNSETIFF, IFF_TUN, IFF_NO_PI
C.include "<linux/ipv6.h>"   -- in6_ifreq

data ErrnoMsg = ErrnoMsg Errno ByteString

-- TODO: replace perror with return code indicating failure point
open :: Ip6Addr -> IO (Either ErrnoMsg Fd)
open (Ip6Addr h l) = allocaBytes maxErrMsg $ \errMsg -> do
    ret <- [C.block| int {
        struct ifreq ifr;
        struct in6_ifreq ifr6;
        int fd, sockfd, err;

        if ((fd = open("/dev/net/tun", O_RDWR)) < 0) {
          err = errno;
          goto err_open;
        }

        memset(&ifr, 0, sizeof(ifr));
        ifr.ifr_flags = IFF_TUN | IFF_NO_PI;
        // Fill ifr_name with dev + \0s
        strncpy(ifr.ifr_name, DEVICE, IFNAMSIZ);

        if (ioctl(fd, TUNSETIFF, &ifr) < 0) {
          err = errno;
          perror("TUNSETIFF");
          goto err_tunsetiff;
        }

        if ((sockfd = socket(AF_INET6, SOCK_DGRAM, 0)) < 0) {
          err = errno;
          perror("SOCKFD");
          goto err_tunsetiff;
        }

        if (ioctl(sockfd, SIOGIFINDEX, &ifr) < 0) {
          err = errno;
          perror("INDEX");
          goto err_sioc;
        }

        memset(&ifr6, 0, sizeof(ifr6));
        ifr6.ifr6_ifindex = ifr.ifr_ifindex;
        ((uint64_t*) ifr6.ifr6_addr.s6_addr32)[0] = $(uint64_t h);
        ((uint64_t*) ifr6.ifr6_addr.s6_addr32)[1] = $(uint64_t l);
        ifr6.ifr6_prefixlen = 64;

        ifr.ifr_mtu = MTU;
        if (ioctl(sockfd, SIOCSIFMTU, &ifr) < 0) {
          err = errno;
          perror("MTU");
          goto err_sioc;
        }

        if (ioctl(sockfd, SIOCSIFADDR, &ifr6) < 0) {
          err = errno;
          perror("ADDR");
          goto err_sioc;
        }

        // Specifying all flags required; disable multicast
        ifr.ifr_flags = IFF_UP | IFF_POINTOPOINT | IFF_NOARP;
        if (ioctl(sockfd, SIOCSIFFLAGS, &ifr) < 0) {
          err = errno;
          perror("SET");
          goto err_sioc;
        }

        close(sockfd);

        return fd;

        err_sioc:
          close(sockfd);
        err_tunsetiff:
          close(fd);
        err_open:
          // Apparently this can fail (?!)
          // Assume it succeeds anyway
          strerror_r(err, $(char* errMsg), $(size_t cMaxErrMsg));
          return -err;
      } |]
    bool (pure (Right (Fd ret))) (Left . ErrnoMsg (Errno (negate ret)) <$> packCString errMsg) $ ret < 0
  where
    maxErrMsg  = 45
    cMaxErrMsg = fromIntegral maxErrMsg
