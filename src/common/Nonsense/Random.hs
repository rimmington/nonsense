{-# language StrictData #-}

module Nonsense.Random
    ( Seed (..)
    , systemRandomSeed, decodeSeed, encodeSeed, seedFromBS
    , FrozenGen, Gen, GenIO, GenST
    , freezeGen, seedGen, thawGen
    , withFrozen, uniformW32
    ) where

import Data.Base16.Types (extractBase16)
import Data.Bits (shiftL, shiftR, (.|.))
import Data.ByteString.Base16 (decodeBase16Untyped, encodeBase16)
import RIO
import RIO.ByteString (pack, unpack)
import System.Entropy (getEntropy)
import System.Random.PCG (FrozenGen, Gen, GenIO, GenST, initFrozen, withFrozen)
import System.Random.PCG qualified as PCG

data Seed
  = Seed Word64 Word64
  deriving (Eq, Show)

systemRandomSeed :: IO Seed
systemRandomSeed = do
    bs <- getEntropy 16
    case seedFromBS bs of
        Just s  -> pure s
        Nothing -> error "getEntropy failure"

encodeSeed :: Seed -> Text
encodeSeed (Seed hh hl) = extractBase16 . encodeBase16 $ pack
    [ fromIntegral $ hh `shiftR` 56
    , fromIntegral $ hh `shiftR` 48
    , fromIntegral $ hh `shiftR` 40
    , fromIntegral $ hh `shiftR` 32
    , fromIntegral $ hh `shiftR` 24
    , fromIntegral $ hh `shiftR` 16
    , fromIntegral $ hh `shiftR` 8
    , fromIntegral   hh
    , fromIntegral $ hl `shiftR` 56
    , fromIntegral $ hl `shiftR` 48
    , fromIntegral $ hl `shiftR` 40
    , fromIntegral $ hl `shiftR` 32
    , fromIntegral $ hl `shiftR` 24
    , fromIntegral $ hl `shiftR` 16
    , fromIntegral $ hl `shiftR` 8
    , fromIntegral   hl
    ]

decodeSeed :: Text -> Maybe Seed
decodeSeed t = either (const Nothing) seedFromBS $ decodeBase16Untyped (encodeUtf8 t)

seedFromBS :: ByteString -> Maybe Seed
seedFromBS bs = case unpack bs of
    [w0, w1, w2, w3, w4, w5, w6, w7, w8, w9, w10, w11, w12, w13, w14, w15] ->
        Just $ Seed (dec w0 w1 w2 w3 w4 w5 w6 w7) (dec w8 w9 w10 w11 w12 w13 w14 w15)
    _ -> Nothing
  where
    dec w0 w1 w2 w3 w4 w5 w6 w7 =
        (fromIntegral w0 `shiftL` 56) .|.
        (fromIntegral w1 `shiftL` 48) .|.
        (fromIntegral w2 `shiftL` 40) .|.
        (fromIntegral w3 `shiftL` 32) .|.
        (fromIntegral w4 `shiftL` 24) .|.
        (fromIntegral w5 `shiftL` 16) .|.
        (fromIntegral w6 `shiftL` 8 ) .|.
         fromIntegral w7

uniformW32 :: GenST s -> ST s Word32
uniformW32 = PCG.uniformW32

freezeGen :: PrimMonad m => Gen (PrimState m) -> m FrozenGen
freezeGen = PCG.save

thawGen :: PrimMonad m => FrozenGen -> m (Gen (PrimState m))
thawGen = PCG.restore

seedGen :: Seed -> FrozenGen
seedGen (Seed hh hl) = initFrozen hh hl
