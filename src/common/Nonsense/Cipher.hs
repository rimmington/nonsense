{-# language StrictData, TemplateHaskell, UndecidableInstances #-}

module Nonsense.Cipher
  ( Cipher, Counter (..), initCipher, setKey, cipherInPlace
  ) where

import Data.Primitive (Prim)
import Foreign
  (ForeignPtr, FunPtr, Ptr, addForeignPtrFinalizer, castPtr, mallocForeignPtrBytes, pokeElemOff,
  withForeignPtr)
import Language.C.Inline.Cpp qualified as C
import Nonsense.Bytes
  (AtLeast, Buffer, Bytes (byteLength), Mutable, SizeOf, allocAtLeast, mutWithBytePtr,
  unsafeWithBytePtr)
import Nonsense.ECDH (SharedSecret (SharedSecret))
import RIO

C.context C.cppCtx
C.include "<botan/camellia.h>"
C.include "<botan/ofb.h>"

data Cipher
  = Cipher (ForeignPtr ()) (AtLeast 16 Buffer)

newtype Counter
  = Counter Word32
  deriving newtype (Bounded, Enum, Eq, Integral, Num, Ord, Prim, Real, Show, SizeOf, Storable)

sizeofOFB :: Int
sizeofOFB = fromIntegral [C.pure| int { sizeof(Botan::OFB) } |]

destroyCipher :: FunPtr (Ptr () -> IO ())
destroyCipher = [C.funPtr| void destroy(void *ofb) {
    ((Botan::OFB*)ofb)->~OFB();
} |]

initCipher :: IO Cipher
initCipher = mask_ do
    fp <- mallocForeignPtrBytes sizeofOFB
    -- OFB takes ownership via a unique_ptr, so we just allocate Camellia
    -- and pass it in.
    withForeignPtr fp $ \ofb -> [C.block| void {
        Botan::Camellia_256 *cm = new Botan::Camellia_256;
        new ($(void *ofb)) Botan::OFB(cm);
    } |]
    addForeignPtrFinalizer destroyCipher fp
    (_, nonce) <- allocAtLeast $ \ptr ->
        pokeElemOff (castPtr ptr) 0 (0 :: Word64)
    pure $ Cipher fp nonce

setKey :: Cipher -> SharedSecret -> IO ()
setKey (Cipher fp _) (SharedSecret k) = withForeignPtr fp $ \ofb -> unsafeWithBytePtr k $ \kp ->
    [C.block| void {
        ((Botan::OFB*)$(void *ofb))->set_key($(uint8_t* kp), 32);
    } |]

cipherInPlace ::
    (Mutable ba)
 => Cipher
 -> Counter
 -> ba
 -> IO ()
cipherInPlace (Cipher fp n) (Counter count) buf =
    withForeignPtr fp $ \ofb ->
    mutWithBytePtr n $ \nonce ->
    mutWithBytePtr buf $ \bytes -> do
        pokeElemOff (castPtr nonce) 1 (fromIntegral count :: Word64)
        [C.block| void {
            ((Botan::OFB*)$(void *ofb))->set_iv($(uint8_t* nonce), 16);
            ((Botan::OFB*)$(void *ofb))->cipher1($(uint8_t* bytes), $(size_t len));
        } |]
  where
    len = fromIntegral $ byteLength buf
