{-# language TemplateHaskell #-}

module Nonsense.LinuxCaps (checkNoCaps, dropAllCapsAndCheck) where

import Language.C.Inline qualified as C
import RIO

C.include "<stdbool.h>"
C.include "<cap-ng.h>"

-- | Drop all capabilities from the effective, bounding and ambient sets.
dropAllCapsAndCheck :: IO ()
dropAllCapsAndCheck = dropAllCaps *> checkNoCaps

-- | Throw if any capabilities remain in the effective, bounding or ambient sets.
checkNoCaps :: IO ()
checkNoCaps = haveAnyCaps >>= (`when` throwIO DropCapabilitiesFailed)

haveAnyCaps :: IO Bool
haveAnyCaps = (/= C.CBool 0) <$> [C.exp| bool { capng_get_caps_process() || capng_have_capabilities(CAPNG_SELECT_ALL) > CAPNG_NONE } |]

dropAllCaps :: IO ()
dropAllCaps = [C.block| void {
    capng_clear(CAPNG_SELECT_ALL);
    capng_apply(CAPNG_SELECT_ALL);
} |]

data DropCapabilitiesFailed = DropCapabilitiesFailed
  deriving (Exception, Show)
