module Nonsense.MonoTime (
    MonoTime32 (..), leq, monoTime32Coarse
  , MonoTime64 (..), monoTime64Coarse
  ) where

import Data.Primitive (Prim)
import RIO
import System.Clock (Clock (MonotonicCoarse), TimeSpec (..), getTime)

-- | Time, in milliseconds, since an arbitrary epoch.
newtype MonoTime32
  = MonoTime32 { ms32 :: Word32 }
  deriving newtype (Eq, Prim)
  deriving stock (Show)

-- | Time, in milliseconds, since an arbitrary epoch.
newtype MonoTime64
  = MonoTime64 { ms64 :: Word64 }
  deriving newtype (Eq, Prim)
  deriving stock (Show)

monoTime32Coarse :: IO MonoTime32
monoTime32Coarse = conv <$> getTime MonotonicCoarse
  where
    conv TimeSpec{sec, nsec} = MonoTime32 . fromIntegral $ (sec * 1e3) + (nsec `div` 10e6)

monoTime64Coarse :: IO MonoTime64
monoTime64Coarse = conv <$> getTime MonotonicCoarse
  where
    conv TimeSpec{sec, nsec} = MonoTime64 . fromIntegral $ (sec * 1e3) + (nsec `div` 10e6)

leq ::
    MonoTime32 -- ^ a time before a or b
 -> Word32     -- ^ a
 -> Word32     -- ^ b
 -> Bool
leq past a b
  | ms32 past > a && ms32 past > b = a <= b
  | ms32 past > a                  = False -- a overflowed, b did not
  | ms32 past > b                  = True  -- b overflowed, a did not
  | otherwise                      = a <= b
