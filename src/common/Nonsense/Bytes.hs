{-# language AllowAmbiguousTypes, CPP, GADTs, TemplateHaskell, TypeFamilies #-}
{-# options_ghc -Wno-redundant-constraints #-}
{- HLINT ignore "Redundant lambda" -}

module Nonsense.Bytes
  ( Bytes (..), Mutable (..), Uninitialised (..)
  , allocRet, clone, pack
  , Buffer, Scrubbed
  , AtLeast
  , atLeast, allocAtLeast, cloneAtLeast, unsafeAtLeast, unsafeUncheckedAtLeast
  , orMore, isAtLeast
  , read, write, copy, fill, slice, sliceBack, drop, dropBack
  , indexByteString
  , constEqMem
  , SizeOf (..)
  , read0, reinterpretV
  ) where

import Data.Bits (complement, testBit, xor, (.&.), (.|.))
import Data.ByteString.Internal (ByteString (PS), mallocByteString)
import Data.ByteString.Unsafe (unsafeIndex)
import Data.Coerce (coerce)
import Data.Type.Equality (type (~))
import Data.Vector.Storable.Mutable qualified as VM
import Foreign
  (FunPtr, IntPtr (..), Ptr, Storable (..), addForeignPtrFinalizerEnv, castPtr, copyBytes,
  fillBytes, intPtrToPtr, mallocForeignPtrBytes, plusPtr, pokeArray, withForeignPtr)
import GHC.Exts (proxy#)
import GHC.TypeLits (KnownNat, Nat, natVal')
import GHC.TypeNats (Div, type (+), type (-), type (<=))
import Language.C.Inline.Cpp qualified as C
import RIO hiding (drop)
import RIO.ByteString qualified as B
import System.IO.Unsafe (unsafeDupablePerformIO)

C.include "<stdint.h>"
C.include "<string.h>"

-- #define SAFER

type Buffer = VM.IOVector Word8

newtype AtLeast (n :: Nat) ba
  = AtLeast ba
  deriving newtype (Bytes, Eq, Mutable, NFData, Show)

orMore :: AtLeast n ba -> ba
orMore (AtLeast ba) = ba

atLeast :: forall n ba. (Bytes ba, KnownNat n) => ba -> Maybe (AtLeast n ba)
atLeast ba | byteLength ba >= len = Just (AtLeast ba)
           | otherwise            = Nothing
  where
    len = fromInteger $ natVal' (proxy# @n)
{-# INLINE atLeast #-}

isAtLeast :: (m <= n) => AtLeast n ba -> AtLeast m ba
isAtLeast = coerce

-- | Throws an exception if the length is insufficient.
unsafeAtLeast :: forall n ba. (Bytes ba, KnownNat n, HasCallStack) => ba -> AtLeast n ba
unsafeAtLeast = fromMaybe (error "unsafeAtLeast: failed length check") . atLeast
{-# INLINE unsafeAtLeast #-}

#ifdef SAFER
unsafeUncheckedAtLeast :: forall n ba. (Bytes ba, KnownNat n, HasCallStack) => ba -> AtLeast n ba
unsafeUncheckedAtLeast = unsafeAtLeast
#else
-- | Does no length check.
unsafeUncheckedAtLeast :: forall n ba. ba -> AtLeast n ba
unsafeUncheckedAtLeast = AtLeast
#endif

allocAtLeast ::
    forall n ba a.
    (Uninitialised ba, Bytes ba, KnownNat n)
 => (Ptr Word8 -> IO a)
 -> IO (a, AtLeast n ba)
allocAtLeast f = coerce <$> allocRet @ba (fromInteger (natVal' (proxy# @n))) f
{-# INLINE allocAtLeast #-}

cloneAtLeast ::
    forall bb ba n.
    (Uninitialised bb, Bytes bb, Bytes ba)
 => AtLeast n ba
 -> IO (AtLeast n bb)
cloneAtLeast (AtLeast ba) = AtLeast <$> clone ba

read_ :: Storable a => VM.IOVector a -> Int -> IO a
#ifdef SAFER
read_ = VM.read
#else
read_ = VM.unsafeRead
#endif

read :: forall i n a. (KnownNat i, i + 1 <= n, Storable a) => AtLeast n (VM.IOVector a) -> IO a
read (AtLeast buf) = read_ buf (fromInteger (natVal' (proxy# @i)))
{-# INLINE read #-}

write_ :: Storable a => VM.IOVector a -> Int -> a -> IO ()
#ifdef SAFER
write_ = VM.write
#else
write_ = VM.unsafeWrite
#endif

write :: forall i n a. (KnownNat i, i + 1 <= n, Storable a) => AtLeast n (VM.IOVector a) -> a -> IO ()
write (AtLeast buf) = write_ buf (fromInteger (natVal' (proxy# @i)))
{-# INLINE write #-}

-- | Copies @n@ bytes.
copy ::
    forall n m ba bb.
    (n <= m, Mutable ba, Bytes bb, KnownNat n)
 => AtLeast m ba
 -> AtLeast n bb
 -> IO ()
copy (AtLeast dst) (AtLeast src) =
    mutWithBytePtr dst $ \dstPtr ->
    unsafeWithBytePtr src $ \srcPtr ->
#ifdef SAFER
        flip (bool (error "copy dst too small")) (byteLength dst >= len) $
#endif
        copyBytes dstPtr srcPtr len
  where
    len = fromInteger (natVal' (proxy# @n))

slice ::
    forall i n m ba.
    (KnownNat i, KnownNat n, i + n <= m, Bytes ba)
 => AtLeast m ba
 -> AtLeast n ba
slice (AtLeast buf) = AtLeast $ unsafeSlice
    (fromInteger (natVal' (proxy# @i)))
    (fromInteger (natVal' (proxy# @n)))
    buf
{-# INLINE slice #-}

sliceBack ::
    forall i n m ba.
    (KnownNat i, KnownNat n, i <= m, n <= i, Bytes ba)
 => AtLeast m ba
 -> AtLeast n ba
sliceBack (AtLeast buf) = AtLeast $ unsafeSlice
    (byteLength buf - fromInteger (natVal' (proxy# @i)))
    (fromInteger (natVal' (proxy# @n)))
    buf
{-# INLINE sliceBack #-}

drop ::
    forall n m ba.
    (KnownNat n, n <= m, Bytes ba)
 => AtLeast m ba
 -> AtLeast (m - n) ba
drop (AtLeast buf) = AtLeast $ unsafeSlice n (byteLength buf - n) buf
  where
    n = fromInteger (natVal' (proxy# @n))
{-# INLINE drop #-}

dropBack ::
    forall n m ba.
    (KnownNat n, n <= m, Bytes ba)
 => AtLeast m ba
 -> AtLeast (m - n) ba
dropBack (AtLeast buf) = AtLeast $ unsafeSlice 0 (byteLength buf - n) buf
  where
    n = fromInteger (natVal' (proxy# @n))
{-# INLINE dropBack #-}

-- | A pinned byte buffer of fixed length but possibly mutable content.
class Bytes ba where
    byteLength :: ba -> Int
    unsafeWithBytePtr :: ba -> (Ptr Word8 -> IO a) -> IO a
    -- | Behaviour for OOB access is undefined.
    unsafeSlice :: Int -> Int -> ba -> ba

class (Bytes ba) => Mutable ba where
    mutWithBytePtr :: ba -> (Ptr Word8 -> IO a) -> IO a
    mutWithBytePtr = unsafeWithBytePtr
    {-# INLINE mutWithBytePtr #-}

class Uninitialised ba where
    uninitialised :: Int -> IO ba

allocRet :: (Uninitialised ba, Bytes ba) => Int -> (Ptr Word8 -> IO a) -> IO (a, ba)
allocRet l f = do
    ba <- uninitialised l
    a <- unsafeWithBytePtr ba f
    pure (a, ba)

clone :: forall bb ba. (Bytes ba, Bytes bb, Uninitialised bb) => ba -> IO bb
clone ba = fmap snd . allocRet len $ \dst ->
    unsafeWithBytePtr ba $ \src ->
        copyBytes dst src len
  where
    len = byteLength ba

pack :: (Uninitialised ba, Bytes ba) => [Word8] -> ba
pack l = unsafeDupablePerformIO $ uninitialised (length l) >>= \ba ->
    ba <$ unsafeWithBytePtr ba (`pokeArray` l)

fill :: (Mutable ba) => ba -> Word8 -> IO ()
fill ba i = mutWithBytePtr ba \dst -> fillBytes dst i (byteLength ba)

instance Bytes Buffer where
    byteLength = VM.length
    {-# INLINE byteLength #-}
    unsafeWithBytePtr = VM.unsafeWith
    {-# INLINE unsafeWithBytePtr #-}
#ifdef SAFER
    unsafeSlice = VM.slice
#else
    unsafeSlice = VM.unsafeSlice
#endif
    {-# INLINE unsafeSlice #-}

instance Mutable Buffer

instance Uninitialised Buffer where
    uninitialised = VM.unsafeNew
    {-# INLINE uninitialised #-}

instance Bytes ByteString where
    byteLength = B.length
    {-# INLINE byteLength #-}
    unsafeWithBytePtr = \(PS fp off _) f -> withForeignPtr fp $ f . (`plusPtr` off)
    {-# INLINE unsafeWithBytePtr #-}
    unsafeSlice = \off len -> B.take len . B.drop off
    {-# INLINE unsafeSlice #-}

instance Uninitialised ByteString where
    uninitialised i = (\fp -> PS fp 0 i) <$> mallocByteString i

constEqMem :: (Bytes a, Bytes b) => a -> b -> IO Bool
constEqMem a b
  | byteLength a /= byteLength b = pure False
  | otherwise = unsafeWithBytePtr a $ \aptr -> unsafeWithBytePtr b $ \bptr ->
        constEqPtr aptr bptr (byteLength a)
{-# INLINABLE constEqMem #-}

constEqPtr :: Ptr Word8 -> Ptr Word8 -> Int -> IO Bool
constEqPtr p q n = loop 0 0
  where
    loop i !acc
      | i == n    = pure $! iz acc
      | otherwise = do
            k <- xor <$> peekByteOff p i <*> peekByteOff q i
            loop (i + 1) (acc .|. k)
    iz :: Word8 -> Bool
    iz i = testBit (complement i .&. (i - 1)) 7

newtype Scrubbed
  = Scrubbed Buffer
  deriving newtype (Bytes, Mutable, NFData)

instance Show Scrubbed where
    show _ = "Scrubbed"

explicitBzero :: FunPtr (Ptr () -> Ptr Word8 -> IO ())
explicitBzero = [C.funPtr| void zero_out(void *lenptr, uint8_t *ptr) {
    explicit_bzero(ptr, (size_t)lenptr);
} |]

instance Uninitialised Scrubbed where
    uninitialised l = do
        fp <- mallocForeignPtrBytes l
        -- We embed the length as the environment "pointer"
        addForeignPtrFinalizerEnv explicitBzero (intPtrToPtr (IntPtr l)) fp
        pure $ Scrubbed $ VM.MVector l fp

class (Storable a) => SizeOf a where
    type Size a :: Nat

instance SizeOf Word64 where
    type Size Word64 = 8
instance SizeOf Word32 where
    type Size Word32 = 4
instance SizeOf Word16 where
    type Size Word16 = 2

-- | Reinterpret a byte buffer, without copying.
reinterpretV :: SizeOf a => AtLeast n Buffer -> AtLeast (Div n (Size a)) (VM.IOVector a)
reinterpretV (AtLeast ba) = AtLeast (VM.unsafeCast ba)
{-# INLINE reinterpretV #-}

-- Using ~ instead of <= means less explicit type nats
-- | Reinterpret a byte buffer, and read the first element.
read0 :: forall a ba n. (Bytes ba, Storable a, SizeOf a, Size a ~ n) => AtLeast n ba -> IO a
read0 (AtLeast ba) =
#ifdef SAFER
        flip (bool (error "read0 argument too small")) (byteLength ba >= sizeOf (undefined :: a)) $
#endif
        unsafeWithBytePtr ba (peek . castPtr)

indexByteString :: forall i n. (KnownNat i, i + 1 <= n) => AtLeast n ByteString -> Word8
indexByteString (AtLeast bs) =
#ifdef SAFER
    B.index bs (fromInteger (natVal' (proxy# @i)))
#else
    unsafeIndex bs (fromInteger (natVal' (proxy# @i)))
#endif
