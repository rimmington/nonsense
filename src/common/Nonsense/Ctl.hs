{-# language OverloadedStrings, StrictData #-}

module Nonsense.Ctl (
    Preflow (..), PreflowId (..), LastRecv (..), RemoteInfo (..), PfSecretLength
  , ctlIncoming, ctlOutgoing, ctlFlowTiming, ctlPreflowTiming, ctlFlowBecameGood
  , timeoutFlowDiscard, timeoutFlowExpires, timeoutSendKeepalive, timeoutWantKeepalive
  , timeoutPreflowExpires
  , nearMaxCount
  , mixHash, mixKey
  ) where

import Data.Primitive (Prim (..))
import Data.Primitive.PVar (PVar, RW, modifyPVar_, newPVar, readPVar, writePVar)
import Data.Vector.Storable.Mutable qualified as VM
import Focus qualified as F
import Foreign (ForeignPtr, newForeignPtr_, nullPtr)
import GHC.TypeNats (type (+))
import Iso.Deriving (As (..), Inject (..), Isomorphic, Project (..))
import Network.Socket (SockAddr)
import Nonsense.Address (AddrKey (..), keyAddr)
import Nonsense.AEAD (AeadKey (AeadKey), TagLength, expandInto, keyCipher)
import Nonsense.Bytes qualified as B
import Nonsense.Cipher (Cipher, Counter)
import Nonsense.ECDH
  (KeyLength, PrivKey, PubKeyBytes, SharedSecret (..), keyAgreement, loadPrivKey, privKeyBytesGen,
  privPubKey)
import Nonsense.Ip6Addr (Ip6Addr (..), Ip6Lo (..))
import Nonsense.K12 (Hash (..), HmacLength, hash, hash2Into, hkdf2Into)
import Nonsense.MonoTime (MonoTime32 (ms32), leq)
import Nonsense.Packet
  (FlowId (..), GotMsg (..), JunkBuffer, V6Buffer, asJunkBuffer, buildGnrl, buildHelo,
  buildKeepalive, buildLt, buildTher, copyIntoJunkBuffer, fromJunkBuffer, getBuf, gnrl2ndHmac,
  gnrlCheck2ndHmac, gnrlConsumeTsuk, gnrlOurId, gnrlTsukTagged, heloTheirEphPubkey, heloTheirId,
  junkBufferSize, ltTsukTagged, mkGotMsg, mkV6Buffer, newJunkBuffer, overwriteV6ToContent,
  readAsV6Len, therCheckHmac, therHmac, therOurId, therTheirEphPubkey, therTheirId, v6DestAddr)
import Nonsense.Prim ()
import Nonsense.Proc
  (Flow (..), Role (..), incrCount, maxSendCount, maybeReplaced, mkFlow, peerReady,
  reservedFirstCount, windowHigh, (<&&>))
import Nonsense.Random (GenST, uniformW32)
import Nonsense.RingBuffer qualified as RB
import RIO
import StmContainers.Map qualified as M
import System.IO.Unsafe (unsafePerformIO)

newtype CtlGotInvalidMessage
  = CtlGotInvalidMessage Text
  deriving stock (Show)
instance Exception CtlGotInvalidMessage

newtype PreflowId
  = PreflowId { getPreflowId :: FlowId }
  deriving stock (Show)
  deriving newtype (Eq, Hashable, Ord)

data LastRecv = LNone | LHelo
  deriving (Eq, Show)
  deriving (Prim)
    via (As Word8 LastRecv)

instance Project Word8 LastRecv where
    prj = \case
        LNone -> 0
        LHelo -> 1

instance Inject Word8 LastRecv where
    inj = \case
        0 -> LNone
        1 -> LHelo
        _ -> error "bug: inj @Word8 @LastRecv"

instance Isomorphic Word8 LastRecv

-- | Handshake state.
--
-- There may be a Flow with the same ID during the half-open pre-goodForSending phase.
data Preflow
  = Preflow
    { oerk     :: PrivKey
      -- ^ Our ephemeral private key
    , tAddr    :: Ip6Lo
      -- ^ Only correct/used when Initiator
    , tsukV    :: TVar (PubKeyBytes ByteString)
      -- ^ Their static public key; only set when Initiator
    , tId      :: PVar FlowId RW
    , buffered :: RB.RingBuffer
      -- ^ Buffered V6 packets, length-verified
    , began    :: MonoTime32
    , lastR    :: PVar LastRecv RW
    , intKey   :: AeadKey
      -- ^ Running handshake key
    , ck       :: SharedSecret
      -- ^ Chaining key
    , h        :: Hash B.Buffer
      -- ^ Running hash
    }

instance Show Preflow where
    show Preflow{tAddr, began} =
        "Preflow {tAddr = " <> show tAddr <>
        ", began = " <> show began <>
        "}"

data RemoteInfo
  = RemoteInfo
    { riSockAddr :: SockAddr
    , riStKey    :: PubKeyBytes ByteString
    }

initCk :: B.AtLeast KeyLength ByteString
initCk = hashBytes $ hash ("Noise_XK1_25519_CamelliaOFBHMAC_K12" :: ByteString)
{-# NOINLINE initCk #-}

-- Empty prologue
initH :: B.AtLeast KeyLength ByteString
initH = hashBytes $ hash initCk
{-# NOINLINE initH #-}

nullForeignPtr :: ForeignPtr Word8
nullForeignPtr = unsafePerformIO $ newForeignPtr_ nullPtr
{-# NOINLINE nullForeignPtr #-}

noTsuk :: TVar (PubKeyBytes ByteString)
noTsuk = unsafePerformIO $ newTVarIO (error "noTsuk")
{-# NOINLINE noTsuk #-}

newPreflowId :: GenST s -> ST s PreflowId
newPreflowId r = PreflowId . FlowId <$> uniformW32 r

-- | An established flow always expires this many ms after creation.
-- Packets will not be sent on a flow after this time.
timeoutFlowExpires :: Word32
timeoutFlowExpires = 120e3

-- | Incoming packets for a given flow are discarded after this time.
timeoutFlowDiscard :: Word32
timeoutFlowDiscard = timeoutFlowExpires + timeoutSendKeepalive

-- | How long to wait for a keepalive before considering the flow unresponsive.
timeoutWantKeepalive :: Word32
timeoutWantKeepalive = 10e3

-- | How long to wait for data to send before sending a keepalive.
timeoutSendKeepalive :: Word32
timeoutSendKeepalive = 5e3

-- | Maximum amount of time for establishment of a new flow.
timeoutPreflowExpires :: Word32
timeoutPreflowExpires = timeoutWantKeepalive

-- | Number of messages sent or received before refresh is attempted.
nearMaxCount :: Counter
nearMaxCount = maxSendCount - 1e8

flowNeedsKeepalive, flowMissedKeepalive, flowActive :: MonoTime32 -> Flow -> IO Bool
flowNeedsKeepalive now flow@Flow{created} = do
    recvYetAck <- readPVar $ recvYetAck flow
    sentAny <- readPVar $ sentAny flow
    pure $ not (leq created (ms32 recvYetAck) (ms32 sentAny)) &&
           leq created (ms32 recvYetAck + timeoutSendKeepalive) (ms32 now)
flowMissedKeepalive now flow@Flow{created} = do
    recvAny <- readPVar $ recvAny flow
    sentYetAck <- readPVar $ sentYetAck flow
    pure $ not (leq created (ms32 sentYetAck) (ms32 recvAny)) &&
           leq created (ms32 sentYetAck + timeoutWantKeepalive) (ms32 now)
flowActive now flow@Flow{created} = do
    sentAny <- readPVar $ sentAny flow
    recvAny <- readPVar $ recvAny flow
    pure $ not (leq created (ms32 sentAny) (ms32 now - timeoutWantKeepalive)) ||
           not (leq created (ms32 recvAny) (ms32 now - timeoutWantKeepalive))

flowNearExpiry :: MonoTime32 -> Flow -> Bool
flowNearExpiry now Flow{..} =
    leq created (ms32 created + timeoutFlowExpires - timeoutWantKeepalive) (ms32 now)

expireFlow :: M.Map Ip6Lo Flow -> Flow -> STM ()
expireFlow loFlow Flow{theirAddr, created = a} =
    M.focus
        (F.unitCases F.Leave $ \Flow{created = b} -> bool F.Leave F.Remove $ a == b)
        theirAddr
        loFlow

discardFlow :: M.Map Ip6Lo Flow -> M.Map FlowId Flow -> FlowId -> STM ()
discardFlow loFlow idFlow fid = do
    mexisting <- M.focus
        F.lookupAndDelete
        fid
        idFlow
    -- Also expire, because this could be for a flow that never got good
    for_ mexisting $ expireFlow loFlow

-- | Returns True if the preflow was found in loPre (so we were Initiator), False otherwise.
discardPreflow :: M.Map Ip6Lo Preflow -> M.Map PreflowId Preflow -> PreflowId -> STM Bool
discardPreflow loPre idPre pid = M.focus F.lookupAndDelete pid idPre >>= \case
    Nothing -> pure False
    Just Preflow{tAddr, began = a} -> M.focus
        (F.cases (False, F.Leave) $ \Preflow{began = b} -> bool (False, F.Leave) (True, F.Remove) $ a == b)
        tAddr
        loPre

ctlFlowTiming ::
    JunkBuffer
 -> (forall a. (forall s. GenST s -> ST s a) -> IO a)
 -> M.Map Ip6Lo Preflow
 -> M.Map PreflowId Preflow
 -> M.Map Ip6Lo Flow
 -> M.Map FlowId Flow
 -> (Ip6Lo -> STM (Maybe RemoteInfo))  -- ^ lookup remote cache
 -> (Ip6Lo -> IO (Maybe RemoteInfo))   -- ^ lookup remote
 -> (SockAddr -> B.Buffer -> IO ())    -- ^ send
 -> (IO () -> IO ())                   -- ^ enqueue action
 -> MonoTime32
 -> FlowId
 -> Flow
 -> IO ()
ctlFlowTiming junk rand loPre idPre loFlow idFlow cache remote send later now fid flow =
    checkAfter timeoutFlowDiscard (atomically $ discardFlow loFlow idFlow fid) <||>
    checkAfter timeoutFlowExpires (atomically $ expireFlow loFlow flow) <||>
    do
        checkUnresponsive
        when (role flow == Initiator) $
            checkNearExpiry *> checkCounterOut *> checkCounterIn
        -- Have to checkKeepalive last because it writes to sentAny
        checkKeepalive
  where
    checkAfter t a
      | leq cr (ms32 cr + fromIntegral t) (ms32 now) = True <$ a
      | otherwise                                    = pure False
      where cr = created flow
    checkKeepalive = mwhen (flowNeedsKeepalive now flow) do
        mcount <- incrCount (sentCount flow)
        -- If count is unavailable, we can't send a KEEP, but incr sentAny anyway
        for_ mcount \count -> do
            msg <- buildKeepalive junk (theirId flow) (sendK flow) count
            (`send` getBuf msg) =<< readTVarIO (remoteAddr flow)
        -- This could race with sending a CONT packet, but the worst result
        -- is that a KEEP is sent a bit too early.
        writePVar (sentAny flow) now
    checkUnresponsive = mwhen (flowMissedKeepalive now flow) pf
    checkNearExpiry = mwhen
        (pure (role flow == Initiator && flowNearExpiry now flow) <&&> flowActive now flow)
        pf
    checkCounterOut = mwhen
        (pure (role flow == Initiator)
            <&&> ((>= fromIntegral nearMaxCount) <$> windowHigh (recvWindow flow)))
        pf
    checkCounterIn = mwhen
        (pure (role flow == Initiator)
            <&&> ((>= fromIntegral nearMaxCount) <$> readPVar (sentCount flow)))
        pf
    pf = munless isReplaced $ void $
        preflowing junk rand loPre idPre idFlow cache remote send later now (theirAddr flow)
    mwhen p a = p >>= (`when` a)
    munless p a = p >>= (`unless` a)
    p <||> a = munless p a
    infixr <||>
    isReplaced = isJust <$> maybeReplaced flow

ctlPreflowTiming ::
    M.Map Ip6Lo Preflow
 -> M.Map PreflowId Preflow
 -> M.Map Ip6Lo Flow
 -> M.Map FlowId Flow
 -> (Ip6Lo -> STM ())  -- ^ clear cache
 -> MonoTime32
 -> PreflowId
 -> Preflow
 -> IO ()
ctlPreflowTiming loPre idPre loFlow idFlow uncache now pid Preflow{began, tAddr} =
    when (leq began (ms32 began + timeoutPreflowExpires) (ms32 now)) $ atomically do
        isInit <- discardPreflow loPre idPre pid
        discardFlow loFlow idFlow (getPreflowId pid)
        when isInit $ uncache tAddr

ctlFlowBecameGood ::
    Cipher  -- ^ any cipher; will be keyed as required
 -> (SockAddr -> B.Buffer -> IO ())  -- ^ send
 -> M.Map Ip6Lo Preflow
 -> M.Map PreflowId Preflow
 -> FlowId
 -> Flow
 -> Preflow
 -> IO ()
ctlFlowBecameGood c send loPre idPre fid flow Preflow{buffered} = do
    bool (keyCipher c sendK *> go) (pure ()) =<< RB.null buffered
    void . atomically $ discardPreflow loPre idPre (PreflowId fid)
  where
    Flow{remoteAddr, sendK, sentCount, theirId} = flow
    go = rbV6 buffered >>= \case
        Nothing -> pure ()
        -- In the unlikely event we immediately run out of counters, give up
        Just (junk, v6) -> forJ_ (incrCount sentCount) \count -> do
            content <- overwriteV6ToContent junk v6 theirId sendK c count
            remote <- readTVarIO remoteAddr
            send remote (getBuf content)
            RB.unsafeCommitRead buffered
            go

ctlOutgoing ::
    JunkBuffer
 -> Cipher -- ^ any cipher; will be keyed as required
 -> (forall a. (forall s. GenST s -> ST s a) -> IO a)
 -> M.Map Ip6Lo Preflow
 -> M.Map PreflowId Preflow
 -> M.Map Ip6Lo Flow
 -> M.Map FlowId Flow
 -> (Ip6Lo -> STM (Maybe RemoteInfo))  -- ^ lookup remote cache
 -> (Ip6Lo -> IO (Maybe RemoteInfo))   -- ^ lookup remote
 -> (SockAddr -> B.Buffer -> IO ())    -- ^ send
 -> (IO () -> IO ())                   -- ^ enqueue action
 -> MonoTime32
 -> JunkBuffer
 -> B.Buffer
 -> IO ()
ctlOutgoing heloJunk c rand loPre idPre loFlow idFlow cache remote send later now junk buf =
    mkV6Buffer buf >>= \case
        Nothing -> throwIO $ CtlGotInvalidMessage "not v6"
        Just v6 -> do
            Ip6Addr _ (Ip6Lo -> theirAddr) <- v6DestAddr v6
            atomically (M.lookup theirAddr loFlow) >>= \case
                Just fl@Flow{theirId, remoteAddr, sendK, sentCount} -> peerReady fl >>= \case
                    True -> incrCount sentCount >>= \case
                        Just count -> do
                            -- Cannot use ssCipher because Proc might be using it
                            keyCipher c sendK
                            content <- overwriteV6ToContent junk v6 theirId sendK c count
                            lastRemote <- readTVarIO remoteAddr
                            send lastRemote (getBuf content)
                        Nothing -> viaPf theirAddr v6
                    False -> atomically (M.lookup theirAddr loPre) >>= \case
                        Just Preflow{buffered} -> addToBuffered v6 buffered
                        Nothing                -> error "no preflow for ungood flow"
                Nothing -> viaPf theirAddr v6
  where
    viaPf theirAddr v6 = addToBuffered v6 . buffered =<<
        preflowing heloJunk rand loPre idPre idFlow cache remote send later now theirAddr
    addToBuffered v6 buffered = do
        -- There's no race here
        rbBuf <- RB.writeVector buffered
        let (fromJunkBuffer -> (dst, _)) =
                asJunkBuffer (B.unsafeUncheckedAtLeast rbBuf)
        _ <- copyIntoJunkBuffer dst v6
        void $ RB.unsafeCommitWrite buffered

ctlIncoming ::
    AddrKey
 -> Cipher -- ^ any cipher; will be keyed as required
 -> (forall a. (forall s. GenST s -> ST s a) -> IO a)
 -> M.Map Ip6Lo Preflow
 -> M.Map PreflowId Preflow
 -> M.Map Ip6Lo Flow
 -> M.Map FlowId Flow
 -> (SockAddr -> B.Buffer -> IO ())      -- ^ send
 -> MonoTime32
 -> SockAddr
 -> JunkBuffer
 -> B.AtLeast PfSecretLength B.Scrubbed  -- ^ scratch buffer
 -> B.Buffer
 -> IO ()
ctlIncoming osk c rand loPre idPre loFlow idFlow send now remote junk scr buf = mkGotMsg buf >>= \case
    Nothing          -> throwIO $ CtlGotInvalidMessage "no parse"
    Just (Helo helo) -> do
        theirId <- heloTheirId helo
        let teuk = heloTheirEphPubkey helo
        (ourId, oerk) <- checkedVars rand idPre idFlow
        for_ (keyAgreement oerk teuk) \ee -> for_ (keyAgreement osrk teuk) \es -> do
            -- tsuk is unused on Responder path, so set to noTsuk; deref will crash
            pf <- newPf loPre idPre ourId oerk now Nothing noTsuk (Just theirId)
            mixHash pf osuk
            mixHash pf teuk
            mixHash pf (VM.MVector @RW 0 nullForeignPtr)
            let oeuk = privPubKey oerk
            mixHash pf oeuk
            mixKey pf ee
            mixKey pf es
            ther <- buildTher junk (getPreflowId ourId) oeuk theirId (intKey pf) (h pf)
            mixHash pf (therHmac ther)
            send remote (getBuf ther)
    Just (Ther ther) -> therOurId ther >>= \ourId -> forPf_ ourId LNone
        -- TODO: check remote is as expected
        \pf@Preflow{..} -> do
            let teuk = therTheirEphPubkey ther
            tsuk <- readTVarIO tsukV
            for_ (keyAgreement oerk teuk) \ee -> for_ (keyAgreement oerk tsuk) \es -> do
                backupSecretState pf scr
                mixHash pf teuk
                mixKey pf ee
                mixKey pf es
                therCheckHmac intKey h ther
                    (restoreSecretState pf scr)
                    -- Somehow oerk×teuk worked but osrk×teuk doesn't
                    (flip (maybe (throwPf ourId)) (keyAgreement osrk teuk) \se -> do
                        theirId <- therTheirId ther
                        writePVar tId theirId
                        mixHash pf (therHmac ther)
                        keyCipher c intKey
                        lt <- buildLt junk osuk theirId intKey c h
                        mixHash pf (ltTsukTagged lt)
                        mixKey pf se
                        gnrl <- buildGnrl intKey h lt
                        mixHash pf (gnrl2ndHmac gnrl)
                        -- Preflow is removed when flow is good or after preflow timeout
                        _ <- prodNewFlow ourId theirId tAddr ck Initiator 0
                        send remote (getBuf gnrl)
                    )
                B.fill scr 0
    Just (Gnrl gnrl) -> gnrlOurId gnrl >>= \ourId -> forPf_ ourId LHelo
        \pf@Preflow{tAddr = _, ..} -> do
            -- Need a copy of the encrypted tsuk for mixHash
            B.copy scr (gnrlTsukTagged gnrl)
            keyCipher c intKey
            forJ_ (gnrlConsumeTsuk intKey c h gnrl) \tsuk ->
                -- Screwup under ephemeral encryption
                flip (maybe (throwPf ourId)) (keyAddr tsuk) \tAddr ->
                flip (maybe (throwPf ourId)) (keyAgreement oerk tsuk) \se -> do
                    mixHash pf (B.slice @0 @(KeyLength + TagLength) scr)
                    mixKey pf se
                    -- If the 2nd HMAC fails, the initiator has screwed up
                    -- No choice but to abandon handshake
                    gnrlCheck2ndHmac intKey h gnrl (throwPf ourId) do
                        mixHash pf (gnrl2ndHmac gnrl)
                        t'Id <- readPVar tId
                        -- Bump sentCount before register to avoid competing with Proc
                        newFlow <- prodNewFlow ourId t'Id tAddr ck Responder =<< RB.toRead buffered
                        throwPf ourId
                        RB.null buffered >>= \case
                            False -> sendBuffered t'Id (sendK newFlow) buffered
                            True  -> do
                                msg <- buildKeepalive junk t'Id (sendK newFlow) reservedFirstCount
                                send remote (getBuf msg)
    Just (Content _)   -> throwIO $ CtlGotInvalidMessage "content"
    Just (Keepalive _) -> throwIO $ CtlGotInvalidMessage "keepalive"
  where
    AddrKey osrk osuk _ = osk
    forPf_ ourId expectLr f = forJ_ (atomically (M.lookup (PreflowId ourId) idPre)) \pf -> do
        lr <- readPVar $ lastR pf :: IO LastRecv
        when (lr == expectLr) $ f pf
    throwPf = void . atomically . discardPreflow loPre idPre . PreflowId
    prodNewFlow ourId@(FlowId (fromIntegral -> ourIdInt)) theirId theirAddr ck role reserve = do
        remoteAddr <- newTVarIO remote
        newFlow <- mkFlow remoteAddr theirId theirAddr ck now role
        modifyPVar_ (sentCount newFlow) (+ reserve)
        replacing <- atomically do
            M.insert newFlow ourId idFlow
            M.focus
                (F.cases (Nothing, F.Set newFlow) \e -> (Just e, F.Set newFlow))
                theirAddr
                loFlow
        for_ replacing $ flip writePVar ourIdInt . replacedBy
        pure newFlow
    -- | Send buffered packets using preserved counts
    sendBuffered theirId k buffered = keyCipher c k *> go reservedFirstCount
      where
        go i = rbV6 buffered >>= \case
            Nothing -> pure ()
            Just (src, v6) -> do
                content <- overwriteV6ToContent src v6 theirId k c i
                send remote (getBuf content)
                RB.unsafeCommitRead buffered
                go (i + 1)

rbV6 :: RB.RingBuffer -> IO (Maybe (JunkBuffer, V6Buffer))
rbV6 rb = RB.readVector rb >>= \case
    Nothing -> pure Nothing
    Just rbBuf -> do
        let (src, shifted) = fromJunkBuffer $ asJunkBuffer (B.unsafeUncheckedAtLeast rbBuf)
        len <- readAsV6Len (B.isAtLeast shifted)
        mv6 <- mkV6Buffer $ VM.take (fromIntegral len) (B.orMore shifted)
        v6 <- maybe (throwIO $ CtlGotInvalidMessage "buffered not v6") pure mv6
        pure $ Just (src, v6)

checkedVars ::
    (forall a. (forall s. GenST s -> ST s a) -> IO a)
 -> M.Map PreflowId Preflow
 -> M.Map FlowId Flow
 -> IO (PreflowId, PrivKey)
checkedVars rand preflow flow = go
  where
    go = do
        vars@(ourId, _) <- newVars
        atomically (M.lookup ourId preflow) >>= \case
            Nothing -> atomically (M.lookup (getPreflowId ourId) flow) >>= \case
                Nothing -> pure vars
                _       -> go
            _ -> go
    newVars = rand $ \gen -> do
        ourId <- newPreflowId gen
        privKey <- loadPrivKey <$> privKeyBytesGen (uniformW32 gen)
        pure (ourId, privKey)

preflowing ::
    JunkBuffer
 -> (forall a. (forall s. GenST s -> ST s a) -> IO a)
 -> M.Map Ip6Lo Preflow
 -> M.Map PreflowId Preflow
 -> M.Map FlowId Flow
 -> (Ip6Lo -> STM (Maybe RemoteInfo))  -- ^ lookup remote cache
 -> (Ip6Lo -> IO (Maybe RemoteInfo))   -- ^ lookup remote
 -> (SockAddr -> B.Buffer -> IO ())    -- ^ send
 -> (IO () -> IO ())                   -- ^ enqueue action
 -> MonoTime32
 -> Ip6Lo                              -- ^ their addr
 -> IO Preflow
preflowing heloJunk rand loPre idPre idFlow cache remote send later now theirAddr =
    maybe new pure =<< atomically (M.lookup theirAddr loPre)
  where
    new = atomically (cache theirAddr) >>= \case
        Just ri -> do
            (ourId, oerk) <- checkedVars rand idPre idFlow
            tsukV <- newTVarIO $ riStKey ri
            pf <- newPf loPre idPre ourId oerk now (Just theirAddr) tsukV Nothing
            heloFromNewPf heloJunk send ourId pf ri
            pure pf
        Nothing -> do
            (ourId, oerk) <- checkedVars rand idPre idFlow
            tsukV <- newTVarIO $ error "tsuk yet to be identified"
            pf <- newPf loPre idPre ourId oerk now (Just theirAddr) tsukV Nothing
            later $ remote theirAddr >>= \case
                Just ri -> do
                    atomically $ writeTVar tsukV (riStKey ri)
                    -- Cannot use heloJunk
                    junk <- newJunkBuffer
                    heloFromNewPf junk send ourId pf ri
                Nothing -> void . atomically $ discardPreflow loPre idPre ourId
            pure pf

heloFromNewPf ::
    JunkBuffer
 -> (SockAddr -> B.Buffer -> IO ())  -- ^ send
 -> PreflowId
 -> Preflow
 -> RemoteInfo
 -> IO ()
heloFromNewPf heloJunk send ourId pf RemoteInfo{riSockAddr, riStKey = tsuk} = do
    mixHash pf tsuk
    let oeuk = privPubKey $ oerk pf
    helo <- buildHelo heloJunk (getPreflowId ourId) oeuk
    mixHash pf oeuk
    mixHash pf (VM.MVector @RW 0 nullForeignPtr)
    send riSockAddr (getBuf helo)

newPf ::
    M.Map Ip6Lo Preflow
 -> M.Map PreflowId Preflow
 -> PreflowId
 -> PrivKey
 -> MonoTime32
 -> Maybe Ip6Lo                    -- ^ their addr
 -> TVar (PubKeyBytes ByteString)  -- ^ their static pubkey
 -> Maybe FlowId                   -- ^ their id
 -> IO Preflow
newPf loPre idPre ourId oerk began addrm tsukV t'Id = do
    buffered <- RB.newAlignedPinnedRingBuffer junkBufferSize 10
    lastR <- newPVar $ maybe LNone (const LHelo) t'Id
    tId <- newPVar $ fromMaybe (FlowId 0) t'Id
    intKey <- AeadKey . snd <$> B.allocAtLeast (const $ pure ())
    ck <- SharedSecret <$> B.cloneAtLeast initCk
    h <- Hash <$> B.cloneAtLeast initH
    let tAddr = fromMaybe (Ip6Lo 0) addrm
    let pf = Preflow{..}
    atomically do
        M.insert pf ourId idPre
        for_ addrm \addr -> M.insert pf addr loPre
    pure pf

mixKey :: Preflow -> SharedSecret -> IO ()
mixKey Preflow{intKey = AeadKey k, ck = ck@(SharedSecret ckb)} ikm = do
    -- New message key into first 32 bytes of k
    hkdf2Into ck ikm ckb (B.isAtLeast k)
    expandInto (SharedSecret (B.slice @0 k)) (AeadKey k)
{-# INLINE mixKey #-}

mixHash :: (B.Bytes ba) => Preflow -> ba -> IO ()
mixHash Preflow{h} ba = hash2Into h ba h
{-# INLINE mixHash #-}

type PfSecretLength = KeyLength + KeyLength + KeyLength + HmacLength

backupSecretState, restoreSecretState :: Preflow -> B.AtLeast PfSecretLength B.Scrubbed -> IO ()
backupSecretState Preflow{intKey = AeadKey k, ck = SharedSecret ck, h = Hash h} b = do
    B.copy (B.slice @0 @(KeyLength + KeyLength) b) k
    B.copy (B.slice @(KeyLength + KeyLength) @KeyLength b) ck
    B.copy (B.slice @(KeyLength + KeyLength + KeyLength) @HmacLength b) h
restoreSecretState Preflow{intKey = AeadKey k, ck = SharedSecret ck, h = Hash h} b = do
    B.copy k (B.slice @0 @(KeyLength + KeyLength) b)
    B.copy ck (B.slice @(KeyLength + KeyLength) @KeyLength b)
    B.copy h (B.slice @(KeyLength + KeyLength + KeyLength) @HmacLength b)

forJ_ :: (Monad m, Foldable t) => m (t a) -> (a -> m b) -> m ()
forJ_ mt f = mt >>= \t -> for_ t f
