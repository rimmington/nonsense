{-# options_ghc -Wno-orphans #-}

module Nonsense.Prim () where

import Data.Coerce (coerce)
import Data.Primitive (Prim (..))
import Iso.Deriving (As (..), Isomorphic, inj, prj)
import RIO

instance (Isomorphic a b, Prim a) => Prim (As a b) where
    sizeOf# x = sizeOf# (prj @a @b (coerce x))
    alignment# x = alignment# (prj @a @b (coerce x))
    indexByteArray# m i = As . inj @a @b $ indexByteArray# m i
    readByteArray# m i s = case readByteArray# m i s of
        (# s', a #) -> (# s', As (inj @a @b a) #)
    writeByteArray# m i (As b) = writeByteArray# m i (prj @a @b b)
    setByteArray# m i l (As b) = setByteArray# m i l (prj @a @b b)
    indexOffAddr# a i = As . inj @a @b $ indexOffAddr# a i
    readOffAddr# m i s = case readOffAddr# m i s of
        (# s', a #) -> (# s', As (inj @a @b a) #)
    writeOffAddr# a i (As b) = writeOffAddr# a i (prj @a @b b)
    setOffAddr# a i l (As b) = setOffAddr# a i l (prj @a @b b)
