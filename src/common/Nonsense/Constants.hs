module Nonsense.Constants where

import RIO

deviceName :: String
deviceName = "nonsense0"

mtu :: Word16
mtu = 1500

subnet :: Word64
subnet = byteSwap64 0xfdf9_70c0_0000_0000
