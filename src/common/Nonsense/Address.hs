{-# language OverloadedStrings #-}

module Nonsense.Address (AddrKey (..), keyAddr, newKey, readAddrKey) where

import Data.Bits ((.&.))
import Nonsense.Bytes
  (Bytes, allocRet, atLeast, indexByteString, read0, slice, sliceBack, unsafeAtLeast)
import Nonsense.ECDH
  (PrivKey, PrivKeyBytes (..), PubKeyBytes, keyLength, loadPrivKey, privKeyBytesGen, privPubKey)
import Nonsense.Ip6Addr (Ip6Lo)
import Nonsense.K12 (Hash (..), hash2)
import Nonsense.Random (Seed, seedGen, thawGen, uniformW32)
import RIO
import RIO.ByteString (pack)
import System.IO (hGetBuf)
import System.IO.Unsafe (unsafePerformIO)
import System.Random.PCG (advance)

keyAddr :: (Bytes ba) => PubKeyBytes ba -> Maybe Ip6Lo
keyAddr pk
  -- Require that the first 20 bits of the hash are zero
  -- Similar to the extension technique in RFC 3972 §7.2
  -- Address security becomes 64 + 20 = 84 bits
  | slice @0 @2 h == unsafeAtLeast (pack [0, 0])
  , indexByteString @2 h .&. 0xf0 == 0
  = Just $! unsafePerformIO $ read0 (sliceBack @8 h)
  | otherwise
  = Nothing
  where
    h = hashBytes $ hash2 ("nonsense_address_K12" :: ByteString) pk

newKey :: Seed -> Int -> IO (Int, PrivKeyBytes, AddrKey)
newKey seed threadCount = runConc . asum $ [0..tc - 1] <&> \i ->
    conc . evaluate . force $ runST do
        gen <- thawGen (seedGen seed)
        advance i gen
        go gen
  where
    tc = fromIntegral threadCount
    go gen = loop 1
      where
        loop !i = do
            rkb <- privKeyBytesGen (uniformW32 gen)
            case privAddrKey rkb of
                Just ak -> pure (i, rkb, ak)
                Nothing -> advance tc gen *> loop (i + 1)

data AddrKey
  = AddrKey
    { akPriv :: PrivKey
    , akPub  :: PubKeyBytes ByteString
    , akLo   :: Ip6Lo
    }
  deriving (Generic, NFData, Show)

readAddrKey :: FilePath -> IO (Maybe AddrKey)
readAddrKey keyfile = do
    (count, scrub) <- withBinaryFile keyfile ReadMode \h -> allocRet keyLength \ptr ->
        hGetBuf h ptr keyLength
    case guard (count == keyLength) *> atLeast scrub of
        Nothing -> pure Nothing
        Just al -> pure $ privAddrKey (PrivKeyBytes al)

privAddrKey :: PrivKeyBytes -> Maybe AddrKey
privAddrKey rkb = case keyAddr uk of
    Just lo -> Just $ AddrKey rk uk lo
    Nothing -> Nothing
  where
    uk = privPubKey rk
    rk = loadPrivKey rkb
