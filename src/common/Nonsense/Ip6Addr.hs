{-# language OverloadedStrings, StrictData, TemplateHaskell, TypeFamilies, UndecidableInstances #-}
{- HLINT ignore "Unused LANGUAGE pragma" -}

module Nonsense.Ip6Addr
  ( -- * Ip6Addr
    Ip6Addr (..), ip6AddrToTuple, tupleToIp6Addr, ip6InSubnet, prettyIp6Addr
    -- * Ip6Lo
  , Ip6Lo (..), addrIp6Lo, ip6LoAddr
    -- * C context
  , ip6Ctx
  ) where

import Data.Bits (shiftL, shiftR, (.|.))
import Data.IP (fromHostAddress6)
import Data.Primitive (Prim)
import Foreign.Storable (Storable (..))
import Language.C.Inline.Context (Context (..))
import Language.C.Types (TypeSpecifier (TypeName))
import Network.Socket (tupleToHostAddress6)
import Nonsense.Bytes (SizeOf (..))
import Nonsense.Constants qualified as Const
import RIO
import RIO.Map qualified as Map

-- | NIH IPv6 address type. Network byte order.
data Ip6Addr
  = Ip6Addr Word64 Word64
  deriving (Eq, Generic, NFData, Ord, Show)

-- | Lower 64 bits of an IPv6 address, network byte order.
newtype Ip6Lo
  = Ip6Lo { ip6Lo :: Word64 }
  deriving stock (Read, Show)
  deriving newtype (Eq, Hashable, NFData, Ord, Prim, SizeOf, Storable)

ip6LoAddr :: Ip6Lo -> Ip6Addr
ip6LoAddr (Ip6Lo lo) = Ip6Addr Const.subnet lo

addrIp6Lo :: Ip6Addr -> Ip6Lo
addrIp6Lo (Ip6Addr _ lo) = Ip6Lo lo

ip6InSubnet :: Ip6Addr -> Bool
ip6InSubnet (Ip6Addr hi _) = hi == Const.subnet

-- TODO: Display instance

-- TODO: use byteOrder + TH
ip6AddrToTuple :: Ip6Addr -> (Word16, Word16, Word16, Word16, Word16, Word16, Word16, Word16)
ip6AddrToTuple (Ip6Addr noh nol) = (hh h, hl h, lh h, ll h, hh l, hl l, lh l, ll l)
  where
    h = byteSwap64 noh
    l = byteSwap64 nol
    hh = fromIntegral . (`shiftR` 48)
    hl = fromIntegral . (`shiftR` 32)
    lh = fromIntegral . (`shiftR` 16)
    ll = fromIntegral

tupleToIp6Addr :: (Word16, Word16, Word16, Word16, Word16, Word16, Word16, Word16) -> Ip6Addr
tupleToIp6Addr (w7, w6, w5, w4, w3, w2, w1, w0) = Ip6Addr noh nol
  where
    noh = byteSwap64 h
    nol = byteSwap64 l
    h = add w7 w6 w5 w4
    l = add w3 w2 w1 w0
    add hh hl lh ll =
        (fromIntegral hh `shiftL` 48) .|.
        (fromIntegral hl `shiftL` 32) .|.
        (fromIntegral lh `shiftL` 16) .|.
        fromIntegral ll

prettyIp6Addr :: Ip6Addr -> String
prettyIp6Addr = show . fromHostAddress6 . tupleToHostAddress6 . ip6AddrToTuple

-- | As @in6_addr@.
instance Storable Ip6Addr where
  sizeOf    _ = 16
  alignment _ = 8
  peek p = Ip6Addr <$> peekByteOff p 0 <*> peekByteOff p 8
  poke p (Ip6Addr h l) = pokeByteOff p 0 h *> pokeByteOff p 8 l

instance SizeOf Ip6Addr where
    type Size Ip6Addr = 16

ip6Ctx :: Context
ip6Ctx = mempty
    { ctxTypesTable = Map.singleton (TypeName "in6_addr") [t|Ip6Addr|] }
