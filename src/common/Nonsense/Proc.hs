{-# language MultiWayIf, StrictData #-}

module Nonsense.Proc (
    Flow (..), Role (..)
  , mkFlow, eqFlow, describeFlow, maybeReplaced, notReplaced
  , incrCount, reservedFirstCount, maxSendCount
  , peerReady
  , splitKey
  , Window (..)
  , newWindow, windowMember, windowSet, windowLength, windowHigh, describeWindow
  , procIncoming, procOutgoing, (<&&>)
  ) where

import Data.Bits (bit, countLeadingZeros, testBit, (.|.))
import Data.Primitive (MutableByteArray, fillByteArray, newByteArray, readByteArray, writeByteArray)
import Data.Primitive.PVar (PVar, RW, atomicAddIntPVar, newPVar, readPVar, writePVar)
import Network.Socket (SockAddr)
import Nonsense.AEAD (AeadKey (..), expandInto, keyCipher)
import Nonsense.Bytes (Buffer, allocAtLeast, constEqMem, slice)
import Nonsense.Cipher (Cipher, Counter (..), initCipher)
import Nonsense.Constants qualified as Const
import Nonsense.ECDH (SharedSecret (..))
import Nonsense.Ip6Addr (Ip6Addr (..), Ip6Lo (..), ip6LoAddr)
import Nonsense.K12 (hkdf2Into)
import Nonsense.MonoTime (MonoTime32 (..), leq)
import Nonsense.Packet
  (ContentMsg, FlowId (..), GotMsg (..), JunkBuffer, V6Buffer, contentCounter, contentOurId,
  keepaliveCheckHmac, keepaliveCounter, keepaliveOurId, mkGotMsg, mkV6Buffer, overwriteContentToV6,
  overwriteV6ToContent, v6DestAddr, v6SourceAddr)
import RIO hiding (Map, lookup, set)
import System.IO.Unsafe (unsafeDupablePerformIO)

data Role = Initiator | Responder deriving (Eq, Show)

data Flow
  = Flow
    { remoteAddr :: TVar SockAddr
    , theirId    :: FlowId
    , theirAddr  :: Ip6Lo
    , sendK      :: AeadKey
    , recvK      :: AeadKey
    , sendCipher :: Cipher -- not thread safe
    , recvCipher :: Cipher -- not thread safe
    , created    :: MonoTime32
    , sentAny    :: PVar MonoTime32 RW
    , sentYetAck :: PVar MonoTime32 RW -- update on send data if <= recvAny
    , recvAny    :: PVar MonoTime32 RW
    , recvYetAck :: PVar MonoTime32 RW -- update on recv data if <= sentAny
    , sentCount  :: PVar Int RW
    , recvWindow :: {-# UNPACK #-}Window
    , role       :: Role
    , replacedBy :: PVar Int RW
    }

notReplaced :: Int
notReplaced = fromIntegral (maxBound :: Word32) + 1

maybeReplaced :: Flow -> IO (Maybe FlowId)
maybeReplaced Flow{replacedBy} = readPVar replacedBy <&> \case
    i | i == notReplaced -> Nothing
      | otherwise        -> Just . FlowId $ fromIntegral i
{-# INLINE maybeReplaced #-}

-- | Check if peer is ready to accept content packets.
--
-- If not for this check, re-ordering could lead to a responder dropping CONT sent once
-- a flow is exposed from Ctl to Proc. Responder must send a KEEP if their queue is empty.
peerReady :: Flow -> IO Bool
peerReady Flow{created, role, recvAny} = case role of
    Responder -> pure True -- GNRL implies peer is ready
    Initiator -> readPVar recvAny <&> \rtime ->
        not $ leq created (ms32 rtime) (ms32 created)
{-# INLINE peerReady #-}

instance Show Flow where
    show Flow{theirAddr, created, role, theirId} =
        "Flow {theirAddr = " <> show theirAddr <>
        ", created = " <> show created <>
        ", role = " <> show role <>
        ", theirId = " <> show theirId <>
        "}"

-- NB: must be atomic because procOutgoing, ctlOutgoing & ctlFlowTiming write
incrCount :: PVar Int RW -> IO (Maybe Counter)
incrCount v = do
    c <- readPVar v
    case c < fromIntegral maxSendCount of
        True  -> Just . fromIntegral . (+1) <$> atomicAddIntPVar v 1
        False -> pure Nothing
{-# INLINE incrCount #-}

-- | Avoid sending messages with counter > this value.
-- Some wiggle room is left for keepalives and synchronisation.
maxSendCount :: Counter
maxSendCount = Counter $ maxBound - 256

reservedFirstCount :: Counter
reservedFirstCount = 0

mkFlow ::
    TVar SockAddr
 -> FlowId
 -> Ip6Lo
 -> SharedSecret
 -> MonoTime32
 -> Role
 -> IO Flow
mkFlow remoteAddr theirId theirAddr ck now role = do
    sentAny <- newPVar now
    sentYetAck <- newPVar now
    recvAny <- newPVar now
    recvYetAck <- newPVar now
    replacedBy <- newPVar notReplaced
    sentCount <- newPVar $ fromIntegral reservedFirstCount
    (sendK, recvK) <- splitKey ck role
    sendCipher <- initCipher
    keyCipher sendCipher sendK
    recvCipher <- initCipher
    keyCipher recvCipher recvK
    recvWindow <- newWindow
    pure Flow{created = now, ..}

-- | Split into sending key, receiving key.
splitKey :: SharedSecret -> Role -> IO (AeadKey, AeadKey)
splitKey ck role = do
    k1 <- snd <$> allocAtLeast (const $ pure ())
    k2 <- snd <$> allocAtLeast (const $ pure ())
    let ss1 = slice @0 k1
        ss2 = slice @0 k1
    hkdf2Into ck (mempty :: ByteString) ss1 ss2
    expandInto (SharedSecret ss1) (AeadKey k1)
    expandInto (SharedSecret ss2) (AeadKey k2)
    pure case role of
        Initiator -> (AeadKey k1, AeadKey k2)
        Responder -> (AeadKey k2, AeadKey k1)

describeFlow :: Flow -> IO String
describeFlow f@Flow{..} = do
    sentAnyV    <- readPVar sentAny
    sentYetAckV <- readPVar sentYetAck
    recvAnyV    <- readPVar recvAny
    recvYetAckV <- readPVar recvYetAck
    replaced    <- maybeReplaced f
    pure $ "Flow" <>
        " {theirAddr = " <> show theirAddr <>
        ", created = " <> show created <>
        ", role = " <> show role <>
        ", theirId = " <> show theirId <>
        ", sentAny = " <> show sentAnyV <>
        ", sentYetAck = " <> show sentYetAckV <>
        ", recvAny = " <> show recvAnyV <>
        ", recvYetAck = " <> show recvYetAckV <>
        ", replaced = " <> show replaced <>
        "}"

(<&&>) :: Monad m => m Bool -> m Bool -> m Bool
ma <&&> mb = ma >>= bool (pure False) mb

eqFlow :: Flow -> Flow -> IO Bool
eqFlow a b = pure rest <&&> io
  where
    io =
        atomically ((==) <$> readTVar (remoteAddr a) <*> readTVar (remoteAddr b)) <&&>
        ((==) <$> readPVar (sentAny a) <*> readPVar (sentAny b)) <&&>
        ((==) <$> readPVar (sentYetAck a) <*> readPVar (sentYetAck b)) <&&>
        ((==) <$> readPVar (recvAny a) <*> readPVar (recvAny b)) <&&>
        ((==) <$> readPVar (recvYetAck a) <*> readPVar (recvYetAck b)) <&&>
        ((==) <$> readPVar (replacedBy a) <*> readPVar (replacedBy b))
    rest =
        theirId a == theirId b &&
        theirAddr a == theirAddr b &&
        unsafeDupablePerformIO (constEqMem (recvK a) (recvK b)) &&
        unsafeDupablePerformIO (constEqMem (sendK a) (sendK b)) &&
        created a == created b

data Window
  = Window
    { lowMark :: {-# UNPACK #-}PVar Int RW
    , bitset  :: {-# UNPACK #-}MutableByteArray RW
    }

-- | Window length in 8-byte words.
windowLength64 :: Int
windowLength64 = 8

windowLength8 :: Int
windowLength8 = windowLength64 * 8

windowLength1 :: Int
windowLength1 = windowLength8 * 8

-- | Window length in bits.
windowLength :: Word32
windowLength = fromIntegral windowLength1

newWindow :: IO Window
newWindow = do
    lowMark <- newPVar 0
    bitset <- newByteArray windowLength8
    fillByteArray bitset 0 windowLength8 0
    pure Window{..}

-- | Is the bit in "the past", ie. set or before the current window?
windowMember :: Word32 -> Window -> IO Bool
windowMember (fromIntegral -> i) Window{..} = do
    low <- readPVar lowMark
    if
      | i <  low                 -> pure True
      | i >= low + windowLength1 -> pure False
      | otherwise -> do
        let (by, bi) = (i - low) `quotRem` 64
        (w :: Int) <- readByteArray bitset by
        pure $ testBit w bi

-- | Returns True if bit was set, False if bit is already passed.
windowSet :: Word32 -> Window -> IO Bool
windowSet (fromIntegral -> i) Window{..} = do
    low <- readPVar lowMark
    if
      | i <  low                 -> pure False
      | i >= low + windowLength1 -> True <$ shiftSet low
      | otherwise                -> True <$ set low
  where
    set :: Int -> IO ()
    set low = do
        let (by, bi) = (i - low) `quotRem` 64
        (w :: Int) <- readByteArray bitset by
        writeByteArray bitset by $ w .|. bit bi
    shiftSet :: Int -> IO ()
    shiftSet low = do
        -- by is an index into memory above win, so add 1
        let ((+1) -> by, bi) = (i - low - windowLength1) `quotRem` 64
        shiftBy by
        writeByteArray bitset (windowLength64 - 1) (bit bi :: Word64)
        writePVar lowMark $ low + (by * 64)
    shiftBy :: Int -> IO ()
    shiftBy m = go 0
      where
        go :: Int -> IO ()
        go n
          | n     >= windowLength64 = pure ()
          | n + m >= windowLength64 = writeByteArray bitset n (0 :: Word64)
          | otherwise = readByteArray @Word64 bitset (n + m) >>= writeByteArray bitset n

-- | May overestimate.
windowHigh :: Window -> IO Word32
windowHigh Window{..} = do
    lo <- readPVar lowMark
    last <- readByteArray @Word64 bitset (windowLength64 - 1)
    -- Must use leading zeros to get the most significant (left-most) bit
    -- 0 is set via bit 0 (ie. 1), so we subtract from 63
    -- If nothing is set we get -1, but that's fine because of (windowLength1 - 64) below
    let tip = 63 - countLeadingZeros last
    pure . fromIntegral $ lo + (windowLength1 - 64) + tip

describeWindow :: Window -> IO String
describeWindow Window{..} = do
    low <- readPVar lowMark
    bs <- for [0..(windowLength64 - 1)] (readByteArray @Word64 bitset)
    pure $ "Window{low = " <> show low <> ", bitset = " <> show bs <> "}"

procOutgoing ::
    Ip6Lo
 -> (Ip6Lo -> IO (Maybe Flow))
 -> IO ()                              -- ^ Dispatch to control
 -> (SockAddr -> ContentMsg -> IO ())  -- ^ Send
 -> MonoTime32
 -> JunkBuffer
 -> Buffer
 -> IO ()
procOutgoing ourLo flowMap dispatch send now junk buf = mkV6Buffer buf >>= \case
    Nothing -> pure ()
    Just v6 -> do
        srcAddr <- v6SourceAddr v6
        (Ip6Addr dstHi (Ip6Lo -> dstLo)) <- v6DestAddr v6
        when (srcAddr == ip6LoAddr ourLo && dstHi == Const.subnet) $ flowMap dstLo >>= \case
            Nothing   -> dispatch
            Just flow -> peerReady flow >>= \case
                True  -> incrCount (sentCount flow) >>= \case
                    Just c  -> sendOff v6 flow c
                    Nothing -> dispatch
                False -> dispatch
  where
    sendOff v6 Flow{..} count = do
        msg <- overwriteV6ToContent junk v6 theirId sendK sendCipher count
        remote <- readTVarIO remoteAddr
        send remote msg
        writePVar sentAny now
        ((leq created `on` ms32) <$> readPVar sentYetAck <*> readPVar recvAny) >>= \case
            True  -> writePVar sentYetAck now
            False -> pure ()

-- TODO: have dispatch / good be return value
procIncoming ::
    Ip6Lo
 -> (FlowId -> IO (Maybe Flow))
 -> IO ()                -- ^ Dispatch to control
 -> (HasCallStack => FlowId -> IO ())    -- ^ Notify flow is now good
 -> (V6Buffer -> IO ())  -- ^ Send
 -> MonoTime32
 -> JunkBuffer
 -> Buffer
 -> IO ()
procIncoming ourLo flowMap dispatch good send now junk buf = mkGotMsg buf >>= \case
    Nothing       -> pure ()
    Just (Helo _) -> dispatch
    Just (Ther _) -> dispatch
    Just (Gnrl _) -> dispatch
    Just (Content msg) -> forUnseen_ contentOurId contentCounter msg $ \c ourId Flow{..} ->
        o6 junk msg ourAddr (deLo theirAddr) recvK recvCipher (Counter c) >>= traverse_ \v6 -> do
            _ <- windowSet c recvWindow
            send v6
            updateRecvAny ourId role recvAny created
            ((leq created `on` ms32) <$> readPVar recvYetAck <*> readPVar sentAny) >>= \case
                True  -> writePVar recvYetAck now
                False -> pure ()
    Just (Keepalive msg) ->
        forUnseen_ keepaliveOurId keepaliveCounter msg $ \c ourId Flow{..} ->
        keepaliveCheckHmac (Counter c) recvK msg (pure ()) do
            _ <- windowSet c recvWindow
            updateRecvAny ourId role recvAny created
  where
    forUnseen_ readId readCounter msg f = do
        ourId <- readId msg
        flowm <- flowMap ourId
        for_ flowm $ \flow@Flow{recvWindow} -> do
            Counter c <- readCounter msg
            m <- windowMember c recvWindow
            unless m $ f c ourId flow
    o6 = overwriteContentToV6
    deLo = ip6LoAddr
    ourAddr = ip6LoAddr ourLo
    updateRecvAny ourId role recvAny created = case role of
        Initiator -> do
            -- Ensure that recvAny > created if any packet has been received, see goodForSending
            let now' = bool now (MonoTime32 $ ms32 now + 1) $ created == now
            prev <- readPVar recvAny
            writePVar recvAny now'
            -- If this is the first packet received, trigger sending buffered packets
            when (prev == created) $ good ourId
        Responder -> writePVar recvAny now
