{-# language OverloadedStrings, StrictData #-}

module Nonsense.Orbit (
    Registered (..), bsRegistered, registeredBS
  , Seen (..), bsSeen, seenBS
  , RemoteAddr (..), remoteSockAddr
  , autoinform, mkRemote, orbit, moonAddr, moonReg
  ) where

import Data.Bits (Bits (testBit))
import Network.HTTP.Client
  (RequestBody (RequestBodyBS), defaultManagerSettings, defaultRequest, host, httpLbs, method,
  newManager, path, port, requestBody, responseBody, responseStatus, responseTimeout,
  responseTimeoutMicro)
import Network.HTTP.Types (Status, methodPost, noContent204, ok200)
import Network.Socket
  (HostAddress, HostAddress6, PortNumber, SockAddr (..), Socket, tupleToHostAddress)
import Network.Socket.ByteString (sendAllTo)
import Nonsense.Bytes (atLeast, drop, orMore, read, slice, unsafeAtLeast)
import Nonsense.ECDH (KeyLength, PubKeyBytes (..))
import Nonsense.Ip6Addr (Ip6Lo (..), addrIp6Lo, ip6LoAddr, prettyIp6Addr)
import Nonsense.Packet (V6Buffer, v6NextHeader, v6Payload, v6SourceAddr)
import RIO hiding (drop)
import RIO.ByteString qualified as B
import RIO.ByteString.Lazy qualified as L
import RIO.Text qualified as T
import System.IO (hPutStrLn)

data RemoteAddr
  = RemoteV4 PortNumber HostAddress
  | RemoteV6 PortNumber HostAddress6
  deriving (Eq, Generic, Read, Show)

data Registered
  = Registered (PubKeyBytes ByteString) RemoteAddr
  deriving (Eq, Generic, NFData, Show)

data Seen
  = Seen Ip6Lo RemoteAddr
  deriving (Eq, Read, Show)

registeredBS :: Registered -> ByteString
registeredBS (Registered (PubKeyBytes pk) r) = orMore pk <> showBS r

bsRegistered :: ByteString -> Maybe Registered
bsRegistered bs = do
    al <- atLeast @KeyLength bs
    ra <- readBS (orMore (drop @KeyLength al))
    pure $ Registered (PubKeyBytes (slice @0 @KeyLength al)) ra

seenBS :: Seen -> ByteString
seenBS = showBS

bsSeen :: ByteString -> Maybe Seen
bsSeen = readBS

showBS :: (Show a) => a -> ByteString
showBS = fromString . show

readBS :: (Read a) => ByteString -> Maybe a
readBS = readMaybe . T.unpack . decodeUtf8Lenient

remoteSockAddr :: RemoteAddr -> SockAddr
remoteSockAddr = \case
    RemoteV4 p h -> SockAddrInet p h
    RemoteV6 p h -> SockAddrInet6 p 0 h 0

sockRemoteAddr :: SockAddr -> Maybe RemoteAddr
sockRemoteAddr = \case
    SockAddrInet p h      -> Just $ RemoteV4 p h
    SockAddrInet6 p _ h _ -> Just $ RemoteV6 p h
    _                     -> Nothing

mkRemote :: IO (Ip6Lo -> IO (Maybe Registered))
mkRemote = do
    manager <- newManager defaultManagerSettings
    pure $ run manager
  where
    r = defaultRequest{host = moonHost, port = 5000}
    run manager addr@(Ip6Lo lo)
      | addr == moonAddr = pure $ Just moonReg
      | otherwise = tryAny (httpLbs r{path = "/lookup/" <> showBS lo} manager) >>= \case
        Right resp
          | responseStatus resp == ok200 ->
            pure $ bsRegistered (L.toStrict (responseBody resp))
          | otherwise ->
            pure Nothing
        Left exc -> Nothing <$ hPutStrLn stderr ("Lookup error: " <> displayException exc)

register :: PubKeyBytes ByteString -> IO (Maybe SomeException)
register (PubKeyBytes uk) = do
    manager <- newManager defaultManagerSettings
    let r = defaultRequest
            { host = moonHost
            , port = 5000
            , path = "/register"
            , method = methodPost
            , requestBody = RequestBodyBS (orMore uk)
            , responseTimeout = responseTimeoutMicro 5e6
            }
    tryAny (httpLbs r manager) >>= \case
        Left exc -> pure (Just exc)
        Right (responseStatus -> status)
          | status == noContent204 -> pure Nothing
          | otherwise -> pure (Just (toException (RegistrationRejected status)))

newtype RegistrationRejected
  = RegistrationRejected Status
  deriving stock (Show)
  deriving anyclass (Exception)

orbit :: PubKeyBytes ByteString -> Ip6Lo -> IO a
orbit uk ourAddr
  | ourAddr == moonAddr = hPutStrLn stderr "Running on moon address, no registration" *> nogo
  | otherwise = go
  where
    go = register uk >>= \case
        Nothing -> threadDelay 60e6 *> go
        Just exc -> do
            hPutStrLn stderr ("Registration error: " <> displayException exc)
            threadDelay 8e6
            go
    nogo = forever $ threadDelay maxBound

autoinform :: Socket -> SockAddr -> V6Buffer -> IO ()
autoinform sock from v6
  | Just pl <- atLeast @20 (v6Payload v6)
  , Just ra <- sockRemoteAddr from = v6NextHeader v6 >>= \case
    0x06 -> do  -- TCP
        flags <- read @13 pl
        src <- v6SourceAddr v6
        let lo = addrIp6Lo src
        -- Bits are numbered from 0 with bit 0 being the least significant bit.
        -- (Some TCP segment diagrams number MSB 0 which is just confusing)
        when (testBit flags 1) do
            sendAllTo sock (seenBS (Seen lo ra)) (SockAddrInet 5000 (tupleToHostAddress (127, 0, 0, 1)))
    _ -> pure ()
  | otherwise = pure ()

moonHost :: ByteString
moonHost = fromString . prettyIp6Addr $ ip6LoAddr moonAddr

moonAddr :: Ip6Lo
moonAddr = Ip6Lo $ byteSwap64 0x1e1f_0fff_547f_8dc2

moonReg :: Registered
moonReg = Registered pubKey (RemoteV4 30000 (tupleToHostAddress (10, 120, 1, 1)))
  where
    pubKey = PubKeyBytes $ unsafeAtLeast $ B.pack
        [33,142,145,26,82,143,220,63,84,148,195,253,45,198,246,212,118,63,203,76,181,178,220,99,215,246,184,74,73,71,205,45]

instance NFData RemoteAddr where
    rnf = \case
        RemoteV4 p h -> p `seq` h `deepseq` ()
        RemoteV6 p h -> p `seq` h `deepseq` ()
