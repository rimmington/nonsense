{-# language GADTs, OverloadedStrings #-}

module Nonsense.AEAD
  ( AeadKey (..), TagLength
  , tagLength
  , expandInto
  , keyCipher
  , encryptInPlace, encryptEmpty, decryptInPlace, decryptEmpty
  ) where

import GHC.TypeNats (type (+), type (-), type (<=))
import Nonsense.Bytes (AtLeast, Bytes, Mutable, Scrubbed, constEqMem)
import Nonsense.Bytes qualified as B
import Nonsense.Cipher (Cipher, Counter, cipherInPlace, setKey)
import Nonsense.ECDH (KeyLength, SharedSecret (..), keyLength)
import Nonsense.K12 (HmacLength, hkdf2Into, hmacForAeadInto)
import RIO

-- | 32 bytes for MAC, then 32 bytes for cipher.
-- Justification: that's what draft-mcgrew-aead-aes-cbc-hmac-sha2-05 does.
newtype AeadKey
  = AeadKey (AtLeast (KeyLength + KeyLength) Scrubbed)
  deriving newtype (Bytes)

instance Show AeadKey where
    show _ = "AeadKey"

type TagLength = 16

tagLength :: Int
tagLength = 16

zeroSS :: SharedSecret
zeroSS = SharedSecret . B.unsafeUncheckedAtLeast . B.pack $ replicate keyLength 0

-- | It is permissible for the secret memory to overlap with the key.
expandInto :: SharedSecret -> AeadKey -> IO ()
expandInto ikm (AeadKey out) =
    hkdf2Into zeroSS ikm (B.slice @0 out) (B.slice @KeyLength out)

keyCipher :: Cipher -> AeadKey -> IO ()
keyCipher c (AeadKey k) = setKey c (SharedSecret (B.slice @KeyLength k))

encryptInPlace ::
    (Mutable ba, Bytes ad, HmacLength <= n, TagLength <= n)
 => Cipher -- ^ A Cipher already keyed via keyCipher
 -> AeadKey
 -> Counter
 -> ad
 -> AtLeast n ba
 -> IO (AtLeast (n - TagLength) ba)
encryptInPlace cipher (AeadKey k) c ad ba = do
    let content = B.dropBack @HmacLength ba
    cipherInPlace cipher c content
    hmacForAeadInto
        (SharedSecret (B.slice @0 k))
        c
        ad
        content
        (B.sliceBack @HmacLength @HmacLength ba)
    pure $ B.dropBack @16 ba

-- | Encrypt a zero-length message; no Cipher required.
encryptEmpty ::
    (Mutable ba, Bytes ad)
 => AeadKey
 -> Counter
 -> ad
 -> AtLeast HmacLength ba
 -> IO (AtLeast TagLength ba)
encryptEmpty (AeadKey k) c ad ba = do
    hmacForAeadInto
        (SharedSecret (B.slice @0 k))
        c
        ad
        (B.slice @0 @0 ba)
        (B.slice @0 ba)
    pure $ B.slice @0 ba

decryptInPlace ::
    (Mutable ba, Bytes ad, TagLength <= n)
 => Cipher -- ^ A Cipher already keyed via keyCipher
 -> AeadKey
 -> Counter
 -> ad
 -> AtLeast n ba
 -> IO (Maybe (AtLeast (n - TagLength) ba))
decryptInPlace cipher (AeadKey k) c ad ba = do
    tmp <- snd <$> B.allocAtLeast @_ @B.Buffer (const $ pure ())
    let content = B.dropBack @TagLength ba
    hmacForAeadInto (SharedSecret (B.slice @0 k)) c ad content tmp
    res <- constEqMem
        (B.slice @0 @TagLength tmp)
        (B.sliceBack @TagLength @TagLength ba)
    case res of
        True -> do
            cipherInPlace cipher c content
            pure $ Just content
        False -> pure Nothing

-- | Decrypt a zero-length message; no Cipher required.
decryptEmpty ::
    (Mutable ba, Bytes ad)
 => AeadKey
 -> Counter
 -> ad
 -> AtLeast TagLength ba
 -> IO Bool
decryptEmpty (AeadKey k) c ad ba = do
    tmp <- snd <$> B.allocAtLeast @_ @B.Buffer (const $ pure ())
    hmacForAeadInto
        (SharedSecret (B.slice @0 k))
        c
        ad
        (B.slice @0 @0 ba)
        tmp
    constEqMem
        (B.slice @0 @TagLength tmp)
        ba
