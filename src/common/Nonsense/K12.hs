{-# language TemplateHaskell #-}

module Nonsense.K12
  ( Hash (..)
  , hash, hash2, hash2Into
  , HmacLength
  , hmacLength, hmacInto
  , hkdf2Into, hmacForAeadInto
  ) where

import Foreign (Ptr, nullPtr)
import Language.C.Inline qualified as C
import Nonsense.Bytes
  (AtLeast, Bytes, Mutable, allocAtLeast, byteLength, mutWithBytePtr, unsafeWithBytePtr)
import Nonsense.Cipher (Counter (..))
import Nonsense.ECDH (SharedSecret (..))
import RIO
import System.IO.Unsafe (unsafePerformIO)

C.include "<string.h>"
C.include "<k12/KangarooTwelve.h>"

-- | 32 bytes.
newtype Hmac ba
  = Hmac (AtLeast HmacLength ba)
  deriving newtype (Bytes, Show)

newtype Hash ba
  = Hash { hashBytes :: AtLeast HmacLength ba }
  deriving newtype (Bytes, Eq, Mutable, Show)

hmacLength :: Int
hmacLength = 32

-- | Length of hash and therefore HMAC.
type HmacLength = 32

hm :: Ptr Word8 -> Ptr Word8 -> C.CSize -> Word8 -> Ptr Word8 -> IO ()
hm keyPtr msg1Ptr msg1Len msg2Byte outPtr =
    -- HMAC blocklength for K12 is rate ÷ 8 = 1344 ÷ 8 = 168
    -- https://keccak.team/files/Keccak-submission-3.pdf 5.1.1
    -- https://keccak.team/files/KangarooTwelve.pdf 3.1
    [C.block| int {
        unsigned char k_ipad[168];
        unsigned char k_opad[168];

        memset(k_ipad, 54, sizeof k_ipad);
        memset(k_opad, 92, sizeof k_opad);
        memcpy(k_ipad, $(uint8_t *keyPtr), 32);
        memcpy(k_opad, $(uint8_t *keyPtr), 32);

        for (int i = 0; i < 32; i++) {
            k_ipad[i] ^= 0x36;
            k_opad[i] ^= 0x5c;
        }

        KangarooTwelve_Instance kt;
        int res = KangarooTwelve_Initialize(&kt, 32);
        if (res != 0) goto quit;
        res = KangarooTwelve_Update(&kt, k_ipad, sizeof k_ipad);
        if (res != 0) goto quit;
        res = KangarooTwelve_Update(&kt, $(uint8_t *msg1Ptr), $(size_t msg1Len));
        if (res != 0) goto quit;
        if ($(uint8_t msg2Byte)) {
            res = KangarooTwelve_Update(&kt, &$(uint8_t msg2Byte), 1);
            if (res != 0) goto quit;
        }
        res = KangarooTwelve_Final(&kt, $(uint8_t *outPtr), NULL, 0);
        if (res != 0) goto quit;

        res = KangarooTwelve_Initialize(&kt, 32);
        if (res != 0) goto quit;
        res = KangarooTwelve_Update(&kt, k_opad, sizeof k_opad);
        if (res != 0) goto quit;
        res = KangarooTwelve_Update(&kt, $(uint8_t *outPtr), 32);
        if (res != 0) goto quit;
        res = KangarooTwelve_Final(&kt, $(uint8_t *outPtr), NULL, 0);

        quit:
        explicit_bzero(k_ipad, sizeof k_ipad);
        explicit_bzero(k_opad, sizeof k_opad);
        return res;
    } |] >>= \case
        0 -> pure ()
        _ -> error "bug: hmac failure"

-- | HMAC per RFC 2104, accepting only a 32-byte key.
hmacInto :: (Bytes ba, Mutable out) => SharedSecret -> ba -> AtLeast HmacLength out -> IO ()
hmacInto key msg out =
    unsafeWithBytePtr key $ \keyPtr ->
    unsafeWithBytePtr msg $ \msgPtr ->
    mutWithBytePtr out (hm keyPtr msgPtr (fromIntegral $ byteLength msg) 0)

-- | HKDF per section 4.3 of the Noise spec.
-- Equivalent to HKDF per RFC 5869, with info = "", length = 64 and accepting only a 32-byte salt.
--
-- It is safe to use a buffer as both input and output.
hkdf2Into ::
    (Bytes ikm, Mutable out1, Mutable out2)
 => SharedSecret
 -> ikm
 -> AtLeast HmacLength out1
 -> AtLeast HmacLength out2
 -> IO ()
hkdf2Into salt ikm out1 out2 =
    unsafeWithBytePtr salt \saltPtr ->
    unsafeWithBytePtr ikm \ikmPtr ->
    mutWithBytePtr out1 \out1Ptr ->
    mutWithBytePtr out2 \out2Ptr -> do
        hm saltPtr ikmPtr (fromIntegral $ byteLength ikm) 0 out2Ptr
        hm out2Ptr nullPtr 0 1 out1Ptr
        hm out2Ptr out1Ptr 32 2 out2Ptr

-- This could use the encoding mechanism from
-- draft-mcgrew-aead-aes-cbc-hmac-sha1-01
-- or
-- https://github.com/paseto-standard/paseto-spec/blob/77ca84e4d0e5f1b1f52975797a01368062065148/docs/01-Protocol-Versions/Common.md#authentication-padding
-- but instead do the simplest thing that could work:
-- suffix the AAD length as a little-endian Word64.
-- The nonce length is fixed to 16 bytes by the block size.
-- The nonce must be authenticated; this is specified explicitly in RFC 5116 §2.1.
-- So the HMAC is: HMAC(N | AD | C | len(AD))
hmacForAeadInto ::
    (Bytes ba, Bytes ad, Mutable out)
 => SharedSecret
 -> Counter
 -> ad
 -> ba
 -> AtLeast HmacLength out
 -> IO ()
hmacForAeadInto ss (Counter (fromIntegral -> nonce1)) ad ba out =
    unsafeWithBytePtr ss \keyPtr ->
    unsafeWithBytePtr ad \adPtr ->
    unsafeWithBytePtr ba \baPtr ->
    mutWithBytePtr out \outPtr ->
    -- HMAC blocklength for K12 is rate ÷ 8 = 1344 ÷ 8 = 168
    -- https://keccak.team/files/Keccak-submission-3.pdf 5.1.1
    -- https://keccak.team/files/KangarooTwelve.pdf 3.1
    [C.block| int {
        unsigned char k_ipad[168];
        unsigned char k_opad[168];

        memset(k_ipad, 54, sizeof k_ipad);
        memset(k_opad, 92, sizeof k_opad);
        memcpy(k_ipad, $(uint8_t *keyPtr), 32);
        memcpy(k_opad, $(uint8_t *keyPtr), 32);

        for (int i = 0; i < 32; i++) {
            k_ipad[i] ^= 0x36;
            k_opad[i] ^= 0x5c;
        }

        KangarooTwelve_Instance kt;
        uint64_t nonce0 = 0;
        int res = KangarooTwelve_Initialize(&kt, 32);
        if (res != 0) goto quit;
        res = KangarooTwelve_Update(&kt, k_ipad, sizeof k_ipad);
        if (res != 0) goto quit;
        res = KangarooTwelve_Update(&kt, (uint8_t*)&nonce0, 8);
        if (res != 0) goto quit;
        res = KangarooTwelve_Update(&kt, (uint8_t*)&$(uint64_t nonce1), 8);
        if (res != 0) goto quit;
        res = KangarooTwelve_Update(&kt, $(uint8_t *adPtr), $(size_t adLen));
        if (res != 0) goto quit;
        res = KangarooTwelve_Update(&kt, $(uint8_t *baPtr), $(size_t baLen));
        if (res != 0) goto quit;
        res = KangarooTwelve_Update(&kt, (uint8_t*)&$(uint64_t adLen64), 8);
        if (res != 0) goto quit;
        res = KangarooTwelve_Final(&kt, $(uint8_t *outPtr), NULL, 0);
        if (res != 0) goto quit;

        res = KangarooTwelve_Initialize(&kt, 32);
        if (res != 0) goto quit;
        res = KangarooTwelve_Update(&kt, k_opad, sizeof k_opad);
        if (res != 0) goto quit;
        res = KangarooTwelve_Update(&kt, $(uint8_t *outPtr), 32);
        if (res != 0) goto quit;
        res = KangarooTwelve_Final(&kt, $(uint8_t *outPtr), NULL, 0);

        quit:
        explicit_bzero(k_ipad, sizeof k_ipad);
        explicit_bzero(k_opad, sizeof k_opad);
        return res;
    } |] >>= \case
        0 -> pure ()
        _ -> error "bug: hmac failure"
  where
    adLen64 :: Word64
    adLen64 = fromIntegral $ byteLength ad
    adLen, baLen :: C.CSize
    adLen = fromIntegral $ byteLength ad
    baLen = fromIntegral $ byteLength ba

-- | Hash @ba || bb@.
hash2 :: (Bytes ba, Bytes bb) => ba -> bb -> Hash ByteString
hash2 ba bb = unsafePerformIO $
    unsafeWithBytePtr ba \baPtr ->
    unsafeWithBytePtr bb \bbPtr -> do
        (_, out) <- allocAtLeast $ h2 baPtr baLen bbPtr bbLen
        pure $ Hash out
  where
    baLen, bbLen :: C.CSize
    baLen = fromIntegral $ byteLength ba
    bbLen = fromIntegral $ byteLength bb

-- | Hash @ba || bb@ into @out@.
hash2Into :: (Bytes ba, Bytes bb, Mutable out) => ba -> bb -> Hash out -> IO ()
hash2Into ba bb out =
    unsafeWithBytePtr ba \baPtr ->
    unsafeWithBytePtr bb \bbPtr ->
    mutWithBytePtr out $ h2 baPtr baLen bbPtr bbLen
  where
    baLen, bbLen :: C.CSize
    baLen = fromIntegral $ byteLength ba
    bbLen = fromIntegral $ byteLength bb

h2 :: Ptr Word8 -> C.CSize -> Ptr Word8 -> C.CSize -> Ptr Word8 -> IO ()
h2 baPtr baLen bbPtr bbLen outPtr =
    [C.block| int {
        KangarooTwelve_Instance kt;
        int res = KangarooTwelve_Initialize(&kt, 32);
        if (res != 0) return res;
        res = KangarooTwelve_Update(&kt, $(uint8_t *baPtr), $(size_t baLen));
        if (res != 0) return res;
        res = KangarooTwelve_Update(&kt, $(uint8_t *bbPtr), $(size_t bbLen));
        if (res != 0) return res;
        res = KangarooTwelve_Final(&kt, $(uint8_t *outPtr), NULL, 0);
        return res;
    } |] >>= \case
        0 -> pure ()
        _ -> error "bug: hmac failure"

hash :: (Bytes ba) => ba -> Hash ByteString
hash ba = unsafePerformIO $
    unsafeWithBytePtr ba \baPtr ->
    fmap (Hash . snd) . allocAtLeast $ \outPtr ->
        [C.block| int {
            KangarooTwelve_Instance kt;
            int res = KangarooTwelve_Initialize(&kt, 32);
            if (res != 0) return res;
            res = KangarooTwelve_Update(&kt, $(uint8_t *baPtr), $(size_t baLen));
            if (res != 0) return res;
            res = KangarooTwelve_Final(&kt, $(uint8_t *outPtr), NULL, 0);
            return res;
        } |] >>= \case
            0 -> pure ()
            _ -> error "bug: hash failure"
  where
    baLen :: C.CSize
    baLen = fromIntegral $ byteLength ba
