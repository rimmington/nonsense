{-# language StrictData #-}
{-# OPTIONS_GHC -Wno-name-shadowing #-}

module Nonsense.Wiring
   ( Wiring
   , wire
   , nextIn, procIn, nextOut, procOut, ctl, flowIdMap
   , sneak, seek
   ) where

import Control.Monad.Primitive (RealWorld, primToPrim)
import Data.Bits (shiftL, shiftR, (.|.))
import Data.Vector.Storable.Mutable qualified as VM
import ListT qualified
import Network.Socket (SockAddr (SockAddrInet), hostAddressToTuple, tupleToHostAddress)
import Nonsense.Address (AddrKey (..), keyAddr)
import Nonsense.Bytes
  (AtLeast, Buffer, Scrubbed, allocAtLeast, isAtLeast, orMore, unsafeUncheckedAtLeast)
import Nonsense.Cipher (Cipher, initCipher)
import Nonsense.Ctl
  (PfSecretLength, Preflow, PreflowId (..), RemoteInfo (..), ctlFlowBecameGood, ctlFlowTiming,
  ctlIncoming, ctlOutgoing, ctlPreflowTiming)
import Nonsense.Ip6Addr (Ip6Lo)
import Nonsense.MonoTime (MonoTime32)
import Nonsense.Packet
  (FlowId, JunkBuffer, JunkBufferSize, V6Buffer, asJunkBuffer, fromJunkBuffer, getBuf,
  junkBufferSize, newJunkBuffer, readAsV6Len)
import Nonsense.Proc (Flow, procIncoming, procOutgoing)
import Nonsense.Random (Gen, seedGen, systemRandomSeed, thawGen)
import Nonsense.RingBuffer
  (RingBuffer, newAlignedPinnedRingBuffer, readVector, unsafeCommitRead, unsafeCommitWrite,
  writeVector)
import RIO
import StmContainers.Map qualified as M
import StmContainers.Set qualified as S

data Wiring
  = Wiring
    { inRb         :: RingBuffer
    , outRb        :: RingBuffer
    , flowIdMap    :: M.Map FlowId Flow
    , flowLoMap    :: M.Map Ip6Lo Flow
    , preflowIdMap :: M.Map PreflowId Preflow
    , preflowLoMap :: M.Map Ip6Lo Preflow
    , newlyGoodSet :: S.Set FlowId
    , genIn        :: Gen RealWorld
    , genOut       :: Gen RealWorld
    , genCtl       :: Gen RealWorld
    , ctlJunk      :: JunkBuffer
    , secretScr    :: AtLeast PfSecretLength Scrubbed
    , spareCipher  :: Cipher
    , staticKey    :: AddrKey
    , sendOut      :: SockAddr -> Buffer -> IO ()
    , cacheMap     :: M.Map Ip6Lo RemoteInfo
    , remote       :: Ip6Lo -> IO (Maybe RemoteInfo)
    , later        :: IO () -> IO ()
    }

nextIn, nextOut :: Wiring -> IO JunkBuffer
nextIn = writeJunk . inRb
nextOut = writeJunk . outRb

-- Sneak packet length and origin address into the first few bytes.
-- This works because of contentJunkBufferOffset.
sneak :: SockAddr -> Int -> Buffer -> IO ()
sneak (SockAddrInet port addr) len buf = do
    let (b0, b1, b2, b3) = hostAddressToTuple addr
        port16 = fromIntegral port :: Word16
    VM.unsafeWrite buf 0 4
    VM.unsafeWrite buf 1 b0
    VM.unsafeWrite buf 2 b1
    VM.unsafeWrite buf 3 b2
    VM.unsafeWrite buf 4 b3
    VM.unsafeWrite buf 5 (fromIntegral port16)
    VM.unsafeWrite buf 6 (fromIntegral (port16 `shiftR` 8))
    VM.unsafeWrite buf 7 (fromIntegral len)
    VM.unsafeWrite buf 8 (fromIntegral (len `shiftR` 8))
sneak _ _ _ = error "unsupported SockAddr type"

seek :: Buffer -> IO (SockAddr, Int)
seek buf = VM.unsafeRead buf 0 >>= \case
    4 -> do
        b0 <- VM.unsafeRead buf 1
        b1 <- VM.unsafeRead buf 2
        b2 <- VM.unsafeRead buf 3
        b3 <- VM.unsafeRead buf 4
        let addr = tupleToHostAddress (b0, b1, b2, b3)
        port0 <- VM.unsafeRead buf 5
        port1 <- VM.unsafeRead buf 6
        let port = fromIntegral $ fromIntegral @_ @Word16 port0 .|. (fromIntegral port1 `shiftL` 8)
        len0 <- VM.unsafeRead buf 7
        len1 <- VM.unsafeRead buf 8
        let len = fromIntegral len0 .|. (fromIntegral len1 `shiftL` 8)
        pure (SockAddrInet port addr, len)
    _ -> error "unsupported SockAddr type"

upTo :: Int -> IO Bool -> IO Bool
upTo count a = a >>= \case
    False -> pure False
    True  -> go 1
  where
    go acc
      | acc < count = a >>= \case
            False -> pure True
            True  -> go (acc + 1)
      | otherwise = pure True

writeJunk :: RingBuffer -> IO JunkBuffer
writeJunk rb = fst . fromJunkBuffer . asJunkBuffer . unsafeUncheckedAtLeast <$> writeVector rb

cache :: Wiring -> Ip6Lo -> STM (Maybe RemoteInfo)
cache Wiring{cacheMap} lo = M.lookup lo cacheMap

uncache :: Wiring -> Ip6Lo -> STM ()
uncache Wiring{cacheMap} lo = M.delete lo cacheMap

caching :: Wiring -> Ip6Lo -> IO (Maybe RemoteInfo)
caching Wiring{cacheMap, remote} lo = remote lo >>= \case
    Nothing -> pure Nothing
    Just v@(RemoteInfo _ k)
      | keyAddr k == Just lo -> Just v <$ atomically (M.insert v lo cacheMap)
      | otherwise            -> pure Nothing

wire ::
    AddrKey
 -> (SockAddr -> Buffer -> IO ())
 -> (Ip6Lo -> IO (Maybe RemoteInfo))
 -> (IO () -> IO ())
 -> IO Wiring
wire staticKey sendOut remote later = do
    inRb <- newAlignedPinnedRingBuffer junkBufferSize 10
    outRb <- newAlignedPinnedRingBuffer junkBufferSize 10
    flowIdMap <- M.newIO
    flowLoMap <- M.newIO
    preflowIdMap <- M.newIO
    preflowLoMap <- M.newIO
    cacheMap <- M.newIO
    newlyGoodSet <- S.newIO
    genIn <- thawGen . seedGen =<< systemRandomSeed
    genOut <- thawGen . seedGen =<< systemRandomSeed
    genCtl <- thawGen . seedGen =<< systemRandomSeed
    (_, secretScr) <- allocAtLeast (const (pure ()))
    ctlJunk <- newJunkBuffer
    spareCipher <- initCipher
    pure Wiring{..}

procIn :: Wiring-> (V6Buffer -> IO ()) -> IO () -> MonoTime32 -> SockAddr -> Buffer -> IO ()
procIn Wiring{..} sendOn notify now addr buf = do
    rbBuf <- writeVector inRb
    let (fromJunkBuffer -> (junk, _)) = asJunkBuffer (unsafeUncheckedAtLeast rbBuf)
        dispatch = do
            sneak addr (VM.length buf) rbBuf
            _ <- unsafeCommitWrite inRb
            notify
        good ourId = do
            atomically $ S.insert ourId newlyGoodSet
            notify
        AddrKey{akLo} = staticKey
    procIncoming akLo (atomically . (`M.lookup` flowIdMap)) dispatch good sendOn now junk buf

procOut :: Wiring -> IO () -> MonoTime32 -> Buffer -> IO ()
procOut Wiring{..} notify now buf = do
    junk <- writeJunk outRb
    let dispatch = unsafeCommitWrite outRb *> notify
        AddrKey{akLo} = staticKey
    procOutgoing akLo (atomically . (`M.lookup` flowLoMap)) dispatch (\a -> sendOut a . getBuf) now junk buf

ctlIn :: Wiring -> IO MonoTime32 -> IO Bool
ctlIn Wiring{..} nowM = readVector inRb >>= \case
    Nothing -> pure False
    Just rbBuf -> True <$ do
        (addr, bufLen) <- seek rbBuf
        let large = unsafeUncheckedAtLeast @JunkBufferSize rbBuf
            (junk, shifted) = fromJunkBuffer $ asJunkBuffer large
            buf = VM.unsafeTake bufLen (orMore shifted)
        now <- nowM
        ctlIncoming
            staticKey
            spareCipher
            (\f -> primToPrim $ f genIn)
            preflowLoMap
            preflowIdMap
            flowLoMap
            flowIdMap
            sendOut
            now
            addr
            junk
            secretScr
            buf
        unsafeCommitRead inRb

ctlOut :: Wiring -> IO MonoTime32 -> IO Bool
ctlOut w@Wiring{..} nowM = readVector outRb >>= \case
    Nothing -> pure False
    Just rbBuf -> True <$ do
        let (junk, shifted) = fromJunkBuffer $ asJunkBuffer (unsafeUncheckedAtLeast rbBuf)
        len <- readAsV6Len (isAtLeast shifted)
        let buf = VM.unsafeTake (fromIntegral len) (orMore shifted)
        now <- nowM
        ctlOutgoing
            ctlJunk
            spareCipher
            (\f -> primToPrim $ f genOut)
            preflowLoMap
            preflowIdMap
            flowLoMap
            flowIdMap
            (cache w)
            (caching w)
            sendOut
            later
            now
            junk
            buf
        unsafeCommitRead outRb

ctlTime :: Wiring -> IO MonoTime32 -> IO ()
ctlTime w@Wiring{..} nowM = do
    newGoodIds <- atomically . ListT.toReverseList $ S.listT newlyGoodSet
    for_ newGoodIds $ \fid -> do
        res <- atomically $
            (,) <$> M.lookup fid flowIdMap <*> M.lookup (PreflowId fid) preflowIdMap
        case res of
            (Just flow, Just preflow) ->
                ctlFlowBecameGood spareCipher sendOut preflowLoMap preflowIdMap fid flow preflow
            -- This runs before any timer checks, so the (pre)flow must be present
            _ -> error "newly good flow missing"
        atomically $ S.delete fid newlyGoodSet
    preflows <- atomically . ListT.toReverseList $ M.listT preflowIdMap
    now <- nowM
    for_ preflows . uncurry $
        ctlPreflowTiming preflowLoMap preflowIdMap flowLoMap flowIdMap (uncache w) now
    flows <- atomically . ListT.toReverseList $ M.listT flowIdMap
    now <- nowM
    for_ flows . uncurry $ ctlFlowTiming
        ctlJunk
        (\f -> primToPrim $ f genCtl)
        preflowLoMap
        preflowIdMap
        flowLoMap
        flowIdMap
        (cache w)
        (caching w)
        sendOut
        later
        now

-- TODO: throttle ctlTime?
ctl :: Wiring -> IO MonoTime32 -> IO Bool
ctl w nowM =
    (||) <$> upTo 3 (ctlIn w nowM) <*> upTo 3 (ctlOut w nowM) <* ctlTime w nowM
